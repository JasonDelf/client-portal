<?php
	function type_available($state , $statecode) {
		// $state is the state of the building from its database row
		// $statecode is the 'states' value for the report, determining which states have access to the item.
		
		//change state value to boolean value
		$bstate = 0;
		switch ($state) {
			case 350:
				$bstate = 2;
				break;
			case 351:
				$bstate = 1;
				break;
			case 352:
				$bstate = 4;
				break;
			case 1873:
				$bstate = 8;
				break;
			case 1874:
				$bstate = 16;
				break;
			case 1875:
				$bstate = 32;
				break;
			case 1876:
				$bstate = 64;
				break;
			case 1877:
				$bstate = 128;
				break;
			case 2693:
				$bstate = 256;
				break;
			case 44052:
				$bstate = 512;
				break;
			case 44054:
				$bstate = 1024;
				break;
			case 44055:
				$bstate = 2048;
				break;
			case 44056:
				$bstate = 4096;
				break;
			case 44057:
				$bstate = 8192;
				break;
		}	
		$options = array();
			
		if ($statecode >= 8192) {
			$options[] = 8192;
			$statecode = $statecode - 8192;
		}
		if ($statecode >= 4096) {
			$options[] = 4096;
			$statecode = $statecode - 4096;
		}
		if ($statecode >= 2048) {
			$options[] = 2048;
			$statecode = $statecode - 2048;
		}
		if ($statecode >= 1024) {
			$options[] = 1024;
			$statecode = $statecode - 1024;
		}
		if ($statecode >= 512) {
			$options[] = 512;
			$statecode = $statecode - 512;
		}
		if ($statecode >= 256) {
			$options[] = 256;
			$statecode = $statecode - 256;
		}
		if ($statecode >= 128) {
			$options[] = 128;
			$statecode = $statecode - 128;
		}
		if ($statecode >= 64) {
			$options[] = 64;
			$statecode = $statecode - 64;
		}
		if ($statecode >= 32) {
			$options[] = 32;
			$statecode = $statecode - 32;
		}
		if ($statecode >= 16) {
			$options[] = 16;
			$statecode = $statecode - 16;
		}
		if ($statecode >= 8) {
			$options[] = 8;
			$statecode = $statecode - 8;
		}
		if ($statecode >= 4) {
			$options[] = 4;
			$statecode = $statecode - 4;
		}
		if ($statecode >= 2) {
			$options[] = 2;
			$statecode = $statecode - 2;
		}
		if ($statecode >= 1) {
			$options[] = 1;
			$statecode = $statecode - 1;
		} 
		if (in_array($bstate, $options)) {
			return true;
		}		
		return false;
	}
?>