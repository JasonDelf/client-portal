<?php 
class ClientLocation extends MY_Model
{
	var $id = '';
	
	public function ClientLocation() {
		parent::MY_Model();
		$this->table = 'client_locations';
		$this->primary_key = 'id';
	}

    function get_tariff_rate( $location, $RP, $tariff, $phase , $get_value = false )
    {
    	$this->db->where('locationid', $location->id);
    	$this->db->where('tariffid', $tariff->id);
    	$this->db->where('tariffphaseid', $phase->id);
    	$this->db->where('reportingperiodid', $RP->id);
       	$sqlq = $this->db->get("location_tariffs");
	   
       	if ( $sqlq->num_rows() == 0 )
       	{
           	if ( $phase->default_value != 0 )
           	{
				return $phase->default_value;
           	}
        } else {
        	return $sqlq->row()->tariffrate;
        }
	}
	
	public function get_details_all($orderby='id', $direction='asc', $filter='') {
		$this->db->order_by($orderby, $direction);
		if ($filter != "") {
			$this->db->where($filter);
		}		
		$locations = $this->db->get($this->table)->result();
		foreach ($locations as $location) {
			$clientname = $this->client->get($location->clientid)->company_name;
			$location->clientname = $clientname;
			$statename = $this->listitem->get($location->physical_state)->itemvalue;
			$location->statename = $statename;
		}

		return $locations;

	}


	public function get_all_details($id) {
		$location = $this->get($id);

		$clientname = $this->client->get($location->clientid)->company_name;
		$location->clientname = $clientname;
		$statename = $this->listitem->get($location->physical_state)->itemvalue;
		$location->statename = $statename;

		return $location;

	}

		
    public function get_reportable_activities ( $LOCATION, $section )
    {
    	$CI =& get_instance();
    	$CI->load->model('locationworksheettemplatesection');
    	$CI->load->model('pool');
       	//get the pools in this section
		$CI->db->where('deleted', 0);
		$CI->db->where('locationid', $LOCATION->id);
		$CI->db->where('sectionid', $section);
		$CI->db->order_by('pos asc');
    	$pools = $CI->db->get('location_worksheet_template_pools')->result();
       
       
       	foreach ( $pools as $pool )
       	{
       		$poolid = $pool->id;
        	$lp = $CI->locationworksheettemplatepool->get( $poolid );
           	$actualpool = $CI->pool->get($lp->poolid);
           	$activities = $CI->pool->get_reportable_activities($actualpool)->result();
           	foreach ( $activities as $activity )
           		$reportable_activities[] = $activity->id;
           
       	}
       	if ( is_array ( $reportable_activities ) )
       	{
       		$CI->db->where_in('id', $reportable_activities );
       		$result = $CI->db->get('pool_activity')->result();
           	return $result;
       }
       
       return false;
    }

    function get_assets( $section = '', $activity = '' )
    {
		$CI =& get_instance();
		
		if ( is_object ( $section ) )
       		$CI->db->where('locationsectionid', $section->id);
		if ( is_object ( $activity ) )
        	$CI->db->where('activityid', $activity->id);
           
        $CI->db->where('deleted', 0);
        $CI->db->where('locationid', $LOCATION->id);
		$results = $CI->db->get('assets')->result();    

		return $results;
       
    }
    
    function get_current_data ( $LOCATION, $rp, $SECTION, $activity, $ef, $asset = '', $asset_number = '' )
    {
       	if ( $asset ) 
           	$this->db->where('assetid', $asset->id);

       	if ( $asset_number )
			$this->db->where('assetnum', $asset_number);
     
		$this->db->where('deleted', '0');
		$this->db->where('clientid', $LOCATION->clientid);
		$this->db->where('locationid', $LOCATION->id);
		$this->db->where('reportingperiodid', $rp->id);
		$this->db->where('ws_sectionid', $SECTION->id);
		$this->db->where('ws_poolactivityid', $activity->id);
		if ( is_object ( $asset ) )
        	$this->db->where('assetid', $asset->id);
		$this->db->where('efid', $ef->id);
		$data = $this->db->get('location_usage')->result();
     
		return $data;
    
    }

	function get_carbon_purchased($RP) {
	
	}
	
	function get_carbon_purchased_cost( $RP ) {
	
	}

}