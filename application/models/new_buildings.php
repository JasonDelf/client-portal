<?php 
class New_Buildings extends MY_Model
{

	public function New_Buildings() {
		parent::MY_Model();
		$this->table = 'tbl_buildings';
		$this->primary_key = 'id';
	}
	
	public function type_available($building, $statecode) {
		$buildingstate = $building->building_state;
		//change state value to boolean value
		switch ($buildingstate) {
			case 350:
				$bstate = 2;
				break;
			case 351:
				$bstate = 1;
				break;
			case 352:
				$bstate = 4;
				break;
			case 1873:
				$bstate = 8;
				break;
			case 1874:
				$bstate = 16;
				break;
			case 1875:
				$bstate = 32;
				break;
			case 1876:
				$bstate = 64;
				break;
			case 1877:
				$bstate = 128;
				break;
		}		
		if ($statecode >= 128) {
			$options[] = 128;
			$statecode = $statecode - 128;
		}
		if ($statecode >= 64) {
			$options[] = 64;
			$statecode = $statecode - 64;
		}
		if ($statecode >= 32) {
			$options[] = 32;
			$statecode = $statecode - 32;
		}
		if ($statecode >= 16) {
			$options[] = 16;
			$statecode = $statecode - 16;
		}
		if ($statecode >= 8) {
			$options[] = 8;
			$statecode = $statecode - 8;
		}
		if ($statecode >= 4) {
			$options[] = 4;
			$statecode = $statecode - 4;
		}
		if ($statecode >= 2) {
			$options[] = 2;
			$statecode = $statecode - 2;
		}
		if ($statecode >= 1) {
			$options[] = 1;
			$statecode = $statecode - 1;
		}

		if (in_array($bstate, $options)) {
			return true;
		}		
		return false;
	}
}