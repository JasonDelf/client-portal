<?php 
class Inspections extends MY_Model
{

	public function Inspections() {
		parent::MY_Model();
		$this->table = 'inspection';
		$this->primary_key = 'id';
	}

	function get_details_all() {
		$results = $this->get_all();
		
		foreach ($results as $row) {
			//building name
			$this->db->where('id', $row->buildingid);
			$row->building_name = $this->db->get('building')->row()->building_name;
		
			//report type
			$this->db->where('id', $row->reportid);
			$row->report = $this->db->get('report')->row()->report_name;
			
		}
		
		return $results;
	
	}
	
}