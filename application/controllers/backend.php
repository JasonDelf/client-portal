<?php

class Backend extends Controller {

	function Backend()
	{
		parent::Controller();	
	}
	
	function index()
	{
		$this->load->model('new_buildings');
		$this->db->limit(20);
		$buildings = $this->new_buildings->get_all();
		foreach ($buildings as $building) {
			echo $building->building_name.br();
		}


	}

	function getorders() {
		$startdate = "2011-05-15";
		$this->load->model('neworder');
		$this->load->model('buildings');
		$this->load->model('reports');
		$this->load->model('buildingtypes');
		$this->load->model('buildingreportanswers');
		$this->load->model('new_clients');
		
		$this->db->where('created >', $startdate);
		$orders = $this->neworder->get_all();
		
		foreach ($orders as $order) {
			$building = $this->buildings->get($order->buildingid);
			if ($building->building_type != "") {
				$buildingtype = $this->buildingtypes->get($building->building_type)->type;
			} else {
				$buildingtype = "";
			}
			$building->type = $buildingtype;
			if ($building->mgmt_co_id > 0) {
				$building->managementcompany = $this->new_clients->get($building->mgmt_co_id)->name;
			} else {
				$building->managementcompany = "";
			}
			$report = $this->reports->get($order->reporttype);
			$answers = $this->buildingreportanswers->get_many_by('orderid', $order->id );
			$order->answers = $answers;
			$order->building = $building;
			$order->report = $report;
		}
		$data['startdate'] = $startdate;
		$data['orders'] = $orders;

		$this->load->view('outputreports', $data);		
	
	}

	
}

/* End of file backend.php */
/* Location: ./system/application/controllers/backend.php */