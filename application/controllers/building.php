<?php

class Building extends Controller {

	function Building()
	{
		parent::Controller();	
	}
	
	function index()
	{

	}
	
	function select($id='') {
		if(!$this->session->userdata('logged_in')) {
			//load the login screen
			$this->load->view('login');
		} else {
			if ($id == '') {
				redirect('/clients');
			}
			// put this building into the session for all page reference
			$this->session->set_userdata('buildingid', $id);
			redirect('/building/details');
		}	
	}
	
	function add($confirm = 0) {
		$this->load->model('buildings');
		if ($confirm == 0) {
			//bring up form to enter CTS and continue
			$this->load->view('clientaccess/addbuilding');
		
		} else {
			$this->load->model('listitems');
			$plan = $this->input->post('plannumber');
			$building = $this->buildings->get_many_by('building_cts', $plan);
			if ($building) {
				$data['building'] = $building;
				$this->load->view('clientaccess/existingbuilding', $data);
			} else {
				$this->load->view('clientaccess/newbuilding');
			}
		}
	}
	
	function changemanager($buildingid) {
		$this->load->model('buildings');
		$this->load->model('listitems');
		$manager = $this->session->userdata('clientid');
		$update['mgmt_co_id'] = $manager;
		$this->buildings->update($buildingid, $update);
		$building = $this->buildings->get($buildingid);
		$data['state'] = $this->listitems->get_by('id', $building->building_state)->itemvalue;
		$data['building'] = $building;
		$this->load->view('clientaccess/updatebuilding', $data);
	
	}
	function newb($newcts='') {
		if(!$this->session->userdata('logged_in')) {
			$this->load->view('login');
		} else {

			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="dataerror">', '</div>');
			$this->load->model('buildingtypes');
			$this->load->model('neworder');
			$this->load->model('buildings');
			$this->load->model('new_clients');


			$this->form_validation->set_rules('buildingname', 'Building Name', 'required|xss_clean');
			if ($this->input->post('state') == 351) {
				$this->form_validation->set_rules('plannumber', 'CTS Number', 'required|xss_clean');
			} elseif ($this->input->post('state') == 350) {
				$this->form_validation->set_rules('plannumber', 'Strata Plan No', 'required|xss_clean');
			} elseif ($this->input->post('state') == 352 ) {
				$this->form_validation->set_rules('plannumber', 'Plan of Subdivision', 'required|xss_clean');
			} else {
				$this->form_validation->set_rules('plannumber', 'Plan Number', 'required|xss_clean');
			}
			$this->form_validation->set_rules('buildingtype', 'Building Type', 'required|xss_clean');
			$this->form_validation->set_rules('buildingaddress', 'Building Address', 'required|xss_clean');
			$this->form_validation->set_rules('suburb', 'Suburb', 'required|xss_clean');
			$this->form_validation->set_rules('postcode', 'Postcode', 'xss_clean');
			$this->form_validation->set_rules('state', 'State', 'required|xss_clean');
			$this->form_validation->set_rules('lotcount', 'Number of Units', 'required|is_natural_no_zero|xss_clean');
			$this->form_validation->set_rules('constructiondate', 'Construction Year', 'required|xss_clean');
			$this->form_validation->set_rules('correspondname', 'Correspondence To: Name', 'required|xss_clean');
			$this->form_validation->set_rules('correspondemail', 'Correspondence To: Email', 'valid_email|xss_clean');

			$this->form_validation->set_rules('correspondname', 'Correspondence To', 'required|xss_clean');
			$this->form_validation->set_rules('correspondemail', 'Correspondence To: Email', 'valid_email|required|xss_clean');	

			if ($this->form_validation->run() == FALSE)
			{
				$client = $this->session->userdata('clientid');
				$buildingid = $newcts;

				//not submitted, or not valid, so show form again
				$data['pagetitle'] = "Client Services by ".COMPANY." - ".$buildingid;
				$data['newplan'] = $newcts;
				$data['mgmtco'] = $this->new_clients->get($this->session->userdata('clientid'));
				$this->load->view('clientaccess/common/header', $data);
				$this->load->view('clientaccess/propertydetails2', $data);
				$this->load->view('clientaccess/common/footer');
			} else {
				$data['building_name'] = $this->input->post('buildingname');
				$data['building_cts'] = $this->input->post('plannumber');
				$data['building_address'] = $this->input->post('buildingaddress');
				$data['building_suburb'] = $this->input->post('suburb');
				$data['building_state'] = $this->input->post('state');
				$data['building_postcode'] = $this->input->post('postcode');
				$data['building_constructiondate'] = $this->input->post('constructiondate');
				$data['building_lotcount'] = $this->input->post('lotcount');
				$data['building_type'] = $this->input->post('buildingtype');
				$data['mgmt_co_id'] = $this->session->userdata('clientid');
				$data['mgmt_contact'] = $this->input->post('mgmt_contact');
				$data['mgmt_contact_email'] = $this->input->post('mgmt_contact_email');
				$data['mgmt_contact_phone'] = $this->input->post('mgmt_contact_phone');
				$data['buildingmanager'] = $this->input->post('buildingmanager');
				$data['buildingmanagerphone'] = $this->input->post('buildingmanagerphone');
				$data['onsitecontact'] = $this->input->post('onsitecontact');
				$data['onsitecontactphone'] = $this->input->post('onsitecontactphone');
				$data['meetonsite'] = $this->input->post('meetonsite');
				$data['keys_required'] = $this->input->post('keysrequired');
				$data['keydetails'] = $this->input->post('keydetails');
				$data['correspondstratamanager'] = $this->input->post('usestratamanager');
				if ($data['correspondstratamanager'] != 1) {
					$data['correspondname'] = $this->input->post('correspondname');
					$data['correspondphone'] = $this->input->post('correspondphone');
					$data['correspondemail'] = $this->input->post('correspondemail');
				} else {
					$data['correspondname'] = $data['mgmt_contact'];
					$data['correspondphone'] = $data['mgmt_contact_phone'];
					if (($this->input->post('correspondemail') != "") && ($this->input->post('mgmt_contact_email') == "")) {
						$data['correspondemail'] = $this->input->post('correspondemail');
					} else {
						$data['correspondemail'] = $data['mgmt_contact_email'];
					}
				}
				$data['date_entered'] = date("Y-m-d H:i:s");
				$this->buildings->insert($data);				
				$this->session->set_userdata('buildingid', $this->db->insert_id());
				$buildingid = $this->session->userdata('buildingid');
				if ($this->session->userdata('currentordering')==1) {
					redirect('/report/information/');
				} else {
					redirect('/report/details/');		
				}

			}
		}
	
	}
	
	function details() {
		if(!$this->session->userdata('logged_in')) {
			$this->load->view('login');
		} else {
			if ($this->session->userdata('buildingid') == "") {
				redirect('/clients/');
			}				
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="dataerror">', '</div>');
			$this->load->model('buildingtypes');
			$this->load->model('neworder');
			$this->load->model('buildings');
			$this->load->model('new_clients');
			$this->load->helper('states');
			$this->form_validation->set_rules('buildingname', 'Building Name', 'required|xss_clean');
			if ($this->input->post('state') == 351) {
				$this->form_validation->set_rules('plannumber', 'CTS Number', 'required|xss_clean');
			} elseif ($this->input->post('state') == 350) {
				$this->form_validation->set_rules('plannumber', 'Strata Plan No', 'required|xss_clean');
			} elseif ($this->input->post('state') == 352 ) {
				$this->form_validation->set_rules('plannumber', 'Plan of Subdivision', 'required|xss_clean');
			} else {
				$this->form_validation->set_rules('plannumber', 'Plan Number', 'required|xss_clean');
			}
			$this->form_validation->set_rules('buildingtype', 'Building Type', 'required|xss_clean');
			$this->form_validation->set_rules('buildingaddress', 'Building Address', 'required|xss_clean');
			$this->form_validation->set_rules('suburb', 'Suburb', 'required|xss_clean');
			$this->form_validation->set_rules('postcode', 'Postcode', 'xss_clean');
			$this->form_validation->set_rules('state', 'State', 'required|xss_clean');
			$this->form_validation->set_rules('num_units', 'Number of Units', 'required|is_natural_no_zero|xss_clean');
			$this->form_validation->set_rules('constructiondate', 'Construction Year', 'required|xss_clean');
			$this->form_validation->set_rules('correspondname', 'Correspondence To: Name', 'required|xss_clean');
			$this->form_validation->set_rules('correspondemail', 'Correspondence To: Email', 'valid_email|xss_clean');
			$this->form_validation->set_rules('correspondname', 'Correspondence To', 'required|xss_clean');
			$this->form_validation->set_rules('correspondemail', 'Correspondence To: Email', 'valid_email|required|xss_clean');	
			if ($this->form_validation->run() == FALSE) {
				$client = $this->session->userdata('clientid');
				$buildingid = $this->session->userdata('buildingid');
				$building = $this->buildings->get($buildingid );
				$mgmt_co_id = $building->mgmt_co_id;
				$data['mgmtco'] = $this->new_clients->get($mgmt_co_id );
				$data['building'] = $building;
				$data['pagetitle'] = "Client Services by ".COMPANY." - ".$buildingid;
				$this->load->view('clientaccess/common/header', $data);
				$this->load->view('clientaccess/propertydetails', $data);
				$this->load->view('clientaccess/common/footer');
			} else {
				$this->load->model('logfile');
				$data['building_name'] = $this->input->post('buildingname');
				$data['building_cts'] = $this->input->post('plannumber');
				$data['building_address'] = $this->input->post('buildingaddress');
				$data['building_suburb'] = $this->input->post('suburb');
				$data['building_state'] = $this->input->post('state');
				$data['building_postcode'] = $this->input->post('postcode');
				$data['building_constructiondate'] = $this->input->post('constructiondate');
				$data['building_num_units'] = $this->input->post('num_units');
				$data['building_type'] = $this->input->post('buildingtype');
				$data['mgmt_contact'] = $this->input->post('mgmt_contact');
				$data['mgmt_contact_email'] = $this->input->post('mgmt_contact_email');
				$data['mgmt_contact_phone'] = $this->input->post('mgmt_contact_phone');
				$data['buildingmanager'] = $this->input->post('buildingmanager');
				$data['buildingmanagerphone'] = $this->input->post('buildingmanagerphone');
				$data['onsitecontact'] = $this->input->post('onsitecontact');
				$data['onsitecontactphone'] = $this->input->post('onsitecontactphone');
				$data['keys_required'] = $this->input->post('keysrequired');
				$data['keydetails'] = $this->input->post('keydetails');
				$data['meetonsite'] = $this->input->post('meetonsite');
				$this->session->set_userdata('buildingmngr', $this->input->post('buildingmngr'));
				$data['correspondstratamanager'] = $this->input->post('usestratamanager');
				if ($data['correspondstratamanager'] != 1) {
					$data['correspondname'] = $this->input->post('correspondname');
					$data['correspondphone'] = $this->input->post('correspondphone');
					$data['correspondemail'] = $this->input->post('correspondemail');
				} else {
					$data['correspondname'] = $data['mgmt_contact'];
					$data['correspondphone'] = $data['mgmt_contact_phone'];
					if (($this->input->post('correspondemail') != "") && ($this->input->post('mgmt_contact_email') == "")) {
						$data['correspondemail'] = $this->input->post('correspondemail');
					} else {
						$data['correspondemail'] = $data['mgmt_contact_email'];
					}
				}
				$buildingid = $this->session->userdata('buildingid');
				$this->buildings->update($buildingid, $data);				
				$insert['clientid'] = $this->session->userdata('clientid');
				$insert['action'] = "Updated Building ".$buildingid;
				$this->logfile->insert($insert);
				if ($this->session->userdata('currentordering')==1) {
					redirect('/report/information/');
				} else {
					redirect('/report/details/');		
				}
			}
		}
	}
	function search() {
		if(!$this->session->userdata('logged_in')) {
			$this->load->view('login');
		} else {
			$this->load->model('buildings');
			$search_name = $this->input->post('search_name');
			$search_address = $this->input->post('search_address');
			$search_suburb = $this->input->post('search_suburb');
			$search_cts = $this->input->post('search_cts');
			$client = $this->session->userdata('clientid');
			$this->db->like('building_name', $search_name);
			$this->db->like('building_address', $search_address);
			$this->db->like('building_suburb', $search_suburb);
			$this->db->like('building_cts', $search_cts);
			$this->db->where('mgmt_co_id',$client);
			$data['buildings'] = $this->buildings->get_all();
			$data['pagetitle'] = "Client Services by ".COMPANY;
			$this->load->view('clientaccess/common/header', $data);
			$this->load->view('clientaccess/propertylist', $data);
			$this->load->view('clientaccess/common/footer');
		}	
	}
	function confirm() {
		$this->load->model('buildings');
		$this->load->model('buildingtypes');
		$this->load->model('new_clients');
		$buildingid = $this->session->userdata('buildingid');
		$building = $this->buildings->get($buildingid);
		$data['building'] = $building;
		$data['buildingtype'] = $this->buildingtypes->get($building->building_type)->type;

		$management = $this->new_clients->get($this->session->userdata('clientid'));
		$data['management'] = $management;

		$this->load->view('/clientaccess/buildingconfirm', $data); 
	}
	
	function cancel() {
		if(!$this->session->userdata('logged_in')) {
			//load the login screen
			$this->load->view('login');
		} else {
			$this->load->model('neworder');
			$order = $this->session->userdata('currentorder');
			$this->neworder->delete($order);
			$this->session->unset_userdata('currentordering');
			
			redirect('/clients/');
		}		
	}
	
	function reset() {
		if(!$this->session->userdata('logged_in')) {
			//load the login screen
			$this->load->view('login');
		} else {
			if ($this->session->userdata('buildingid') == "") {
				redirect('/clients/');
			}				
			$this->session->unset_userdata('buildingid');
			$this->session->unset_userdata('currentordering');
			
			redirect('/clients/');
		}
	}

	function cancelcheck() {
		$this->load->view('clientaccess/cancelcheck');
	}
	
}

/* End of file building.php */
/* Location: ./system/application/controllers/building.php */