<?php

class Account extends Controller {

	function Account()
	{
		parent::Controller();	
	}
	
	function index()
	{
		if($this->session->userdata('logged_in')) {
			if ($this->session->userdata('accesslevel') == 9) {
				$this->load->model('clientlogin');
				$this->load->library('form_validation');
				$this->form_validation->set_error_delimiters('<div class="dataerror">', '</div>');
	
				$this->form_validation->set_rules('oldpw', 'Current Password', 'required|xss_clean');
				$this->form_validation->set_rules('newpw', 'New Password', 'required|xss_clean');
				$this->form_validation->set_rules('conpw', 'Confirmation Password', 'matches[newpw]|required|xss_clean');

				if ($this->form_validation->run() == FALSE)
				{
					$userid = $this->session->userdata('clientid');
					$data['login'] = $this->clientlogin->get($this->session->userdata('id'));
					$data['message'] = "";
					$this->load->view('password', $data);
				} else {
					$oldpw = $this->input->post('oldpw');
					$newpw = $this->input->post('newpw');
					// get current password
					$current = $this->clientlogin->get($this->session->userdata('id'))->password;
					
					
					if (sha1($oldpw) == $current) {
						$update['password'] = sha1($newpw);
						$this->clientlogin->update($this->session->userdata('id'), $update);
						$data['message'] = "Password updated";
						$this->load->view('password', $data);
					} else {
						echo "no match";
					}
				}
				
			
			} else {
				$this->load->model('clientlogin');
				$this->load->library('form_validation');
				$this->form_validation->set_error_delimiters('<div class="dataerror">', '</div>');
	
				$this->form_validation->set_rules('name', 'Name', 'required|xss_clean');
				$this->form_validation->set_rules('address', 'Address', 'required|xss_clean');
				$this->form_validation->set_rules('suburb', 'Suburb', 'required|xss_clean');
				$this->form_validation->set_rules('state', 'State', 'required|xss_clean');
				$this->form_validation->set_rules('postcode', 'Postcode', 'exact_length[4]|numerical|required|xss_clean');
				$this->form_validation->set_rules('phone1', 'Phone 2', 'numerical|required|xss_clean');
				$this->form_validation->set_rules('phone2', 'Phone 1', 'numerical|xss_clean');
				$this->form_validation->set_rules('fax', 'Fax', 'numerical|xss_clean');
				$this->form_validation->set_rules('email', 'Email', 'valid_email|required|xss_clean');
	
				if ($this->form_validation->run() == FALSE)
				{
					$clientid = $this->session->userdata('clientid');
					$data['login'] = $this->clientlogin->get($this->session->userdata('id'));
					$data['user'] = $this->new_clients->get($clientid);
					$data['message'] = "";
					$this->load->view('account', $data);
				} else {
					$update['name'] = $this->input->post('name');
					$update['address'] = $this->input->post('address');
					$update['suburb'] = $this->input->post('suburb');
					$update['state'] = $this->input->post('state');
					$update['postcode'] = $this->input->post('postcode');
					$update['phone1'] = $this->input->post('phone1');
					$update['phone2'] = $this->input->post('phone2');
					$update['fax'] = $this->input->post('fax');
					$update['email'] = $this->input->post('email');
					
					$clientid = $this->session->userdata('clientid');
					$this->new_clients->update($clientid, $update);
					$data['login'] = $this->clientlogin->get($this->session->userdata('id'));
					$data['user'] = $this->new_clients->get($clientid);
					$data['message'] = "Your details have been updated.";
					$this->load->view('account', $data);
					
				}
			}
		}
	}
	
	function password() {
		if($this->session->userdata('logged_in')) {
			$this->load->model('clientlogin');
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="dataerror">', '</div>');

			$this->form_validation->set_rules('oldpw', 'Current Password', 'required|xss_clean');
			$this->form_validation->set_rules('newpw', 'New Password', 'required|xss_clean');
			$this->form_validation->set_rules('conpw', 'Confirmation Password', 'matches[newpw]|required|xss_clean');
			
			if ($this->form_validation->run() == FALSE)
			{
				$clientid = $this->session->userdata('clientid');
				$data['login'] = $this->clientlogin->get($this->session->userdata('id'));
				$data['message'] = "";
				$this->load->view('password', $data);
			} else {
				$oldpw = $this->input->post('oldpw');
				$newpw = $this->input->post('newpw');
				// get current password
				$current = $this->clientlogin->get($this->session->userdata('id'))->password;
				if (sha1($oldpw) == $current) {
					$update['password'] = sha1($newpw);
					$this->clientlogin->update($this->session->userdata('id'), $update);
					$data['message'] = "Password updated";
					$this->load->view('password', $data);
				} else {
					echo "no match";
				}
			}
		}
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */