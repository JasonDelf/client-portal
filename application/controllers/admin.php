<?php

class Admin extends Controller {

	function Admin()
	{
		parent::Controller();	
	}
	
	function index()
	{
		if(!$this->session->userdata('logged_in')) {
			//load the login screen
			$this->load->view('login');
		} else {
			$data['pagetitle'] = "Client Services by ".COMPANY." :: ADMIN";
			$this->load->view('clientaccess/common/header', $data);
			$this->load->view('admin/dashboard');
			$this->load->view('clientaccess/common/footer');
		}
	}

	function users($action = '') {

		if(!$this->session->userdata('logged_in')) {
			//load the login screen
			$this->load->view('login');
		} else {
			
			$this->load->model('clientlogin');

			$drop_clientid = $this->input->post('clientid');
			
			$data['message'] = "";
			
			//delete client if clientid was submitted
			if($drop_clientid!=''){
				 $this->clientlogin->delete($drop_clientid);
				 $data['message'] = "Drop user successfully";
			}
			 
			$action =  $this->input->post('action');
			if($action=='add'){
				
				 	 	 	 	 	
					$this->load->library('form_validation');

					$this->form_validation->set_rules('username', 'Username', 'required');
					$this->form_validation->set_rules('password', 'Password', 'required');
				    $this->form_validation->set_rules('retype_password', 'Retype Password', 'xss_clean|required|matches[password]|alpha_numeric|sha1');					
					
					$this->form_validation->set_rules('email', 'Email', 'required');
					
					if ($this->form_validation->run() == TRUE)
					{
						$insert['username']		 = $this->input->post('username');
						$insert['fullname']		 = $this->input->post('fullname');					
						$insert['password']		 = sha1($this->input->post('password'));
						
						$insert['email']		 = $this->input->post('email');
						$insert['accesslevel']	 = $this->input->post('accesslevel');
						$insert['clientid']		 =  $this->input->post('clientid');
						$insert['created']		 =  date("Y-m-d H:i:s");
						$insert['modified']		 =  date("Y-m-d H:i:s");						
						
								
						 $this->clientlogin->insert($insert);
						
						$data['message'] = "New user has been inserted successfully";
									
					}

			}
			 
			$data['pagetitle'] = "Client Services by ".COMPANY." :: ADMIN";
			$this->load->view('clientaccess/common/header', $data);
			
			
			
			
			
			$where_user =  "(accesslevel=1 OR accesslevel=5) AND (username<>'')";
			$this->db->where($where_user);
			$this->db->order_by("username", "asc"); 
			

			
			$this->load->model('new_clients');
			
			
			$data['users'] = $this->clientlogin->get_all();
			$this->db->order_by("name", "asc");
			$data['clients'] = $this->new_clients->get_all();
			
			
			$this->load->view('admin/user',$data);
			$this->load->view('clientaccess/common/footer');
		}
	}

	function report($action = '', $id = '')
	{
		if(!$this->session->userdata('logged_in')) {
			//load the login screen
			$this->load->view('login');
		} else {
			$data['pagetitle'] = "Client Services by ".COMPANY." :: ADMIN";
			$this->load->view('clientaccess/common/header', $data);
			$this->load->model('reports');
			if ($action == "") {
				$this->db->where('deleted', 0);
				$this->db->orderby('report_name');
				$data['reports'] = $this->reports->get_all();
				$this->load->view('admin/reportlist', $data);

			} else if ($action == "questions") {
				if ($id == "") {
					$this->db->where('deleted', 0);
					$this->db->orderby('report_name');
					$data['reports'] = $this->reports->get_all();
					$this->load->view('admin/questionentry', $data);			
				} else {
					$this->load->model('buildingreportquestions');
					$data['report'] = $this->reports->get($id);
					$this->db->where('deleted', 0);
					$this->db->orderby('pos');
					$data['questions'] = $this->buildingreportquestions->get_many_by('report', $id);	
					$this->load->view('admin/questionlist', $data);
				}
			} else if ($action == "add") {
				$this->load->library('form_validation');
	
				$this->form_validation->set_rules('report_name', 'Report Name', 'required|xss_clean');

				if ($this->form_validation->run() == FALSE)
				{
					//not submitted, or not valid, so show form again
					$this->load->model('lists');
					$this->load->model('listitems');
					$this->load->helper('states');
					$data['report'] = $this->reports->get($id);
					$this->load->view('admin/reportadd', $data);
				}
				else
				{
					$data = array();
					$data['report_name'] = $this->input->post('report_name');
					$statevalue = 0;
					for ($i = 1; $i <= 8; $i ++) {
						$statevalue += $this->input->post('state'.$i);
					}	
					$data['states'] = $statevalue;
					$data['modified'] = date("Y-m-d h:i:s");
					
					$this->reports->insert($data);
					
					redirect('admin/report');
				}
			} else if ($action == "delete") {
				$this->load->model('reports');
				if ($this->input->post('confirm') == 1) {
					$data = array();
					$data['deleted'] = 1;
					$data['deleteddate'] = date("Y-m-d h:i:s");
					$this->reports->update($id, $data);
					redirect('/admin/report');
				} else {
					$data['report'] = $this->reports->get($id);
					$this->load->view('admin/deleterep', $data);
				}
				

			} else if ($action == "detail") {
				$this->load->library('form_validation');
	
				$this->form_validation->set_rules('report_name', 'Report Name', 'required|xss_clean');

				if ($this->form_validation->run() == FALSE)
				{
					//not submitted, or not valid, so show form again
					$this->load->model('lists');
					$this->load->model('listitems');
					$this->load->helper('states');
					$data['report'] = $this->reports->get($id);
					$this->load->view('admin/reportdetail', $data);
				}
				else
				{
					$data = array();
					$data['report_name'] = $this->input->post('report_name');
					$statevalue = 0;
					for ($i = 1; $i <= 14; $i++) {
						$statevalue += $this->input->post('state'.$i);
					}	
					$data['states'] = $statevalue;
					$data['modified'] = date("Y-m-d h:i:s");
					
					$this->reports->update($id, $data);
					
					redirect('admin/report');
				}
			} else if ($action == "questionedit") {
				$this->load->library('form_validation');
				$this->load->model('buildingreportquestions');
	
				$this->form_validation->set_rules('question', 'Question', 'required|xss_clean');
				$this->form_validation->set_rules('questiontag', 'Question Tag', 'required|xss_clean');
				$this->form_validation->set_rules('questiontype', 'Question Type', 'required|xss_clean');
				$this->form_validation->set_rules('options', 'Options', 'xss_clean');
				$this->form_validation->set_rules('dependson', 'Only Show If', 'xss_clean');
				$this->form_validation->set_rules('dependsif', 'Only Show if Equal', 'xss_clean');

				if ($this->form_validation->run() == FALSE)
				{
					//not submitted, or not valid, so show form again
					$data['report'] = $this->reports->get($this->buildingreportquestions->get($id)->report);
					$data['question'] = $this->buildingreportquestions->get($id);	
					$this->load->view('admin/questionedit', $data);			
				} else {
					// save data
					$data = array();
					$data['question'] = $this->input->post('question');
					$data['questiontag'] = $this->input->post('questiontag');
					$data['questiontype'] = $this->input->post('questiontype');
					$data['questionshort'] = $this->input->post('questionshort');
					$data['options'] = $this->input->post('options');
					$data['required'] = $this->input->post('required');
					$data['dependson'] = $this->input->post('dependson');
					$data['dependsif'] = $this->input->post('dependsif');
					$data['requiredif'] = $this->input->post('requiredif');
					$data['modified'] = date('Y-m-d h:i:s');
					
					$this->buildingreportquestions->update($id, $data);
					$reportid = $this->buildingreportquestions->get($id)->report;
					
					redirect('/admin/report/questions/'.$reportid);
					
				}
			} else if ($action == "questiondelete") {
				$this->load->model('buildingreportquestions');
				if ($this->input->post('confirm') == 1) {
					$reportid = $this->buildingreportquestions->get($id)->report;
					$data = array();
					$data['deleted'] = 1;
					$data['deleteddate'] = date("Y-m-d h:i:s");
					$this->buildingreportquestions->update($id, $data);
					redirect('/admin/report/questions/'.$reportid);
				} else {
					$data['question'] = $this->buildingreportquestions->get($id);
					$this->load->view('admin/deleteq', $data);
				}
			} else if ($action == "addquestion") {
				$this->load->library('form_validation');
				$this->load->model('buildingreportquestions');

				$this->form_validation->set_rules('question', 'Question', 'required|xss_clean');
				$this->form_validation->set_rules('questiontag', 'Question Tag', 'required|xss_clean');
				$this->form_validation->set_rules('questiontype', 'Question Type', 'required|xss_clean');
				$this->form_validation->set_rules('questionshort', 'Question Title', 'required|xss_clean');
				$this->form_validation->set_rules('options', 'Options', 'xss_clean');

				if ($this->form_validation->run() == FALSE)
				{
					//not submitted, or not valid, so show form again
					$data['report'] = $this->reports->get($id);
					$this->load->view('admin/questionadd', $data);			
				} else {
					// save data
					$this->db->select_max('pos');
					$lastpos = $this->db->get('tbl_buildingreportquestions')->row()->pos;
					$nextpos = $lastpos + 1;
					$data = array();
					$data['question'] = $this->input->post('question');
					$data['report'] = $id;
					$data['questiontag'] = $this->input->post('questiontag');
					$data['questiontype'] = $this->input->post('questiontype');
					$data['questionshort'] = $this->input->post('questionshort');
					$data['pos'] = $nextpos;
					$data['options'] = $this->input->post('options');
					$data['required'] = $this->input->post('required');
					$data['modified'] = date('Y-m-d h:i:s');
					$data['created'] = $data['modified'];
					
					$this->buildingreportquestions->insert($data);
					
					redirect('/admin/report/questions/'.$id);
				}
			}
			$this->load->view('clientaccess/common/footer');
		}
	}

	function questions ($action, $data) {
		$this->load->model('buildingreportquestions');
		if ($action == "sort") {
			$items = explode('-', $data);
			foreach ($items as $position => $item) {
				$idval = substr($item, 2);
				$update['pos'] = $position+1;
				$update['modified'] = date("Y-m-d H:i:s");
				$this->buildingreportquestions->update($idval, $update);
			}	
		} elseif ($action == "getanswers") {
			$question = $this->buildingreportquestions->get($data);
			if (!$question) continue;
			if (($question->questiontype == 'singleselect') || ($question->questiontype == 'multiselect')) {
				$options = $this->buildingreportquestions->get($data)->options;
				if ($options) {
					$optionsarray = explode(',',$options);
					foreach ($optionsarray as $row) {
						$optarray[$row] = $row;
					}
				}
				echo form_dropdown('dependsif', $optarray);
			} else if (($question->questiontype == 'textsmall') || ($question->questiontype == 'textmed') || ($question->questiontype == 'textlarge')) {
				$qtype = $question->questiontype;
				$input = array(
					'name'	=> 'dependsif',
				);
				if ($qtype == 'textsmall') {
					$input['size'] = '6';
				} elseif ($qtype == 'textmed') {
					$input['size'] = '20';
				} elseif ($qtype == 'textlarge') {
					$input['size'] = '40';
				}
				
				echo form_input($input);
			}
		
		}
	}
}

/* End of file admin.php */
/* Location: ./system/application/controllers/admin.php */