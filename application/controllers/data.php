<?php

class Data extends Controller {

	function Data()
	{
		parent::Controller();	
	}
	
	function index()
	{
	}

	function buildingtypes($state) {
		$this->load->model('buildingtypes');
		$this->load->helper('states');
				
		$buildingtypes = $this->buildingtypes->get_all();
		$types[''] = "";
		foreach ($buildingtypes as $rowitem) {
			if (type_available($state, $rowitem->states)) {
				$types[$rowitem->id] = $rowitem->type;
			}
		}
		foreach ($types as $key=>$value) {
			echo "<option value=".$key.">".$value."</option>";	
		}
	}	

	function reportdetails() {
		$this->load->library('form_validation');
	
		$this->form_validation->set_rules('report_name', 'Report Name', 'required|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			//not submitted, or not valid, so show form again
			$this->load->model('lists');
			$this->load->model('listitems');
			$this->load->helper('states');
			$data['report'] = $this->reports->get($id);
			$this->load->view('admin/reportdetail', $data);
		}
		else
		{
			$data = array();
			$data['report_name'] = $this->input->post('report_name');
			$statevalue = 0;
			for ($i = 1; $i <= 9; $i ++) {
				$statevalue += $this->input->post('state'.$i);
			}	
			$data['states'] = $statevalue;
			$data['modified'] = date("Y-m-d h:i:s");
			
			$this->reports->update($id, $data);
			
			redirect('admin/report');
		}

	}

	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */