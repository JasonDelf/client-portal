<?php

class Report extends Controller {

	function Report()
	{
		parent::Controller();	
	}
	
	function index()
	{

	}
	
	function selection($confirmed = "")
	{
		if(!$this->session->userdata('logged_in')) {
			//load the login screen
			$this->load->view('login');
		} else {
			if ($this->session->userdata('buildingid') == "") {
				redirect('/clients/');
			}
			if ($confirmed == 0) {
				$this->load->model('buildings');
				$this->load->helper('states');
				$data['pagetitle'] = "Client Services by ".COMPANY;
				$this->load->view('clientaccess/common/header', $data);
				$buildingid = $this->session->userdata('buildingid');
				$building = $this->buildings->get($buildingid);
				$state = $building->building_state;
				$this->load->model('reports');
				$this->db->order_by('report_name');
				$allreports = $this->reports->get_all();
				foreach ($allreports as $row) {
					if (type_available($state, $row->states)) {
						$reports[] = $row;
					}
				}
				$data['reports'] = $reports;
				//print_r($data);
				$this->load->view('clientaccess/reportlist', $data);
				$this->load->view('clientaccess/common/footer');
			} else {
				// It is run when users click on the button 'Continue with Request.
				$this->load->model('reports');
				$this->load->model('neworder');
				$this->load->model('buildings');

				$selections = explode(',',$this->input->post('currentselection'));
			
				$report = array();
				if (is_array($selections)) {
					foreach ($selections as $reportid) {
						$report[] = $this->reports->get($reportid);
					}
				}   else {
					$report[] = $this->reports->get($selections);
				}
				$this->session->set_userdata('selection', $report);
				$buildingid = $this->session->userdata('buildingid');
	
				$building = $this->buildings->get($buildingid);

				$insert['buildingid'] = $buildingid;
				$insert['created'] = date("Y-m-d H:i:s");
				$insert['modified'] = date("Y-m-d h:i:s");
				$insert['keydetails'] = $building->keydetails;
				$insert['keys_required'] = $building->keys_required;
				if ($building->meetonsite == "0") {
					$insert['meetonsite'] = "0";
				} else {
					if ($building->onsitecontact != ""){
						$insert['meetonsite'] = $building->onsitecontact . " - " . $building->onsitecontactphone;
					} elseif ($building->buildingmanager != ""){
						$insert['meetonsite'] = $building->buildingmanager . " - " . $building->buildingmanagerphone;
					} else {
						$insert['meetonsite'] = $building->mgmt_contact . " - " . $building->mgmt_contact_phone;
					}
				}
				foreach ($report as $row) {
					$insert['reporttype'] = $row->id;
					output($insert);
					$this->neworder->insert($insert);
					$orderid[$row->id] = $this->db->insert_id();
				}
				$this->session->set_userdata('orderid', $orderid);

				$this->session->set_userdata('selection', $report);
				//print_r($report);

				redirect("report/selectionmade");
			}
		}
	}
	
	
	function selectionmade() {
		if(!$this->session->userdata('logged_in')) {
			//load the login screen
			$this->load->view('login');
		} else {
			if ($this->session->userdata('buildingid') == "") {
				redirect('/clients/');
				return;
			}
			$report = $this->session->userdata('selection');
			//print_r($report);
			if (empty($report[0])) {
				redirect('report/selection');
				return;
			}

			$this->load->model('reports');
			$this->load->model('buildings');
			$this->load->model('neworder');


			$buildingid = $this->session->userdata('buildingid');

			$orders = $this->session->userdata('orderid');
			$data['orders'] = $orders;
			$data['pagetitle'] = "Client Services by ".COMPANY;
			$this->load->view('clientaccess/common/header', $data);
			$data['buildingname'] = $this->buildings->get($buildingid)->building_name;
			$data['reports'] = $report;
			$this->load->view('clientaccess/reportframe', $data);
			$this->load->view('clientaccess/common/footer');
		}	
	}

	function details()
	{
		if(!$this->session->userdata('logged_in')) {
			//load the login screen
			$this->load->view('login');
		} else {
			if ($this->session->userdata('buildingid') == "") {
				//redirect('/clients/');
			}
			
			$this->load->model('buildingplans');
			$this->load->model('buildings');
			$this->load->model('neworder');
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="dataerror">', '</div>');

			$this->form_validation->set_rules('plans', 'Building Plans', 'required|xss_clean');
			$buildingid = $this->session->userdata('buildingid');
		
			if ($this->form_validation->run() == FALSE)
			{
				$data['building'] = $this->buildings->get($buildingid);
				//not submitted, or not valid, so show form again
				$data['pagetitle'] = "Client Services by ".COMPANY." - ".$buildingid;
				$this->load->view('clientaccess/common/header', $data);
				$this->db->orderby('submitted DESC');
				$data['buildingplans'] = $this->buildingplans->get_many_by('buildingid', $buildingid);
				$this->load->view('clientaccess/buildingplans', $data);
				$this->load->view('clientaccess/common/footer');
			} else {
				if ($this->input->post('back')=="back") {
					redirect("building/details");
					return;
				}
				$back = 0;
				
				if ($this->input->post('submit')=="upload") {
					//submitted and valid, so save the changes and continue
					$config['upload_path'] = './library/data/plans/';
					$config['allowed_types'] = 'zip|pdf|jpg|gif|png'; // Note: a bug in this library. non-mime file extension must be put before mime extension.
					$config['max_size']	= '8000';
	
					$this->load->library('upload', $config);
					
					if ($this->input->post('plantitle') == '') {
						$plantitle = "Building Plan";
					} else {
						$plantitle = $this->input->post('plantitle');
					}
		
					if ( ! $this->upload->do_upload('uploadplans'))
					{
						$error = array('error' => $this->upload->display_errors());
						print_r($error);
		//				redirect('report/details/'.$id);
		//				$this->load->view('upload_form', $error);
					}
					else
					{
						$result = array('upload_data' => $this->upload->data());
						$uploaddata = $result['upload_data'];
								
						$data2['buildingid'] = $buildingid;
						$data2['plantitle'] = $plantitle;
						$data2['filename'] = $uploaddata['file_name'];
						$data2['filesize'] = $uploaddata['file_size'];
						$data2['submitted'] = date("Y-m-d h:i:s");
	
						$this->buildingplans->insert($data2);
							
						//save filename by a session variable
						$filename_item = array('filename'  =>  $uploaddata['file_name']);
						$this->session->set_userdata($filename_item);	
					}
				}

				$this->session->set_userdata('building plans', $this->input->post('plans'));
		//		$data['buildingplans'] = $this->input->post('plans');
		//		$data['modified'] = date("Y-m-d h:i:s");
		//		$orderid = $this->session->userdata('currentorder');
		//		echo $orderid;				
		//		$this->neworder->update($orderid, $data);
				
				$reportid = $this->session->userdata('reporttype');
				
				if ($back == 1) {
					$this->session->set_userdata('currentordering', '1');
					redirect("report/information/".$id."/".$report);
				} else {
					redirect ("/report/selection/");
				}
					
			}
		}
	}

	function information($report="", $order="")
	{
		if(!$this->session->userdata('logged_in')) {
			//load the login screen
			$this->load->view('login');
		} else {
			if ($this->session->userdata('buildingid') == "") {
				redirect('/clients/');
			}				
			$this->load->model('neworder');
			//update the orders table to include this report now for the current building
			$reportid = $this->session->userdata('currentorder');
			$update['reporttype'] = $report;
			$this->neworder->update($reportid, $update);
		

			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="dataerror">', '</div>');
			$this->load->model('buildingreportquestions');
			$reportquestions = $this->buildingreportquestions->get_many_by('report', $report);
			foreach ($reportquestions as $question) {
				$rules = "";
				if ($question->required) {
					$rules = "required|";
				}
				if ($question->answertype == 'email') {
					$rules .= 'valid_email|';
				} elseif ($question->answertype == 'alpha') {
					$rules .= 'alpha|';
				} elseif ($question->answertype == 'alphanumeric') {
					$rules .= 'alphanumeric|';
				} elseif ($question->answertype == 'numeric') {
					$rules .= 'numeric|';
				}

				$rules .= "xss_clean";
				$this->form_validation->set_rules($question->questiontag, $question->questionshort, $rules);
			}
			$this->form_validation->set_rules('orderid','Order No', 'required');

			//check for "required if" fields
			foreach ($reportquestions as $question) {
				//only keep if 'required if' = 1 then check to see if required
				if ($question->requiredif == 1) {
					$dependson = $question->dependson;
					$dependsif = $question->dependsif;
					$dependsquestion = $this->buildingreportquestions->get($dependson)->questiontag;
					$answer = $this->input->post($dependsquestion);
					if ($answer == $dependsif) {
						$this->form_validation->set_rules($question->questiontag, $question->questionshort, 'required|xss_clean');
					}
				}
			}
			
			$this->load->model('reports');
			$this->load->model('buildings');
			$this->load->model('buildingreportquestions');
			$this->load->model('buildingreportquestiontips');


			if ($this->form_validation->run() == FALSE)
			{
				//invalid submission so return the validation errors to display
				if (isset($_POST['submit'])) {
					echo validation_errors();
				
				} else {
					//not submitted so show form 
					$buildingid = $this->session->userdata('buildingid');

					$this->load->model('buildingreportanswers');
					$this->db->where('orderid', $order);
					$answers = $this->buildingreportanswers->get_many_by('buildingid', $buildingid);
					$storedanswers = array();
					foreach ($answers as $answer) {
						$storedanswers[$answer->question] = $answer->answer;
					}
					$data['storedanswers'] = $storedanswers;

					$data['report'] = $this->reports->get($report);
					$this->db->orderby('pos');
					$reportquestions = $this->buildingreportquestions->get_many_by('report', $report); 
					$tips = array();
					foreach ($reportquestions as $question) {
						$tips[$question->id] = $this->buildingreportquestiontips->get_many_by('question', $question->id);
					}
					$data['request'] = $this->neworder->get($order)->request;
					$data['requestby'] = $this->neworder->get($order)->requestby;
					$data['reportquestions'] = $reportquestions;
					$data['tips'] = $tips;
					$data['order'] = $order;
					$this->load->view('clientaccess/reportinfo', $data);
				}
			} else {
				$back = 0;
				$buildingid = $this->session->userdata('buildingid');
				$this->load->model('buildingreportanswers');
				$this->load->model('neworder');
				$orderid = $this->input->post('orderid');				

				$update['request'] = $this->input->post('request'.$orderid);
				if ($this->input->post('requestby'.$orderid) > "") {
					$update['requestby'] = $this->input->post('requestby'.$orderid);
				} 
				$update['buildingplans'] = $this->session->userdata('building plans');												

				$this->neworder->update($orderid, $update);

				//store updated information
				$data['buildingid'] = $buildingid;
				$data['created'] = date("Y-m-d h:i:s");
				$data['modified'] = $data['created'];
				$data['orderid'] = $orderid;
				//$data['request'] = $this->input->post('request');
			
				$availq = $this->buildingreportquestions->get_many_by('report', $report);
				$q_array = array();
				foreach ($availq as $q) {
					$q_array[] = $q->questiontag;
				}
				//clear old information
				$this->buildingreportanswers->delete_many_by(array("orderid"=>$orderid));

				foreach ($_POST as $key=> $value) {
					if ($key != "orderid") {
						$data['question'] = $key;
						$data['answer'] = $value;
						if (in_array($key, $q_array))  {
							$this->buildingreportanswers->insert($data);
						}
					}
				}
				
				echo "Details saved";

			}

		}
	}
	
	function cancel($orderid, $confirmed = '') {
		$this->load->model('neworder');
		$thisorder = $this->neworder->get($orderid);
		if ($confirmed == 1) {
			$currentreports = $this->session->userdata('selection');
			$orderids = $this->session->userdata('orderid');
			foreach ($currentreports as $current) {
				if ($current->id != $thisorder->reporttype) {
					$temp[] = $current;
				}
			}
			foreach ($orderids as $key => $value) {
				output($key);
				output($value);
				output($orderid);
				echo "======".br();
				if ($orderid != $value ) {
					$temp2[$key] = $value;
				}
			}
			$this->session->set_userdata('selection', $temp);
			$this->session->set_userdata('orderid', $temp2);
			$this->neworder->delete($orderid);
		} else {
			$this->load->model('reports');
			$order = $this->neworder->get($orderid);
			$report = $this->reports->get($order->reporttype);
			$data['order'] = $order;
			$data['report'] = $report;
			$this->load->view('clientaccess/reportcancel', $data);
		}
	
	}

	function cancel2($orderid, $confirmed = '') {
		$this->load->model('neworder');
		$thisorder = $this->neworder->get($orderid);
		if ($confirmed == 1) {
			$currentreports = $this->session->userdata('selection');
			$orderids = $this->session->userdata('orderid');
			foreach ($currentreports as $current) {
				if ($current->id != $thisorder->reporttype) {
					$temp[] = $current;
				}
			}
			foreach ($orderids as $key => $value) {
				output($key);
				output($value);
				output($orderid);
				echo "======".br();
				if ($orderid != $value ) {
					$temp2[$key] = $value;
				}
			}
			$this->session->set_userdata('selection', $temp);
			$this->session->set_userdata('orderid', $temp2);
			$this->neworder->delete($orderid);
		} else {
			$this->load->model('reports');
			$order = $this->neworder->get($orderid);
			$report = $this->reports->get($order->reporttype);
			$data['order'] = $order;
			$data['report'] = $report;
			$this->load->view('clientaccess/reportcancel2', $data);
		}
	
	}

	
	function confirm($orderid) {
		$this->load->model('buildingreportanswers');
		$this->load->model('buildingreportquestions');
		$this->load->model('neworder');

		$order = $this->neworder->get($orderid);	
		$data['order'] = $order;
		$answers = $this->buildingreportanswers->get_many_by('orderid', $orderid);
		foreach ($answers as $answer) {
			$this->db->where('report', $order->reporttype);
			$questions[$answer->id] = $this->buildingreportquestions->get_by('questiontag', $answer->question)->question;
		}

		$data['answers'] = $answers;
		if (isset($questions)) {
			$data['questions'] = $questions;
		} else {
			$data['questions'] = "";
		}
		$this->load->view('/clientaccess/reportconfirm', $data);
	}
	
	
	private function sendEmailAdmin($client_id,$error_code,$error_path,$building_data,$order_data){
		$this->load->library('email');
		$config['protocol']    = 'smtp';
		$config['smtp_host']    = 'solutionsie-com-au.mail.protection.outlook.com';
		$config['smtp_port']    = '25';
		$config['smtp_timeout'] = '7';
		$config['smtp_user']    = 'it@solutionsie.com.au';
		$config['smtp_pass']    = 'Zxcvbnm100!';
		$config['charset']    = 'utf8';
		$config['newline']    = "\r\n";
		$config['crlf'] = "\r\n";
		$config['mailtype'] = 'html'; // or html
	

		$this->email->initialize($config);
			
			
		$message = "<html><body>Error occured for $client_id.<br/><br/>
		Note the session data:<br/>";
		$session_data=$this->session->all_userdata();
		foreach ($session_data as $key => $value){
			$message=$message."$key = $value<br/>";
		}
		$message=$message."<br/><br/>";
		foreach ($building_data as $key => $value){
			$message=$message."$key = $value<br/>";
		}
		$message=$message."<br/><br/>";
		foreach ($order_data as $key => $value){
			$message=$message."$key = $value<br/>";
		}
		$message=$message."<br/><br/>
		Error path = $error_path";
		$subject = 'Error report for '.$client_id." - Error code:".$error_code;

		$this->email->from("it@solutionsie.com.au");
		$this->email->subject($subject);
		$this->email->to("it@solutionsie.com.au");
		$this->email->message($message ."</body></html>");
		$this->email->send();
	}
	
	function delivery()
	{
		if(!$this->session->userdata('logged_in')) {
			//load the login screen
			$this->load->view('login');
		} else {
			if ($this->session->userdata('buildingid') == "") {
				redirect('/clients/');
			}				
			$this->load->model('reports');
			$this->load->model('buildings');
			$this->load->model('neworder');
			$this->load->model('new_clients');
			$this->load->model('buildingreportanswers');
			$this->load->model('buildingtypes');

			$building = $this->buildings->get($this->session->userdata('buildingid'));
			$orders = $this->session->userdata('orderid');
// 			$orders = array();

			if(empty($orders) || empty($building)){
				$this->sendEmailAdmin($this->session->userdata('clientid'),"480","report/delivery",$building,$orders);
				$this->session->set_userdata('loginmsg', 'An error occured!! Please try again!<br/>If problem persists, please contact SIE( mention error code 480)');
				redirect('/clients/error');
			}else{
				foreach ($orders as $order) { 
					$oneorder = $this->neworder->get($order);
					$report[$order] = $this->reports->get($oneorder->reporttype);
					$orderdetails[] = $oneorder;
				}
				$data['pagetitle'] = "Client Services by ".COMPANY;
				$data['orderdetails'] = $orderdetails;
				$data['orders'] = $orders;
				$data['report'] = $report;
				$this->load->view('clientaccess/common/header', $data);
				$data['building'] = $building;
				$data['management'] = $this->new_clients->get($this->session->userdata('clientid'));
	
				
				$this->session->set_userdata('currentordering', '1');
				$this->load->view('clientaccess/delivery', $data);
				$this->load->view('clientaccess/common/footer');
			}
		}
	}
	
	function delivered($id='', $report='')
	{
		if(!$this->session->userdata('logged_in')) {
			//load the login screen
			$this->load->view('login');
		} else {

			if ($this->session->userdata('buildingid') == "") {
				redirect('/clients/');
			}	
			
			$this->load->model('reports');
			$this->load->model('buildings');
			$this->load->model('neworder');
			$this->load->model('new_clients');
			$this->load->model('buildingreportanswers');
			$this->load->model('buildingtypes');
			$this->load->model('buildingreportquestions');
			$this->load->model('buildingtypes');
			$this->load->model('logfile');

			$building = $this->buildings->get($this->session->userdata('buildingid'));

			$orders = $this->session->userdata('orderid');

			foreach ($orders as $order) {
				$oneorder = $this->neworder->get($order);
				$report[$order] = $this->reports->get($oneorder->reporttype);
				$orderdetails[] = $oneorder;
				$answers[$order] = $this->buildingreportanswers->get_many_by('orderid', $order);
				foreach ($answers[$order] as $answer) {
					$this->db->where('report', $oneorder->reporttype);
					$questions[$answer->id] = $this->buildingreportquestions->get_by('questiontag', $answer->question)->question;
				}
			}
			$buildingid = $this->session->userdata('buildingid');
			$building = $this->buildings->get($buildingid);
			$data['building'] = $building;
			$buildingtype = $this->buildingtypes->get($building->building_type)->type;
	
			$management = $this->new_clients->get($building->mgmt_co_id);

			$this->load->library('email');
			$config['protocol']    = 'smtp';
			$config['smtp_host']    = 'solutionsie-com-au.mail.protection.outlook.com';
			$config['smtp_port']    = '25';
			$config['smtp_timeout'] = '7';
			$config['smtp_user']    = 'it@solutionsie.com.au';
			$config['smtp_pass']    = 'Zxcvbnm100!';
			$config['charset']    = 'utf8';
			$config['newline']    = "\r\n";
			$config['crlf'] = "\r\n";
			$config['mailtype'] = 'html'; // or html
		


			$this->email->initialize($config);
			
			
			$message = "<html><head>";
			$message .= "<style type='text/css'>label{float:left; width:170px;font-weight: bold;margin-right: 10px;height: auto;}div{height: auto;clear:both; border-bottom: 1px solid #000;}</style></head><body>";
			$message .= "<h2>Online Order</h2>";
			
			$message .= "<h3>SUMMARY</h3>";
			foreach ($orderdetails as $od) {
				$message .= "<strong>".$od->request.":</strong>".nbs(4).$report[$od->id]->report_name."<br />";
			}
			$message .= br(2);
			$message .= "<h3>Building Details</h3>";
			$message .= "<table width=500px border=1 cellspacing=0 cellpadding=3>";
			$message .= "<tr><th>Building Name</th><td>".$building->building_name."</td></tr>";
			$message .= "<tr><th>CTS/SP Number</th><td>".$building->building_cts."</td></tr>";
			$message .= "<tr><th>Address</th><td>".$building->building_address."</td></tr>";
			$message .= "<tr><th>Town.Suburb</th><td>".$building->building_suburb."</td></tr>";
			$message .= "<tr><th>Postcode</th><td>".$building->building_postcode."</td></tr>";
			$message .= "<tr><th>CTS/SP Number</th><td>".$building->building_cts."</td></tr>";
			$message .= "<tr><th>Construction Year</th><td>".$building->building_constructiondate."</td></tr>";
			$message .= "<tr><th>Building Type</th><td>".$buildingtype."</td></tr>";
			$message .= "<tr><th>No. of Units</th><td>".$building->building_num_units."</td></tr>";
			if ($this->session->userdata('buildingmngr')==1) {
				$message .= "<tr><th>Building Manager</th><td>".$building->buildingmanager." - ".$building->buildingmanagerphone."</td></tr>";
			}
			if ($building->meetonsite==1) {
				$message .= "<tr><th>Onsite Representative</th><td>".$building->onsitecontact." - ".$building->onsitecontactphone."</td></tr>";
			}
			
			$message .= "<tr><th>&nbsp;</th><td>&nbsp;</td></tr>";

			$message .= "<tr><th>Management</th><td>".$management->name."</td></tr>";
			$message .= "<tr><th>Management Contact</th><td>".$building->mgmt_contact."</td></tr>";
			if ($building->mgmt_contact_phone > "") {
				$mgmtphone = " / ".$building->mgmt_contact_phone;
			} else {
				$mgmtphone = "";
			}
			if ($building->mgmt_contact_email > "" && $management->email != $building->mgmt_contact_email) {
				$mgmtemail = " / ".$building->mgmt_contact_email;
			} else {
				$mgmtemail = "";
			}
			$message .= "<tr><th>Phone</th><td>".$management->phone1.$mgmtphone."</td></tr>";
			$message .= "<tr><th>Fax</th><td>".$management->fax."</td></tr>";
			$message .= "<tr><th>Address</th><td>".$management->address."</td></tr>";
			$message .= "<tr><th>Town/Suburb</th><td>".$management->suburb."</td></tr>";
            		if ($management->state == "350") {
                		$message .= "<tr><th>State</th><td>NSW</td></tr>";
            		} else if ($management->state == "351") {
                		$message .= "<tr><th>State</th><td>QLD</td></tr>";
            		} else {
				$message .= "<tr><th>State</th><td>".$management->state."</td></tr>";
            		}
			$message .= "<tr><th>Postcode</th><td>".$management->postcode."</td></tr>";
			$message .= "<tr><th>Email</th><td>".$management->email.$mgmtemail."</td></tr>";
			$message .= "</table>";
			$message .= "<br><br>";
	
			$omsg = "";
			$qmsg = "";
			foreach ($orderdetails as $od) {
				$ordermsg = "<h4>".$report[$od->id]->report_name." (".$od->request.")</h4>";
				$ordermsg .= "<table width=500px border=1 cellspacing=0 cellpadding=3>";
				if ($od->requestby == "") {
					$requestby = "no set date";
				} else {
					$requestby = $od->requestby;
				}
				$ordermsg .= "<tr><th>Request By:</th><td>".$requestby."</td></tr>";
				switch ($od->buildingplans) {
					case "usefiled":
						$plans = "Use plans already on file";
						break;
					case "electronic":
						$plans = "We will send through electronic copies to ".COMPANY;
						break;
					case "sendthrough":
						$plans = "We will fax or email plans to ".COMPANY;
						break;
					case "noplans":
						$plans = "We request ".COMPANY." to obtain plans at our expense";
						break;
					default:
						$plans = "Use plans already on file";
						break;
				}
				$ordermsg .= "<tr><th>Building Plans</th><td>".$plans."</td></tr>";
				if ($building->meetonsite == "0") {
					$meetonsite = "No meeting required";
				} else {
					$meetonsite = $od->meetonsite;
				} 	
				$ordermsg .= "<tr><th>MOS Contact</th><td>".$meetonsite."</td></tr>";
				if ($building->keys_required == 0) 
				{ 
					$keyr = "No";
				} 
			      	elseif ($building->keys_required == 1) 
				{
					$keyr = "Yes";
				}
				$ordermsg .= "<tr><th>Access Keys Required</th><td>".$keyr."</td></tr>";
				if ($building->keydetails != "") { 
					$keyd = $building->keydetails;
				} else {
					$keyd = "-";
				}
				$ordermsg .= "<tr><th>Key Details</th><td>".$keyd."</td></tr>";
				foreach ($answers[$od->id] as $rowitem) {
					if ($rowitem->question != "submit") {
						$this->db->where('report', $report[$od->id]->id);
						$question = $this->buildingreportquestions->get_by('questiontag',$rowitem->question)->question;
						if ($rowitem->answer != "") {
							$answer = $rowitem->answer;
						} else { 
							$answer = "-";
						}
						$ordermsg .= "<tr><th>".$question."</th><td>".$answer."</td></tr>";
						if ($answer=="Appoint Solutions in Engineering" && ($report[$od->id]->report_name=="FIRE SAFETY REPORT - INITIAL" ||
							$report[$od->id]->report_name=="FIRE SAFETY REPORT - ANNUAL")) {
							$ordermsg .= "<tr><th>Evacuation Coordinator</th><td>Yes - At additional Cost</td></tr>";
						}
					}	
				}
				$ordermsg .= "</table>";
				$ordermsg .= "<br><br>";

				if ($od->request == "Quote"){
					$qmsg .= $ordermsg;
				} else {
					$omsg .= $ordermsg;
				}
			}
			$from_name = "SIE Online Portal";

			if (trim($omsg) != ""){
				$from = "orders@solutionsie.com.au";
			} else {
				$from = "quotes@solutionsie.com.au";
			}
			$subject = 'Online Ordering from '.$management->name;

			$filename  = $this->session->userdata('filename');
			$file_path = 'E:/rgwwwroot/MainSite/library/data/plans/';	
			
			$this->load->library('email');

			$this->email->from($from, $from_name);

			if ($filename != ""){
				$this->email->attach($file_path.$filename);
			}

			if (trim($management->email) != "") {
				$to = $management->email;
				$this->email->subject($subject);
				$this->email->to($to);
				$this->email->bcc("it@solutionsie.com.au");
				$this->email->message($message . $qmsg . $omsg . "</body></html>");
				$this->email->send();
				
			}
	
			if (trim($omsg) != ""){
				$to = "orders@solutionsie.com.au";
				$this->email->to($to);
				$this->email->bcc("it@solutionsie.com.au");
				$this->email->subject($subject);
				$this->email->message($message . $omsg . "</body></html>");
				$this->email->send();
			}
			if (trim($qmsg) != "") {
				$to = "quotes@solutionsie.com.au";
				$this->email->to($to);
				$this->email->bcc("it@solutionsie.com.au");
				$this->email->subject($subject);
				$this->email->message($message . $qmsg . "</body></html>");
				$this->email->send();
			}
			//update log file
			$insert['clientid'] = $this->session->userdata('clientid');
			$insert['action'] = "Ordered reports/quotes";
			$rowid = $this->logfile->insert($insert);

			redirect('/report/complete/');
		}
	}

	function complete() {
		if(!$this->session->userdata('logged_in')) {
			//load the login screen
			$this->load->view('login');
		} else {
			if ($this->session->userdata('buildingid') == "") {
				redirect('/clients/');
			}	
			if ($this->session->userdata('orderid') == "") {
				redirect('report/selection');
			}
			$this->load->model('reports');
			$this->load->model('neworder');
			$this->load->model('buildings');
			$this->load->model('new_clients');
			$data['pagetitle'] = "Client Services by ".COMPANY;
			$this->load->view('clientaccess/common/header', $data);
			
			$data['building'] = $this->buildings->get($this->session->userdata('buildingid'));
			$data['client'] = $this->new_clients->get($this->session->userdata('clientid'));

			$orders = $this->session->userdata('orderid');
			foreach ($orders as $order) {
				$oneorder = $this->neworder->get($order);
				$reports[$order] = $this->reports->get($oneorder->reporttype)->report_name;
			}
			$data['reports'] = $reports;

			//save filename by a session variable
			$filename_item = array('filename'  =>  "");
			$this->session->set_userdata($filename_item);
			$building_item = array('buildingid'  =>  "");
			$this->session->set_userdata($building_item);

			$this->load->view('clientaccess/completed', $data);
			$this->load->view('clientaccess/common/footer');
		}
	}
}

/* End of file report.php */
/* Location: ./system/application/controllers/report.php */
