<?php

class Services extends Controller {

	function Services()
	{
		parent::Controller();	
	}
	
	function index()
	{
		$data['pagetitle'] = COMPANY." &raquo; Services";
		$this->load->view('common/header', $data);
		$this->load->view('common/leftsidebar');
		$this->load->view('services');
		$this->load->view('reportlist');
		$this->load->view('common/footer');
	}
	
	function order() {
		$data['pagetitle'] = COMPANY." &raquo; Services &raquo; Request Quote/Order";
		$this->load->view('common/header', $data);
		$this->load->view('common/leftsidebar');
		$this->load->view('services/order');
		$this->load->view('reportlist');
		$this->load->view('common/footer');
	}
	
	function report($type = "") {
		$data['pagetitle'] = COMPANY." &raquo; Services &raquo; Request Quote/Order";
		$this->load->view('common/header', $data);
		$this->load->view('common/leftsidebar');
		$this->load->view('services/'.$type);
		$this->load->view('reportlist');
		$this->load->view('common/footer');
		
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */