<?php

class Legal extends Controller {

	function Legal()
	{
		parent::Controller();	
	}
	
	function index()
	{
		$data['pagetitle'] = COMPANY." &raquo; Legislation Information";
		$this->load->view('common/header', $data);
		$this->load->view('common/leftsidebar');
		$this->load->view('legislation');
		$this->load->view('reportlist');
		$this->load->view('common/footer');
	}
	
	function privacy() {
		$data['pagetitle'] = COMPANY." &raquo; Privacy Policy";
		$this->load->view('common/header', $data);
		$this->load->view('common/leftsidebar');
		$this->load->view('legal/privacy');
		$this->load->view('common/footer');
	}

	function tandc() {
		$data['pagetitle'] = COMPANY." &raquo; Supply Terms and Conditions";
		$this->load->view('common/header', $data);
		$this->load->view('common/leftsidebar');
		$this->load->view('legal/tandc');
		$this->load->view('common/footer');
	}
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */