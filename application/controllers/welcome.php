<?php

class Welcome extends Controller {

	function Welcome()
	{
		parent::Controller();	
	}
	
	function index()
	{
		$data['pagetitle'] = COMPANY;
		$this->load->view('common/header', $data);
		$this->load->view('common/leftsidebar');
		$this->load->view('welcome');
		$this->load->view('common/rightsidebar');
		$this->load->view('common/footer');
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */