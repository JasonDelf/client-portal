<?php

class News extends Controller {

	function News()
	{
		parent::Controller();	
	}
	
	function index()
	{
		$data['pagetitle'] = COMPANY." &raquo; Latest News";
		$this->load->view('common/header', $data);
		$this->load->view('common/leftsidebar');
		$this->load->view('news');
		$this->load->view('reportlist');
		$this->load->view('common/footer');
	}
	
	function archives() {
		$data['pagetitle'] = COMPANY." &raquo; Latest News &raquo; Archives";
		$this->load->view('common/header', $data);
		$this->load->view('common/leftsidebar');
		$this->load->view('archives');
		$this->load->view('common/footer');
		
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */