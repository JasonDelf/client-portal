<?php

class Contact extends Controller {

	function Contact()
	{
		parent::Controller();	
	}
	
	function index()
	{
		$data['pagetitle'] = COMPANY." &raquo; Contact Us";
		$this->load->view('common/header', $data);
		$this->load->view('common/leftsidebar');
		$this->load->view('contact');
		$this->load->view('common/footer');
	}
	
	function send()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('firstname', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('lastname', 'Last Name', 'required|xss_clean');
		$this->form_validation->set_rules('email', 'Email', 'valid_email|xss_clean');
		$this->form_validation->set_rules('message', 'Message', 'required|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			echo validation_errors();
		}
		else
		{
			if ($this->input->post('confirm') != "") continue;
			
			$this->load->model('contacts');
			
			$data['firstname'] = $this->input->post('firstname');
			$data['lastname'] = $this->input->post('lastname');
			$data['company'] = $this->input->post('company');
			$data['email'] = $this->input->post('email');
			$data['phone'] = $this->input->post('phone');
			$data['message'] = $this->input->post('message');
			
			$this->contacts->insert($data);

			$this->load->library('email');
			$config['mailtype'] = 'html';
			$config['protocol'] = 'mail';
			$config['wordwrap'] = FALSE;
			$config['charset'] = 'iso-8859-1';
			$config['wrapchars'] = 300;
			$this->email->initialize($config);
			
			
			$message = "";
			$message = "<html><head>";
			$message .= "<style type='text/css'>label{float:left; width:170px;font-weight: bold;margin-right: 10px;height: auto;}div{height: auto;clear:both; border-bottom: 1px solid #000;}</style></head><body>";
			$message .= "<h2>Website Contact</h2>";
			$message .= "<label>First Name</label>".$data['firstname'].br();
			$message .= "<label>Last Name</label>".$data['lastname'].br();
			$message .= "<label>Company</label>".$data['company'].br(2);

			$message .= "<label>Email</label>".$data['email'].br();
			$message .= "<label>Phone</label>".$data['phone'].br(2);

			$message .= "<label>Message</label>".$data['message'].br();

			$message .= "</body></html>";
			
			$to      = "enquiries@solutionsinengineering.com";
			$subject = 'Website Contact';
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: Solutions in Engineering Contact' . "\r\n";
			$headers .= 'Bcc: dath@solutionsinengineering.com' . "\r\n";

			mail($to, $subject, $message, $headers);

			echo "Message sent...";
		}

	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */