<?php

class Insurance extends Controller {

	function Insurance()
	{
		parent::Controller();	
	}
	
	function professional()
	{
		$data['pagetitle'] = COMPANY." &raquo; Professional Indemnity Insurance";
		$this->load->view('common/header', $data);
		$this->load->view('common/leftsidebar');
		$this->load->view('insurance/professional');
		$this->load->view('common/footer');
	}

	function publicliability()
	{
		$data['pagetitle'] = COMPANY." &raquo; Products & Public Liability Insurance";
		$this->load->view('common/header', $data);
		$this->load->view('common/leftsidebar');
		$this->load->view('insurance/publicliability');
		$this->load->view('common/footer');
	}

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */