<?php

class Documents extends Controller {

	function Documents()
	{
		parent::Controller();	
	}
	
	function index()
	{
		$data['pagetitle'] = COMPANY." &raquo; Other Documents";
		$this->load->view('common/header', $data);
		$this->load->view('common/leftsidebar');
		$this->load->view('documents');
		$this->load->view('reportlist');
		$this->load->view('common/footer');
	}
	
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */