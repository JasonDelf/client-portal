<?php

class Help extends Controller {

	function Help()
	{
		parent::Controller();	
	}
	
	function page($pagenumber)
	{
		if(!$this->session->userdata('logged_in')) {
			//load the login screen
			$this->load->view('login');
		} else {
			$this->load->model('helpfile');
			$data['help'] = $this->helpfile->get($pagenumber);
			$this->load->view('clientaccess/help/help', $data);
		}
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */