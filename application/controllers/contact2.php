<?php

class Contact2 extends Controller {

	function Contact2()
	{
		parent::Controller();	
	}
	
	function index()
	{
		$data['pagetitle'] = COMPANY." &raquo; Contact Us";
		$this->load->view('common/header', $data);
		$this->load->view('common/leftsidebar');
		$this->load->view('contact2');
		$this->load->view('common/footer');
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */