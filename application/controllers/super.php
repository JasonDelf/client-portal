<?php

class Super extends Controller {

	function Super()
	{
		parent::Controller();	
	}
	
	function index()
	{
		$this->load->model('clientreport');
		$this->load->view('super/xml');
	}
	
	function table($modelname) {
		$this->load->model($modelname);
		//$this->db->where('deleted', 0);
		$this->db->order_by('id');
		if ($modelname == 'user') {
			$data['rows'] = $this->$modelname->get_details_all()->result();	
		} else {
			$data['rows'] = $this->$modelname->get_details_all('id','asc','');
		}
		$data['modelname'] = $modelname;
		$this->load->view('super/xml', $data);
	}
	
	function details($modelname, $id) {
		$this->load->model($modelname);
		$data['rows'] = $this->$modelname->get($id);
		$data['modelname'] = $modelname;
		$this->load->view('super/xml2', $data);
	}
	
	function login() {
		$this->load->library('simplelogin');
		$this->load->model('users');

		$username = $_POST['username'];
		$password = $_POST['password'];
	
		if($this->simplelogin->login($username, $password)) {
			$user = $this->users->get($this->session->userdata('clientid'));
			$this->session->set_userdata('clientname', $user->fullname);
			//logged in ok, so also need to load the views per page into session variable
			$response = array(
				'logged' => true,
				'name' => $user->fullname,
				'id' => $user->id
			);
			echo json_encode($response);
		} else {
			$response = array(
				'logged' => false,
				'message' => 'Invalid Username and/or Password'
			);
			echo json_encode($response);
		}
		
	}
		
	function inspections($inspectorid) {
		$this->load->model('inspections');
		
		$json   = array();
		$this->db->where('submitted', 0);
		$this->db->where('last_submitted', '0000-00-00 00:00:00');
		$this->db->where('inspectorid', $inspectorid);
		$jobstodo = $this->inspections->get_details_all();
		
		foreach ($jobstodo as $job) {
			$json[] = array(
				'buildingid'	=> $job->buildingid,
				'auditid'		=> $job->auditid,
				'reportid'		=> $job->reportid,
				'job_number'	=> $job->job_number	,
				'building'		=> $job->building_name,
				'report'		=> $job->report
			);
		}
 
		header("Content-Type: text/json");
		echo json_encode($json ); 

	}

}

/* End of file super.php */
/* Location: ./system/application/controllers/super.php */