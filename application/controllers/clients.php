<?php

class Clients extends Controller {

	function Clients()
	{
		parent::Controller();	
	}
	
	function index()
	{
		if(!$this->session->userdata('logged_in')) {
			//load the login screen
			$data['message'] = $this->session->userdata('loginmsg');
			$this->load->view('clientaccess/login', $data);
			$this->session->unset_userdata('loginmsg');
		} else {
			if (($this->session->userdata('accesslevel') == 1) || ($this->session->userdata('accesslevel') == 5)) {
				$data['pagetitle'] = "Client Services by ".COMPANY;
				$this->load->view('clientaccess/common/header', $data);
	
				$this->load->model('buildingquestions');
				$this->load->model('buildinganswers');
				$this->load->model('buildings');
				$client = $this->session->userdata('clientid');
				$data['client'] = $client;
				$data['buildings'] = $this->buildings->get_many_by('mgmt_co_id',$client );
				$this->session->set_userdata('currentordering',0);
				$this->load->view('clientaccess/propertylist', $data);
				
				$this->load->view('clientaccess/common/footer');
			} elseif ($this->session->userdata('accesslevel') == 9) {
				redirect('/admin');
			}			
			
		}
	}
	
	function error()
	{
			$data['message'] = $this->session->userdata('loginmsg');
			$this->load->view('clientaccess/login', $data);
			$this->load->library('simplelogin');
			$this->simplelogin->logout();
	}
	
	
	function login()
	{
		$this->load->library('simplelogin');
		$this->load->library('form_validation');
		$this->load->model('new_clients');
		$this->load->model('users');
		$this->load->model('logfile');


		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == false) {
			redirect('');			
		} else {
			if($this->simplelogin->login($this->input->post('username'), $this->input->post('password'))) {
				if (($this->session->userdata('accesslevel') == 1) || ($this->session->userdata('accesslevel') == 5)) {
					$client = $this->new_clients->get($this->session->userdata('clientid'));
					$this->session->set_userdata('clientname', $client->name);
					//logged in ok
					//update log file
					$insert['clientid'] = $client->id;
					$insert['action'] = "Logged in";
					$rowid = $this->logfile->insert($insert);
					//go to property list
					redirect('/clients');
				} elseif ($this->session->userdata('accesslevel') == 9) {
					
					
					redirect('/admin');
				}
				
			} else {
				$this->session->set_userdata('loginmsg', 'Incorrect username or password. Please try again.');
				redirect('/clients');			
			}			
		}
	}
	
	function dologin() {
		if(!$this->session->userdata('logged_in')) {
			//load the login screen
			$this->load->view('clientaccess/login');
		} else {
			//already logged in so go to normal page
			redirect('/clients');
		}	
	}
	
	function logout()
	{
		$this->load->library('simplelogin');

		//Logout
		$this->simplelogin->logout();
		//go back to main page
		redirect('http://www.solutionsinengineering.com');		
	}

	function account()
	{
		$this->load->model('new_clients');
		$this->load->model('clientlogin');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('name', 'Company Name', 'xss_clean|required');
		$this->form_validation->set_rules('address', 'Address', 'xss_clean|required');
		$this->form_validation->set_rules('suburb', 'Suburb', 'xss_clean|required');
		$this->form_validation->set_rules('state', 'Country/State', 'xss_clean|required');
		$this->form_validation->set_rules('postcode', 'Postcode', 'xss_clean|required');
		$this->form_validation->set_rules('phone1', 'Phone 1', 'xss_clean|required');
		$this->form_validation->set_rules('email', 'Email', 'xss_clean|valid_email|required');
		$this->form_validation->set_rules('contact_person_email', 'Contact Person - Email', 'xss_clean|valid_email');

		$clientid = $this->session->userdata('clientid');
		
		if ($this->session->userdata('accesslevel') == 1) {
			// Master User account
			if ($this->form_validation->run() == false) {
				if (!isset($_POST['submit'])) {
					$data['user'] = $this->new_clients->get_by('id', $clientid);
					$data['account'] = $this->clientlogin->get($this->session->userdata('id'));
					$this->load->view('clientaccess/clientmasteraccount', $data);
				} else {
					echo validation_errors();
				}
			} else {
				$update['name'] = $this->input->post('name');
				$update['address'] = $this->input->post('address');
				$update['suburb'] = $this->input->post('suburb');
				$update['state'] = $this->input->post('state');
				$update['postcode'] = $this->input->post('postcode');
			
				$update['phone1'] = $this->input->post('phone1');
				$update['phone2'] = $this->input->post('phone2');
				$update['fax'] = $this->input->post('fax');
				$update['email'] = $this->input->post('email');
			
				$update['contact_person_name'] = $this->input->post('contact_person_name');
				$update['contact_person_phone'] = $this->input->post('contact_person_phone');
				$update['contact_person_email'] = $this->input->post('contact_person_email');
				$id = $this->session->userdata('clientid');
				$this->new_clients->update($id, $update);
				echo "Details have been saved";
			}
		} else {
			// Individual User account
			$data['user'] = $this->session->userdata('username');//clients->get_by('id', $clientid);
			$data['account'] = $this->clientlogin->get($this->session->userdata('id'));
			$this->load->view('clientaccess/clientaccount', $data);
		}
	}

	
	function forgotten() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean|required');

		if ($this->form_validation->run() == false) {
			$this->load->view('clientaccess/forgotten');
		} else {
			$this->load->model('new_clients');
			$this->load->model('clientlogin');
			$this->load->model('logfile');
			$email = $this->input->post('email');
			$client = $this->new_clients->get_by('email', $email);
			if ($client) {
				$pw = "";
				$notused = array(58,59,95,96);
				// generate new password
				
				for ($i = 1; $i <= 10; $i ++) {
					$chrid = rand(48, 122);
					if (!in_array($chrid, $notused)) {
						$pw .= chr($chrid);
					}	
				}
				$clientlogin = $this->clientlogin->get_by('clientid', $client->id);
				
				//update the database
				$id = $clientlogin->id;
				$update['password'] = sha1($pw);
				$this->clientlogin->update($id, $update);
				
				//notify the user
				$this->load->library('email');
				$config['mailtype'] = 'html';
				$config['protocol'] = 'mail';
				$config['wordwrap'] = FALSE;
				$config['charset'] = 'iso-8859-1';
				$config['wrapchars'] = 300;
				$this->email->initialize($config);
			
							
				$message = "Dear ".$client->name."<br/>";
				$message .= "At your request, your password for our online portal has been changed. Here are your account details:<br/><br />";
				$message .= "Username: ".$clientlogin->username."<br />";
				$message .= "Password: ".$pw."<br /><br />";
				$message .= "You should log in very soon and change your password to something more memorable.<br />";
				$message .= "Once you have logged in, clicking the Account button in the header will bring up details about your company and also allow you to change your password.<br /><br />";
				$message .= "Thank you for using our online portal,<br /><br />";
				$message .= "Solutions in Engineering";
				
				$to = $email;
				$subject = 'Account details for Solutions in Engineering Client Portal';
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= 'From: Solutions In Engineering <enquiry@solutionsinengineering.com>' . "\r\n";
				$headers .= 'Reply-To: enquiry@solutionsinengineering.com' . "\r\n";
				$headers .= 'Bcc: dath@solutionsinengineering.com' . "\r\n";

				//echo $message;
				//echo $headers;
				//echo $to;
				mail($to, $subject, $message, $headers);

				//update log file
				$insert['clientid'] = $this->session->userdata('clientid');
				$insert['action'] = "Password changed by system";
				$rowid = $this->logfile->insert($insert);

				redirect('/report/complete/');

			} else {
				$data['message'] = "That email address was not found. Please check and try again";
				$this->load->view('clientaccess/forgotten', $data);
			}

		}
	}
	
	function password($level = 1) {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('oldpassword', 'Current Password', 'xss_clean|required');
		$this->form_validation->set_rules('newpassword', 'New Password', 'xss_clean|required');
		$this->form_validation->set_rules('confirmpassword', 'Confirm Password', 'xss_clean|required|matches[newpassword]');

		if ($this->form_validation->run() == false) {
			echo validation_errors();
		} else {
			$this->load->model('clientlogin');

			$oldpwd = $this->input->post('oldpassword');
			$userid = $this->session->userdata('id');
			$clientlogin = $this->clientlogin->get_by('id', $userid);

			if ($clientlogin->password == sha1($oldpwd)) {
				// was a match so update
				$update['password'] = sha1($this->input->post('newpassword'));
				$this->clientlogin->update($clientlogin->id, $update);
				echo "Password changed.";
			}else {
				// wasn't a match so report error
				echo "Your old password was not correct.";
			}
		}
	
	}
	
	function idaccounts() {
		$this->load->model('clientlogin');
		$this->load->model('new_clients');
		$clientid = $this->session->userdata('clientid');
		$this->db->where('accesslevel', 5);
		$data['individuals'] = $this->clientlogin->get_many_by('clientid', $clientid);
		$data['client'] = $this->new_clients->get_by('id', $clientid);
		$this->load->view('clientaccess/individuals', $data);
	}
	
	function addindividual() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('fullname', 'Full name', 'xss_clean|required');
		$this->form_validation->set_rules('username', 'Username', 'xss_clean|required');
		$this->form_validation->set_rules('password', 'Password', 'xss_clean|required');


		$this->form_validation->set_rules('email', 'Email', 'xss_clean|required|valid_email');

		
		if ($this->form_validation->run() == false) {
			echo validation_errors();
		} else {
			$this->load->model('clientlogin');
			$username = $this->input->post('username');
			$found = 0;
			//check to see if username already exists
			if ($this->input->post('update') == 0) {
				$allusers = $this->clientlogin->get_all();
				foreach ($allusers as $user) {
					if ($user->username == $username) $found = 1;
				}
				if ($found == 1) {
					echo "unavail";
				} else {
					$insert['password'] = sha1($this->input->post('password'));
					$insert['accesslevel'] = 5;
					$insert['clientid'] = $this->session->userdata('clientid');
					$insert['created'] = date("Y-m-d H:i:s");
					$insert['username'] = $username;
					$insert['fullname'] = $this->input->post('fullname');
					$insert['email'] = $this->input->post('email');
					$insert['modified'] = date("Y-m-d H:i:s");
	
					$this->clientlogin->insert($insert);
					echo $this->db->insert_id();
				}
			} else {
				// this is an update
				// first get the id of the row for this user
				$oldusername = $this->input->post('oldusername');
				$thisuser = $this->clientlogin->get_by('username', $oldusername);
				$thisuserid = $thisuser->id;
				
				$allusers = $this->clientlogin->get_all();
				foreach ($allusers as $user) {
					if (($user->username == $username) && ($user->id != $thisuserid)) $found = 1;
				}
				if ($found == 1) {
					echo "unavail";
				} else {
					// entered username is either the same, or is available
					$insert['username'] = $username;
					$insert['fullname'] = $this->input->post('fullname');
					$insert['email'] = $this->input->post('email');
					$insert['modified'] = date("Y-m-d H:i:s");
					
					$this->clientlogin->update($thisuserid, $insert);				
					echo $thisuserid;

				}
			
			}
			
		}
	
	}
	
	function newaccount() {
		$this->load->view('clientaccess/newaccount');
	}
	
	function getdata($userid) {
		$this->load->model('clientlogin');
		$client = $this->clientlogin->get_by('id', $userid);
		
		echo json_encode($client);
	}

	function deleteuser($userid) {
		$this->load->model('clientlogin');
		$this->clientlogin->delete($userid);
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */