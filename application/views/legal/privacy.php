<script>
	$(function() {
		$('#printbtn').click(function() {
			window.print();
			return false;
		});
	});
</script>
<div id='main' class='grid_7'>
	<h1>Privacy Policy</h1>
	
	<div id="toolbarbuttons">
		<?php
			$print = array(
				'id'	=> 'print',
				'name'	=> 'print',
				'src'	=> '/library/images/printersmall.png',
				'title'	=> 'Print this document'
			);
			$pdf = array(
				'id'	=> 'pdf',
				'name'	=> 'pdf',
				'src'	=> '/library/images/pdfsmall.png',
				'title'	=> 'View PDF of this document'
			);
			echo anchor('',img($print), "id='printbtn'").nbs(4);
			echo anchor('/library/docs/pdf/privacy.pdf', img($pdf), "target = '_blank'");
		?>
	</div>
	<p><?=COMPANY?> is a member of the National Community Titles Institute (NCTI) which is a national representative association for parties involved in the professional full-time administration of community, strata and body corporate schemes. We provide professional services including Sinking Fund Forecasts and Safety Reports etc to the strata titles industry throughout Australia.</p>
	<p><?=COMPANY?> provides these services on behalf of your scheme and in doing so hold personal information about you.</p>
	<p>This policy explains how we deal with your personal information in line with our legal obligations, Australian Standards and our Best Practice commitment to the Service we provide you.</p>
	<h3>OUR INFORMATION</h3>
	<p>Solutions IE Pty Ltd</p>
	<p><label>Offices: </label>Melbourne, Sydney, Brisbane</p>
	<p><label>Telephone: </label>1300 136 036</p> 
	<p><label>Facsimile: </label>1300 136 037</p>
	<p><label>E-mail: </label>admin@solutionsie.com.au</p>
	<p><label>Website: </label>www.solutionsie.com.au</p>
	<p><label>Privacy Officer: </label>&nbsp;</p>
    
	<h3>PERSONAL INFORMATION</h3>
	<p>Personal information is any information about you that identifies you or information by which your identity can be reasonably discovered. Examples are your name, address, lot number, unit entitlement, taxation information, e-mail address, credit card information etc.</p>
	
	<h3>WHY WE COLLECT PERSONAL INFORMATION</h3>
	<p>Collecting your personal information is essential for us to be able to perform our lawful business undertakings under our agreement with the scheme. Our obligations to the scheme are to provide professional reports as requested by a Body Corporate or Owners Corporation representative.</p>
	<h3>HOW WE COLLECT PERSONAL INFORMATION</h3>
	<p>Where possible we collect your personal information directly from you. However personal information is generally collected during the course of our relationship with the scheme. Sometimes personal information may be collected about you from other sources. For example a Body Corporate Manager or Strata Manager will have your personal information. In this case we rely on the fact that you have previously given consent to any collection, use, or disclosure of your personal information by the Scheme Manager or Committee. If you give your personal information directly to us we will usually need your consent in writing but we may accept your verbal consent in some circumstances. Sometimes we will assume you have given your consent by your conduct with us.</p>
	<h3>HOW WE USE PERSONAL INFORMATION</h3>
	<p>We use your personal information to provide professional services only to you and your scheme. This information is strictly controlled and we disclose your personal information only to our specialists and services providers as required while conducting our business on behalf of your Committee.</p>
	<h3>DIRECT MARKETING</h3>
	<p>We may use your personal information to provide you with information about our products and services. Contact us if you do not want to receive any of this information.</p>
	<h3>ENSURING YOUR PERSONAL INFORMATION IS UP-TO-DATE</h3>
	<p>It is very important that the personal information we collect from you is accurate, complete and up-to-date. Now and then we will ask you to tell us of any changes to your personal information.</p>
	<p>We take all reasonable precautions to safeguard your personal information from loss, misuse or unauthorised access, modification and disclosure.</p>
	<h3>ACCESS TO OUR SERVICES VIA THE INTERNET</h3>
	<p>You can view the privacy statement when you access our website.</p>
	<h3>SECURITY OF YOUR PERSONAL INFORMATION ONLINE</h3>
	<p>Generally, e-mail is not a secure way to communicate and you should be aware of this when sending personal information to us via e-mail.</p>
	<h3>ACCESSING YOUR PERSONAL INFORMATION</h3>
	<p>You may request access to any of the personal information we hold about you at any time.</p>
	<p>A summary of personal information such as your name and address details, contact telephone numbers and the matters your scheme has engaged us on are available upon written request being received by us and for more detailed requests, a fee may be charged to cover the cost of its retrieval and supply.</p>
	<h3>PERSONAL INFORMATION IDENTIFIERS</h3>
	<p>A Commonwealth identifier is a Commonwealth Government identification number such as your Tax File Number (TFN) or Medicare Number.</p>
	<p>We do not use these as a way of identifying the information we have about you.</p>
	<h3>DO I HAVE TO BE IDENTIFIED AT ALL?</h3>
	<p>Where we can, we may offer you the opportunity to deal with us anonymously. Otherwise, the nature of our job is such that anonymity cannot be maintained.</p>
	<h3>SENSITIVE INFORMATION</h3>
	<p>Sensitive information can be information about your racial or ethnic origin, political opinions, membership of political associations, religious beliefs or affiliations, membership of professional or trade associations or trade unions, sexual preferences or practices, criminal records or health.</p>
	<p>We only collect, use or disclose sensitive information about you as allowed by law, where we have received your consent to do so, or the collection is necessary to do our job under our agreement with your scheme.</p>
	
	<h3>OUR PRIVACY POLICY MAY CHANGE FROM TIME TO TIME</h3>
	<p>We regularly review all our policies and procedures and may change this Privacy Policy from time to time. This Privacy Policy was last amended on August 2007.</p>
	<h3>CONCERNS OR REQUESTS</h3>
	<p>If you have a question about this Policy, wish to lodge a request to access your personal information or you believe we have not protected your personal information please contact us. If you are not satisfied with the response, you can refer your complaint to the Federal Privacy Commissioner by telephoning 1300 363 992.</p>
</div>