<div id='main' class='grid_7'>
	<h1>Latest News</h1>

  <p>At Solutions In Engineering, we believe that providing information and  education on issues affecting our clients is just as important as the detailed advice  and guidance delivered in our consultancy services. We provide the information  our clients require to proactively manage risk and comply with legislation,  standards and directives.<br /></p>
    <p>We regularly update information on industry trends and legislative requirements  for our clients and the property and strata management industries. We use our  experience to analyse, interpret and explain our clients' obligations and how  to fulfil them in a real-world environment. We provide solutions.<br />
      <br />
      </p>
      Our Newsletters and E-Bulletins contain the latest information on  compliance and risk-management in the property management and strata  industries.
      </p>
    </p>
  <table width="539" height="222" border="0">
    <tr valign="top">
      <td width="153" height="210" valign="top"><p align="left"><a href="../../library/docs/pdf/QLD_LNP_What_it_means_for_strata.pdf">QLD LNP - What is means for strata</a></p>
        <p align="left">&nbsp;</p>
        <p align="left"><a href="../../library/docs/pdf/QLDLegislationUpdate-AsbestosCompliance&amp;TransistionalPeriods.pdf">QLD Legislation Update - Asbestos Transitional Periods</a></p>      </td>
      <td width="153" height="210" valign="top"><p align="left"><a href="../../library/docs/pdf/NSW_Asbestos_Compliance_Ceilings.pdf">NSW Legislation Update - Asbestos - Common Property Ceilings</a></p>
        <p align="left">&nbsp;</p>
      <p align="left"><a href="../../library/docs/pdf/NSWLegislationUpdate-AsbestosCompliancein2012.pdf">NSW Legislation Update - Asbestos Compliance in 2012 </a></p>      </td>
      <td width="153" height="210" valign="top"><p align="left"><a href="../../library/docs/pdf/VICLegislationUpdate-AsbestosCompliance.pdf">VIC Legislation Update - Asbestos Compliance in 2012 (National Code vs Safe Work Victoria)</a></p>      </td>
    </tr>
  </table>
  <p><br />
  For more information, please select a service from the right-hand menu, or <a href="/contact">contact us</a>.</p>
  <p>&nbsp;</p>
</div>
