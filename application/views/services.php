<div id='main' class='grid_7'>
	<h1>Services</h1>

	<p>Solutions in Engineering provides a wide range of audit, consulting and   advisory services in relation to property assets for property owners, managers   and developers.</p>
	<p>We operate throughout Queensland, New South Wales, Australian Capital Territory and Victoria, and   by arrangement to South Australia and the Northern Territory.</p>
	<p>Our services encompass a broad range of specialisations including:</p>
	<ul class='listing'>
	  <li> Sinking fund forecasting &amp; planning</li>
	  <li>Building maintenance planning &amp; reporting</li>
	  <li> Structural engineering</li>
	  <li>Utilities cost management reporting</li>
	  <li>Occupational health &amp; safety reporting</li>
	  <li> Workplace health &amp; safety reporting</li>
	  <li> Fire and essential safety reporting</li>
	  <li>Fire and evacuation planning</li>
	  <li>Emergency response planning</li>
	  <li>Pool safety inspections</li>
	  <li> Balustrade testing</li>
	  <li>Asbestos Surveys, Asbestos Registers &amp; Management Plans</li>
	  <li>Insurance valuations</li>
  </ul>
  <p><br />
    For more information, please select a service from the right-hand menu, or <a href="/contact">contact us</a>.</p>
	<p>&nbsp;</p>
</div>
