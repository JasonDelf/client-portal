<?=doctype('html5')?>
<head>
<?php
	$meta = array(
	        array('name' => 'robots', 'content' => 'no-cache'),
	        array('name' => 'description', 'content' => 'My Great Site'),
	        array('name' => 'keywords', 'content' => 'love, passion, intrigue, deception'),
	        array('name' => 'robots', 'content' => 'no-cache'),
	        array('name' => 'Content-type', 'content' => 'text/html; charset=utf-8', 'type' => 'equiv')
	    );
	echo meta($meta); 
?>
	<title><?=$pagetitle?></title>

	<?=link_tag('main')?>
	<?php
		$link = array(
        	'href' => 'library/css/printer.css',
          	'rel' => 'stylesheet',
          	'media' => 'print'
		);

		echo link_tag($link);
	?>
	
	<?=script_tag('modernizr-1.6.min')?>
	<?=script_tag('jquery-1.4.4.min')?>
	<?=script_tag('jquery.form')?>
	<script type="text/javascript">

  		var _gaq = _gaq || [];
  		_gaq.push(['_setAccount', 'UA-3316354-11']);
  		_gaq.push(['_trackPageview']);

  		(function() {
    		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  		})();

	</script>
</head>

<body>
	<div id='container' class='container_12'>
		<header class='screen'>
			<?php
				$image_properties = array(
					'src' => '/library/images/header.jpg',
					'width' => '960',
					'title' => 'Solutions in Engineering'
				);
				echo img($image_properties);
			?>
		
		</header>
		<header class='print'>
			<?php
				$image_properties = array(
					'src' => '/library/images/header_web.png',
					'alt' => COMPANY,
					'width' => '700',
				);
				echo img($image_properties);
			
			?>
		</header>		