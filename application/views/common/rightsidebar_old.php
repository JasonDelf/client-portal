		<div id='sidebar' class='grid_3'>
			<div id="special">
				<h3>Winter Special</h3>
				<p>Start updating your building details for the chance to win a special prize pack.</p>
				<!--
				prizeour "Clever Climate Energy Saver Kit" which  includes: </p>
				<ul>
					<li>An eight plug stand-by eliminator and remote control</li>
					<li>A wireless power monitor</li>
					<li>5 power saving light globes</li>
				</ul>
				-->
				<p>Simply log-in to our online portal and enter or update your existing building info details. Not only will you receive 1 entry ticket per building for the chance to win, you will have discovered a smarter way to access a quote request or to place a work order.</p>

				<p><a href="/clients/dologin" class="awesome large dkblue">Click Here to Enter Now</a></p>
				<p class='small'>Offer Expires: 31 August 2011</p>
			
			
			</div>
		
		
			<h3>Client Testimonials</h3>
			<div class='testimonial'>
				<p>“I have found Solutions in Engineering the most professional, reliable and accurate service provider. They do not 'cut-corners' or provide work that is not completely relevant to the building and I know I can rely on the result."</p>
				<div class='client'>
					Lisa Miller<br />
					Strata Title Management
				</div>
			</div>
			<div class='testimonial'>
				<p>“Solutions in Engineering provide an exceptional service and always go the extra mile for your needs."</p>
				<div class='client'>
					Karina Knight<br />
					Whelan Property Group Pty Ltd
				</div>
			</div>
			<div class='testimonial'>
				<p>"Solutions in Engineering … make it 'easy'. Plain, simple service delivery, time efficient and above all easy to get if you have any problems. Solutions in Engineering every time for us."</p>
				<div class='client'>
					Greg Roberts<br/>
					The Community Managers
				</div>
			</div>

		</div>