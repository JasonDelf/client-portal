		<div id="footer-print">
			<p>&copy; Copyright 2005 - <?=date("Y")?> Solutions In Engineering</p>
		</div>
		<div class='clearfix'>&nbsp;</div>
		
	</div> <!-- container-->
	<footer>
		<div id='sitelinks'>
			<a href="/legal/privacy">Privacy Policy</a>
			<a href="/legal/tandc">Supply Terms & Conditions</a>
			<a href="/insurance/professional">Professional Indemnity Insurance</a>
			<a href="/insurance/publicliability">Products & Public Liability Insurance</a>
			<a href="http://intranet.solutionsie.com.au" target="_blank">Staff Portal</a>
		</div>
		<p>&copy; Copyright 2005 - <?=date("Y")?> Solutions In Engineering</p>
	
	</footer>
</html>