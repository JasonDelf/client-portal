		<div class='grid_2'>
			<nav>
				<ul>
					<li><a href="/">Home</a></li>
					<li><a href="/services">Services</a></li>
					<li><a href="/services/order">Quote/Order</a></li>
					<li><a href="/news">Latest News</a></li>
					<li><a href="/legal">Legislation</a></li>
					<li><a href="/documents">Other Documents</a></li>
					<li><a href="/contact">Contact Us</a></li>
				</ul>
			</nav>
			<div id='portal'>
				<h3>Client Portal</h3>
				<!--<h4>Private Beta</h4>-->
				<?php
			
					$attr = array(
						'id'	=> 'frmlogin'
					);
					echo form_open('/clients/login', $attr);
						echo div_open();
							$username = array(
								'id'	=> 'username',
								'name'	=> 'username',
								'size'	=> '14'
							);
							echo form_label('Username:', 'username');
							echo form_input($username);
						echo div_x();
						
						echo div_open();
							$password = array(
								'id'	=> 'password',
								'name'	=> 'password',
								'size'	=> '14'
							);
							echo form_label('Password:', 'password');
							echo form_password($password);
						echo div_x();
						
						echo div_open('','buttons');
							$login = array(
								'id'	=> 'login',
								'name'	=> 'login',
								'content'	=> 'Login',
								'class'	=> 'awesome blue',
								'type'	=> 'submit'
							);
							echo form_button($login);
						
						echo div_x();
						echo div_open('loginlinks');
							echo anchor("clients/forgotten", "Forgotten username/password?").br();
							echo anchor("clients/newaccount", "Don't have login details?").br();
						echo div_x();
					echo form_close();
		
				?>
			</div>
		</div>