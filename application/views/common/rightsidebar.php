		<div id='sidebar' class='grid_3'>

		
		
			<h3>Client Testimonials</h3>
			<div class='testimonial'>
				<p>“I have found Solutions in Engineering the most professional, reliable and accurate service provider. They do not 'cut-corners' or provide work that is not completely relevant to the building and I know I can rely on the result."</p>
				<div class='client'>
					Lisa Miller<br />
					Strata Title Management
				</div>
			</div>
			<div class='testimonial'>
				<p>“Solutions in Engineering provide an exceptional service and always go the extra mile for your needs."</p>
				<div class='client'>
					Karina Knight<br />
					Whelan Property Group Pty Ltd
				</div>
			</div>
			<div class='testimonial'>
				<p>"Solutions in Engineering … make it 'easy'. Plain, simple service delivery, time efficient and above all easy to get if you have any problems. Solutions in Engineering every time for us."</p>
				<div class='client'>
					Greg Roberts<br/>
					The Community Managers
				</div>
			</div>

		</div>