<div id='main' class='grid_10'>
  <div class='grid_7 alpha'>
	<h1>Contact Us</h1>
	<?php
		$formattr = array(
			'id'	=> 'frmContact'
		);
		echo form_open('contact/send', $formattr);
		echo div_open('response');
		echo div_x();
		echo div_open();
			echo form_hidden('confirm', '');
			$input = array(
				'id'	=> 'firstname',
				'name'	=> 'firstname',
				'size'	=> '25'
			);
			echo form_label('First Name', 'firstname');
			echo form_input($input);
		echo div_x();
		
		echo div_open();
			$input = array(
				'id'	=> 'lastname',
				'name'	=> 'lastname',
				'size'	=> '25'
			);
			echo form_label('Last Name', 'lastname');
			echo form_input($input);
		echo div_x();

		echo div_open();
			$input = array(
				'id'	=> 'company',
				'name'	=> 'company',
				'size'	=> '25'
			);
			echo form_label('Company', 'company');
			echo form_input($input);
		echo div_x();

		echo div_open();
			$input = array(
				'id'	=> 'email',
				'name'	=> 'email',
				'size'	=> '40'
			);
			echo form_label('Email', 'email');
			echo form_input($input);
		echo div_x();

		echo div_open();
			$input = array(
				'id'	=> 'phone',
				'name'	=> 'phone',
				'size'	=> '25'
			);
			echo form_label('Phone', 'phone');
			echo form_input($input);
		echo div_x();

		echo div_open();
			$input = array(
				'id'	=> 'message',
				'name'	=> 'message',
				'rows'	=> '8',
				'cols'	=> '40'
			);
			echo form_label('Message', 'message');
			echo form_textarea($input);
		echo div_x();
	
		echo div_open();
			$button = array(
				'id'	=> 'submit',
				'name'	=> 'submit',
				'content'	=> 'Send Message',
				'type'	=> 'submit'
			);
			echo form_button($button);
			$button = array(
				'id'	=> 'reset',
				'name'	=> 'reset',
				'content'	=> 'Clear Form',
				'type'	=> 'reset'
			);
			echo form_button($button);
			
		echo div_x();
	
		echo form_close();
	?>
  </div>
  
  <div class='grid_3 omega'>
	<div id="phonenumbers">
		<h4>Phone Us:</h4>
		<p>Australia: 1300 136 036</p>
		<p>New Zealand: 0800 136 036</p>
		<h4>Locations:</h4>
		<p>Brisbane</p>
		<p>Sydney</p>
		<p>Melbourne</p>
		<p>Auckland</p>
	</div>
  </div>
</div>
<script>
	$(function() {
		function showResponse(responseText, statusText, xhr, $form)  { 
			$("#response").html(responseText);
		}
		
		$("#frmContact").ajaxForm({ 
	        target: '#response', 
	        success: function() { 
    	        $('#response').show('slow').delay(2000).hide('slow'); 
        	} 
		});
	});
</script>