<div id='main' class='grid_7'>
	<h1>Specialised Reports for Property Assets</h1>
	
	<?php
		$prop = array(
			'src'	=> '/library/images/No-1.jpg',
			'class'	=> 'bodyimage right',
			'alt'	=> 'Highrise',
			'title'	=> 'Highrise building'
		);
		echo img($prop);
	?>
	<p>You need  a company to provide an innovative and consultative approach in providing for  your specific needs in the professional management of property assets. </p>
	<p>You  operate under heavy demands in managing those assets. We understand that you as  a professional manager need someone to take the monkey off your back!</p>
	<p>Simply,  when you pass a job onto our team at Solutions in Engineering, we will do the  work in such a way that you know the job will get done as quickly and  professionally as possible, and all with a minimum of fuss.</p>
	<p>We  undertake a proactive method of innovation and client consultation that  underpins our success in the market place. We have consolidated our position of  strength within the industries for those that manage strata, and those that  manage and develop property.</p>
	<p>When you  place the order with us, you will know it will be handled thoroughly and  efficiently, enabling you to get on with your other important tasks. This is  the promise that we make to you; that no matter how big or small your job is we  will treat it with the professional care and attention that it deserves. Here  is how... </p>
  <p class='list'><span class='point'>We communicate.</span> You want to be kept informed of important issues and at the same time do not have the time or the desire to get a whole bunch of calls. Our approach to communication is, you get called only when necessary and useful.</p>
	<p class='list'><span class='point'>We get all the information up front.</span> Our order forms are designed to get all the information that we need to get the job done from go to whoa. So unless something unexpected comes up, you won&rsquo;t get calls that remind you of the Spanish Inquisition.</p>
	<p class='list'><span class='point'>We provide value for money.</span> We are here to serve you for the long term believing that if we maintain reasonable fees along with reliable responsive service, you will keep using us.</p>
	<p class='list'><span class='point'>We give you clear advice.</span> We understand that a report needs to be conclusive and furthermore that you need to be able to find that conclusion easily. Our reports are designed so that you can find the conclusion or recommendation quickly and easily.</p>
	<p class='list'><span class='point'>We hold full insurance.</span> We have full Professional Indemnity & Public Liability Insurances that covers our full services range giving peace of mind to you, our customer.</p>
	<p class='list'><span class='point'>We stand behind our work.</span> We offer an ironclad guarantee to the professional manager and all of our clients that manage property asset&hellip; </p>
 

</div>
