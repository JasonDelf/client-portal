		<div id='sidebar' class='grid_3'>
			<h3>Reports</h3>
			<ul class='reports'>
				<li><a href="/services/report/pool">Pool Compliance</a></li>
				<li><a href="/services/report/sinking">Sinking Fund</a></li>
				<li><a href="/services/report/fire">Fire</a></li>
				<li><a href="/services/report/insurance">Insurance</a></li>
				<li><a href="/services/report/safety">Safety</a></li>
				<li><a href="/services/report/maintenance">Maintenance</a></li>
				<li><a href="/services/report/asbestos">Asbestos</a></li>
				<li><a href="/services/report/balustrade">Balustrade</a></li>
				<li><a href="/services/report/part5">Part 5</a></li>

			</ul>

		</div>