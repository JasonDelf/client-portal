<?php
header("Content-type: text/xml"); 

//output($rows);

$xml_output = "<?xml version=\"1.0\"?>\n"; 
$xml_output .= "<".$modelname."s>\n"; 


foreach ($rows as $row) {
    $xml_output .= "\t<$modelname>\n"; 
	foreach ($row as $key=>$value) {
		if ($value == "") {
			$value = " ";
		}
        $value = str_replace("&", "&amp;", $value); 
        $value = str_replace("<", "&lt;", $value); 
        $value = str_replace(">", "&gt;", $value); 
        $value = str_replace("\"", "&quot;", $value); 

		
	    $xml_output .= "\t\t<$key>" . $value . "</$key>\n"; 
	
	}
    $xml_output .= "\t</$modelname>\n"; 

}

$xml_output .= "</".$modelname."s>"; 

echo $xml_output; 

?>	