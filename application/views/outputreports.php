<html>
<head>
<title>Online Orders since <?=date("d M, Y", strtotime($startdate))?></title>
<?=link_tag('reporting')?>
</head>
<body>
<?php
	foreach ($orders as $order) {
		echo "<div class='order'>";
			echo "<br />";
			echo heading("Order No: ".$order->id, 1);
			echo heading("Building Details",2); 
			?>
			<label>Building Name:</label><?=$order->building->building_name?><br />
			<label>CTS:</label><?=$order->building->building_cts?><br />
			<label>Building Type:</label><?=$order->building->type?><br />
			<label>Address:</label><?=$order->building->building_address?><br />
			<label>&nbsp;</label><?=$order->building->building_suburb?> <?=$order->building->building_postcode?><br />
			<label>Number of Units:</label><?=$order->building->building_num_units?><br />
			<label>Lot Count:</label><?=$order->building->building_lotcount?><br />
			<label>Construction Date:</label><?=$order->building->building_constructiondate?><br />
			<br />

			<label>Building Manager:</label><?=$order->building->buildingmanager?><br />
			<label>Manager Phone:</label><?=$order->building->buildingmanagerphone?><br />
			<br />

			<label>Onsite Contact:</label><?=$order->building->onsitecontact?><br />
			<label>Contact Phone:</label><?=$order->building->onsitecontactphone?><br />
			<br />

			<label>Correspond To:</label><?=$order->building->correspondname?><br />
			<label>Phone:</label><?=$order->building->correspondphone?><br />
			<label>Email:</label><?=$order->building->correspondemail?><br />
			<br />
			
			<label>Management Company:</label><?=$order->building->managementcompany?><br />
			<label>Contact:</label><?=$order->building->mgmt_contact?><br />
			<label>Phone:</label><?=$order->building->mgmt_contact_phone?><br />
			<label>Email:</label><?=$order->building->mgmt_contact_email?><br />
			<br />

			<label>Keys Required:</label><?=$order->building->keys_required?><br />
			<label>Key Details:</label><?=$order->building->keydetails?><br />
			<br />
			<?php
			echo heading("Report Details - ".$order->request,2); 
			?>
			<label>Report Type:</label><?=$order->report->report_name?><br />
			<?php 
			if ($order->requestby != "") {
				$date = $order->requestby;
			} else {
				$date = "N/A";
			}
			?>
			<label>Requested By Date:</label><?=$date?><br />
			<label>Meet on Site?:</label><?=$order->meetonsite?><br />

			<?php
			echo heading("Report Specific Questions",2); 
			foreach ($order->answers as $answer) {
				?>
				<label><?=$answer->question?></label><?=$answer->answer?><br />
				<?php
			}
			if (sizeof($order->answers) == 0) {
				?>
				<label>&nbsp;</label>None<br />
				<?php
			}
		echo "</div>";
	}
?>

</body>
</html>