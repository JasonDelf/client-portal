<?php
	echo heading("Add Building",2);
	$attr = array(
		'id'	=> 'addbldg'
	);
	echo form_open('/building/add/1', $attr);
		echo div_open();
			$plan = array(
				'id'	=> 'plannumber',
				'name'	=> 'plannumber',
				'size'	=> '10'
			);
			echo form_label('Plan Number', 'plannumber');
			echo form_input($plan);
		echo div_x();

		echo div_open();
			echo br();
			$check = array(
				'id'	=> 'check',
				'name'	=> 'check',
				'content'	=> 'Check for this building',
				'class'	=> 'awesome large orange',
				'type'	=> 'submit'
			);
			echo form_button($check);
		echo div_x();

		echo div_open('results');
		
		echo div_x();

	echo form_close();
?>
<script>
	$(function() {
		var options = {
			target:        '#results'
		}
		$("#addbldg").ajaxForm(options); 

	});
</script>