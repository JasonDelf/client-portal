<?php
	echo heading("Cancel Report",2);
	echo p("Are you sure you want to cancel this report:");
	echo p($report->report_name);
	$attr = array(
		'id'	=> 'frmcancelreport'
	);
	echo form_open('/report/cancel/'.$order->id.'/1', $attr);
		echo div_open('buttons');
			$keep = array(
				'id'	=> 'keep',
				'name'	=> 'keep',
				'type'	=> 'button',
				'content'	=> 'No, keep it',
				'class'	=> 'awesome large dkgreen'
			);
			echo form_button($keep);
			$delete = array(
				'id'	=> 'delete',
				'name'	=> 'delete',
				'type'	=> 'submit',
				'content'	=> 'Yes, cancel it',
				'class'	=> 'awesome large red'
			);
			echo form_button($delete);
		echo div_x();
	echo form_close();
?>
<script>
	$(function() {
		
        $('#frmcancelreport').ajaxForm(function() { 
			window.location="/report/selectionmade";
		});
		
		$("#keep").click(function() {
			$("#pop-up").hide();
		});
	});
</script>