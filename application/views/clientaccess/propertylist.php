<script>
	$(function() {
		$('tr:even').addClass('alt');

		$('#help').click(function() {
			var src = "/help/page/1";
			$.ajax ({
				type:		'GET',
				url:		src,
				success:	function(data) {
					$("#pop-up .access-form div").html(data);
				}
			});


			$("#pop-up").show();
			return false;
		});
		$('.close-the-window').click(function() {
			$("#pop-up").hide();
		});
		
		$('#pop-up').hide();	
		
		$("#addbuilding").click(function() {
			var src = "/building/add";
			$.ajax ({
				type:		'GET',
				url:		src,
				success:	function(data) {
					$("#pop-up .access-form div").html(data);
				}
			});


			$("#pop-up").show();
			return false;
			
		});	
	});
</script>

<div class='col66'>
	<?=heading("Property List", 1)?>
</div>
<div class='col33 rightalign'>
	<?php
		$addbtn = array(
			'name'	=> 'addbldg',
			'id'	=> 'addbldg',
			'src'	=> 'library/images/add.png',
			'width'	=> '36',
			'height'=> '36'
		);
		$anchor = array(
			'title' => ' Add another building ',
			'id'	=> 'addbuilding'
		);
		echo anchor('#', img( $addbtn), $anchor);
		echo nbs(4);

		$helpbtn = array(
			'name'	=> 'help',
			'id'	=> 'help',
			'src'	=> 'library/images/admin/help.png'
		);
		$anchor = array(
			'title' => ' Help ',
			'id'	=> 'help'
		);
		echo anchor('#', img( $helpbtn), $anchor);
	?>
</div>
<div id='infomessage'>
	<p><strong>Please note:</strong> This portal works well with a modern standards-compliant browser such as <a href="http://www.apple.com/safari" target="_blank">Safari 4+</a>, <a href="http://getfirefox.com" target="_blank">Firefox 3+</a>, <a href="http://www.opera.com/browser/next/" target="_blank">Opera 10+</a>, <a href="http://www.google.com/chrome" target = "_blank">Chrome 9+</a>. It will work sufficiently with <a href="http://windows.microsoft.com/en-US/internet-explorer/downloads/ie-8" target="_blank">Internet Explorer 8+</a>.  </p>
	<p>Solutions In Engineering does not recommend using Internet Explorer 7 or below for this client portal.</p>
</div>
<div class='clearfix'></div>
<?php
?>
	<div id="searchbar">
		<?=form_open('building/search')?>
		<div class='col66'>
			<h2>Search</h2>
		</div>
		<div class='col33 rightalign'>
			<?php
			
				$gobtn = array(
					'type'	=> 'submit',
					'name'	=> 'searchbtn',
					'id'	=> 'searchbtn',
					'content'	=> 'Search',
					'class'	=> 'awesome blue'
				);
				echo form_button($gobtn);
			?>
		</div>
		<div class='clearfix'></div>
		<table id='searchtable' width=90%>
			<thead>
				<tr>
					<th width=30%>Property Name</th>
					<th width=30%>Address</th>
					<th width=25%>Town/Suburb</th>
					<th wisth=15%>Plan Number</th>
				</tr>
			</thead>
			<tbody>	
			<?php
				$search_name = array(
					'name'	=> 'search_name',
					'id'	=> 'search_name',
					'size'	=> '25'
				);
				$search_address = array(
					'name'	=> 'search_address',
					'id'	=> 'search_address',
					'size'	=> '25'
				);
				$search_suburb = array(
					'name'	=> 'search_suburb',
					'id'	=> 'search_suburb',
					'size'	=> '20'
				);
				$search_cts = array(
					'name'	=> 'search_cts',
					'id'	=> 'search_cts',
					'size'	=> '7'
				);
			?>
				<tr>
					<td><?=form_input($search_name)?></td>
					<td><?=form_input($search_address)?></td>
					<td><?=form_input($search_suburb)?></td>
					<td><?=form_input($search_cts)?></td>
				</tr>
			</tbody>
		</table>
		<?=form_close()?>
	</div>
	<table id='properties' width=90% class='datatable'>
		<thead>
			<tr>
				<th width=30%>Property Name</th>
				<th width=30%>Address</th>
				<th width=25%>Town/Suburb</th>
				<th wisth=15%>Plan Number</th>
			</tr>
		</thead>
		<tbody>	
	<?php
		foreach ($buildings as $building) {
			echo "<tr id=".$building->id.">";
			echo "<td>".anchor("building/select/".$building->id, $building->building_name)."</td>";
			echo "<td>".$building->building_address."</td>";
			echo "<td>".$building->building_suburb."</td>";
			echo "<td>".$building->building_cts."</td>";
			
			echo "</tr>";
		}
	?>
		</tbody>
	</table>



	