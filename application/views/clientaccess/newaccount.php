<?= doctype('html5')?>
<head>
	<title>Client Services :: by Solutions in Engineering</title>
	<?= link_tag('reset')?>
	<?= link_tag('login')?>
	<?= link_tag('buttons')?>
	<?= script_tag('jquery')?>
</head>
<body>
	<?php
		echo div_open('login', 'login');
		if (isset($message)) {
			echo div_open('', 'error');
			echo p($message);
			echo div_x();
		}
		echo "<div>";
		echo p("If you have not been given your account details for our Client Portal, please click the link below to send us an email. We will follow up with your details shortly.");
		echo p("<a href='mailto:enquiry@solutionsinengineering.com?Subject=Requesting Account Details for Client Portal' class='awesome large dkgreen'>Contact Us</a>");
		echo div_x();
	?>
</body>
</html>