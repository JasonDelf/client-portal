<script>
	$(function() {
		$('tr:even').addClass('alt');
		
		$('#cancel').click(function() {
			$("#dialog-modal").dialog('open');
			return false;
		});
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
	
		$( "#dialog-modal" ).dialog({
			height: 240,
			modal: true,
			autoOpen: false,
			buttons: {
				"Yes": function() {
					$( this ).dialog( "close" );
					window.location = '/building/cancel/';
				},
				"No": function() {
					$( this ).dialog( "close" );
				}
			}

		});
		
		var $tabs = $( "#tabs" ).tabs({
			ajaxOptions: {
				error: function( xhr, status, index, anchor ) {
					$( anchor.hash ).html(
						"Couldn't load this tab. We'll try to fix this as soon as possible. " +
						"If this wouldn't be a demo." );
				}
			}
		});

	});
</script>
<div id="dialog-modal">
	<h3>Cancel this Order</h3>
	<p>Are you sure you want to cancel this order? You cannot undo this action.</p>
</div>
<h1>Confirmation of Requests</h1>

	<div id = "printButton" align="right">
		<a href="javascript:window.print();">
			<img id="printerIcon" title="Print" alt="Print" src="/library/images/printer.gif" border="0"/>
		</a>
	</div>

<div id="tabs">
	<ul>
	<?php
		echo "<li><a href='/building/confirm/'>BUILDING DETAILS</a></li>";

		foreach ($orderdetails as $orderdetail) {
			$ordername = '';
			echo "<li><a href='/report/confirm/".$orderdetail->id."'>".$report[$orderdetail->id]->report_name."</a></li>";
		}
	?>	
	</ul>
</div>
<p>&nbsp;</p>
<h2>All services provided by SIE are supplied on the basis of SIE &quot;Supply Terms and Conditions&quot;, which are available from our office or our website: <a href="http://www.solutionsinengineering.com/terms-conditions/" target="_blank">www.solutionsinengineering.com/terms-conditions/</a>.</h2>
<h2>By selecting the Submit Request button below, you agree to SIE Supply Terms and Conditions.</h2>
<?php
	echo div_open('buttons');
		$submit = array(
			'name'	=> 'submit',
			'id'	=> 'submit',
			'class'	=> 'awesome large dkgreen'
		);
		$goback = array(
			'name'	=> 'back',
			'id'	=> 'back',
			'class'	=> 'awesome large orange'
		);
		$cancel = array(
			'name'	=> 'cancel',
			'id'	=> 'cancel',
			'class'	=> 'awesome large red'
		);
		echo anchor("/report/delivered/", "Submit Request", $submit);
	
	echo div_x();
	
	echo div_open('','clearfix');
	echo div_x();
?>

