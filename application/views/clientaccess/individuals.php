	<?= link_tag('main')?>
	<?= link_tag('buttons')?>
	<?= script_tag('jquery.min')?>
	<?= script_tag('jquery.form')?>
<body class='popup'>
<?php
	echo heading("Individual Accounts", 1);
?>
<?php
	
	echo "<table width=300px class='idlist'>";
	foreach ($individuals as $individual) {
		$image_edit = array(
    	    'src' 	=> 'library/images/admin/edit24.png',
        	'alt' 	=> 'Edit',
          	'class' => 'edit',
          	'width' => '20',
          	'height'=> '20',
          	'title'	=> 'Edit this account'
   		);
		$image_delete = array(
    	    'src' 	=> 'library/images/admin/delete24.png',
        	'alt' 	=> 'Delete',
          	'class' => 'delete',
          	'width'	=> '20',
          	'height'=> '20',
          	'title' => 'Delete this account'
		);

		echo "<tr id=".$individual->id.">";
			echo "<td width=35%>".$individual->username."</td>";
			echo "<td width=55%>".$individual->fullname."</td>";
			echo "<td width=5%>".img($image_edit)."</td>";
			echo "<td width=5%>".img($image_delete)."</td>";

		echo "</tr>";
	}
	echo "</table>";

	if (isset($message)) {
		echo div_open("message");
			echo p($message);
		echo div_x();
	}
	echo div_open('response');
		echo p("Details saved");
	echo div_x();

	$formattr = array(
		'id'	=> 'accountform'
	);

	echo form_open('clients/addindividual', $formattr);
		echo div_open('response2');
			echo p("Password has been changed");
		echo div_x();

		echo form_fieldset("Details");

			echo div_open();
				echo "<input type=hidden name=update value=0 id=update />";
				echo "<input type=hidden name=userid value=0 id=userid />";
				$fullname= array(
					'id'	=> 'fullname',
					'name'	=> 'fullname',
					'size'	=> '30'
				);
				echo form_label("Full name", 'fullname');
				echo form_input($fullname);
			echo div_x();

			echo div_open();
				echo "<input type=hidden name=oldusername id=oldusername />";
				$username= array(
					'id'	=> 'username',
					'name'	=> 'username',
					'size'	=> '30'
				);
				echo form_label("Username", 'username');
				echo form_input($username);
			echo div_x();

			echo div_open();
				$password= array(
					'id'	=> 'password',
					'name'	=> 'password',
					'size'	=> '30'
				);
				echo form_label("Password", 'password');
				echo form_input($password);
			echo div_x();

			echo div_open();
				$email= array(
					'id'	=> 'email',
					'name'	=> 'email',
					'size'	=> '30'
				);
				echo form_label("Email", 'email');
				echo form_input($email);
			echo div_x();

			echo div_open('');
				echo br();
				$reset = array(
					'id'	=> 'reset',
					'name'	=> 'reset',
					'content'	=> 'Clear Form',
					'class'	=> 'awesome medium orange',
					'type'	=> 'reset'
				);
				$change = array(
					'id'	=> 'savechange',
					'name'	=> 'savechange',
					'content'	=> 'Save Account Details',
					'class'	=> 'awesome medium dkgreen',
					'type'	=> 'submit'
				);
				$delete = array(
					'id'	=> 'deletethis',
					'name'	=> 'deletethis',
					'content'	=> 'Delete This User',
					'class'	=> 'awesome medium red hidden',
					'type'	=> 'button'
				);

				echo form_label("", '');
				echo form_button($reset).nbs(4);
				echo form_button($change);
				echo form_button($delete);
			echo div_x();

		echo form_fieldset_close();
	echo form_close();

?>
</body>
<script>
	$(function() {
	
		$("#deletethis").hide();

	    var options = { 
			success:	showResponse
		};

	    $('#accountform').ajaxForm(options); 
	
		function showResponse(responseText, statusText, xhr, $form) {
			$("#response").html("Details Saved").show('slow').delay(2000).hide('slow').delay(500);
			if (statusText == "success") {
				var userid = responseText;
				var username = $("#username").val();
				var fullname = $("#fullname").val();
				
				$("#fullname").val("");
				$("#username").val("");
				$("#password").val("");
				$("#email").val("");
				if ($("#update").val() == '0') {
					$("table.idlist tr:last-child").after("<tr id="+responseText+"><td>"+username+"</td><td>"+fullname+"</td><td><img class=edit width=20 height=20 title='Edit this account' alt='Edit' src='http://www.solutionsie.com.au/library/images/admin/edit24.png'></td><td><img class=delete width=20 height=20 title='Delete this account' alt='Delete' src='http://www.solutionsie.com.au/library/images/admin/delete24.png'></td></tr>");
				} else {
					$("#"+userid+" td").first().html(username).next().html(fullname);					
				}
				
			}
		}
		
		$(".edit").live('click',function() {
			var userid = $(this).parent().parent().attr('id');

			var username = $(this).parent().prev().prev().html();
			var href = "/clients/getdata/"+userid;
			
			$.getJSON(href, function(data) {
				$("#userid").val(data.id);	
				$("#username").val(data.username);	
				$("#oldusername").val(data.username);
				$("#fullname").val(data.fullname);
				$("#email").val(data.email);
				$("#update").val("1");
				$("#password").val("*****");
			});
			
		});
		
		$(".delete").live('click',function() {
			$("#savechange").hide();

			var userid = $(this).parent().parent().attr('id');

			var username = $(this).parent().prev().prev().html();
			var href = "/clients/getdata/"+userid;
			
			$.getJSON(href, function(data) {
				$("#userid").val(data.id);	
				$("#username").val(data.username);
				$("#oldusername").val(data.username);
				$("#fullname").val(data.fullname);
				$("#email").val(data.email);
				$("#update").val("1");
				$("#password").val("*****");
				$("#deletethis").show();
			});
			
		});
		
		$("#deletethis").click(function() {
			var userid = $("#userid").val();
			var href = "/clients/deleteuser/"+userid;

			$.ajax({
				url: href,
				success: function(){
					//$("#response").html("User removed").show('slow').delay(2000).hide('slow').delay(500);  				
					$("#"+userid).remove();
					$("#fullname").val("");
					$("#username").val("");
					$("#password").val("");
					$("#email").val("");
				}
			});

			
			$("#deletethis").hide();
			$("#savechange").show();

		
		});
		
	});
</script>
