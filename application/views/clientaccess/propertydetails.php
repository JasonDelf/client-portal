

<div class='col66'>
	<?=heading("Property Details", 1)?>
</div>
<div class='col33 rightalign'>
	<?php
		$helpbtn = array(
			'name'	=> 'help',
			'id'	=> 'help',
			'src'	=> 'library/images/admin/help.png'
		);
		$anchor = array(
			'title' => ' Help ',
			'id'	=> 'help'
		);
		echo anchor('#', img( $helpbtn), $anchor);
	?>
</div>
<div class='clearfix'></div>

<?php
	$needkeyknowledge = array(6,10,11,12,15);

	echo validation_errors();	

	echo form_open('building/details/');
	echo form_fieldset('Property Information');
		echo div_open('','left');
			$buildingname = array(
				'name'	=> 'buildingname',
				'id'	=> 'buildingname',
				'size'	=> '30',
				'value'	=> set_value('buildingname', $building->building_name)
			);
			echo div_open();
				echo form_label('Building Name:', 'buildingname');
				echo form_input($buildingname);
				echo "<span class='required'>*</span>";
			echo div_x();
			
			$plannumber = array(
				'name'	=> 'plannumber',
				'id'	=> 'plannumber',
				'size'	=> '10',
				'value'	=> set_value('plannumber', $building->building_cts)

			);
			echo div_open();
				$numberlabel = array(
					'id'	=> 'plannumberlabel'
				);
				if ($building->building_state == 351) {
					echo form_label('CTS Number:', 'plannumber', $numberlabel);				
				} elseif ($building->building_state == 350) {
					echo form_label('Strata Plan No:', 'plannumber', $numberlabel);			
				} elseif ($building->building_state == 352 ) {
					echo form_label('Plan of Subdivision:', 'plannumber', $numberlabel);
				} else {
					echo form_label('Plan/Registration Number:', 'plannumber', $numberlabel);
				}
				echo form_input($plannumber);
				echo "<span class='required'>*</span> Enter 0 if Non-Strata";
			echo div_x();

		echo div_open();
			$buildingaddress = array(
				'name'	=> 'buildingaddress',
				'id'	=> 'buildingaddress',
				'size'	=> '30',
				'value'	=> set_value('buildingaddress', $building->building_address)
			);
			echo div_open();
				echo form_label('Building Address:', 'buildingaddress');
				echo form_input($buildingaddress);
				echo "<span class='required'>*</span>";
			echo div_x();
			
			$suburb = array(
				'name'	=> 'suburb',
				'id'	=> 'suburb',
				'size'	=> '30',
				'value'	=> set_value('suburb', $building->building_suburb)
			);
			echo div_open();
				echo form_label('Suburb:', 'suburb');
				echo form_input($suburb);
				echo "<span class='required'>*</span>";
			echo div_x();
			
			$postcode = array(
				'name'	=> 'postcode',
				'id'	=> 'postcode',
				'size'	=> '4',
				'maxlength' => '4',
				'value'	=>  set_value('postcode', $building->building_postcode)
			);
			echo div_open();
				echo form_label('Postal/ZIP Code:', 'postcode');
				echo form_input($postcode);
			echo div_x();

			$statelistid = $this->lists->get_by('listname', 'States')->id;			
			$statelistitems = $this->listitems->get_many_by('listid', $statelistid);
			foreach ($statelistitems as $rowitem) {
				$states[$rowitem->id] = $rowitem->itemvalue;
			}
			
			echo div_open();
				echo form_label('Country/State:', 'state');
				echo form_dropdown('state', $states, $building->building_state, 'id=state');
				echo "<span class='required'>*</span>";
			echo div_x();
			
		echo div_x();

			$buildingtypes = $this->buildingtypes->get_all();
			$types[''] = "";
			foreach ($buildingtypes as $rowitem) {
				if (type_available($building->building_state, $rowitem->states)) {
					$types[$rowitem->id] = $rowitem->type;
				}
			}
			echo div_open();
				echo form_label('Building Type:', 'buildingtype');
				echo form_dropdown('buildingtype', $types, $building->building_type, 'id=buildingtype');
				echo "<span class='required'>*</span>";
			echo div_x();

			$num_units = array(
				'name'	=> 'num_units',
				'id'	=> 'num_units',
				'size'	=> '10',
				'value' => set_value('num_units', $building->building_num_units)
			);
			echo div_open();
				echo form_label('Number of Units:', 'num_units');
				echo form_input($num_units);
				echo "<span class='required'>*</span>";
			echo div_x();
			
			$constructiondate = array(
				'name'	=> 'constructiondate',
				'id'	=> 'constructiondate',
				'size'	=> '10',
				'value' => set_value('constructiondate', $building->building_constructiondate)
			);
			echo div_open();
				echo form_label('Construction Year or Date Registered:', 'constructiondate');
				echo form_input($constructiondate);
				echo "<span class='required'>*</span>";
			echo div_x();
			
		echo div_x();
		
		echo div_open('','clearfix'); echo div_x();
	
		$tmp_keys_required = "";
		if($this->input->post('keysrequired')) $tmp_keys_required  = $this->input->post('keysrequired');
		else  $tmp_keys_required = $building->keys_required;
			
		$keysrequired = array(
		    'name'        => 'keysrequired',
		    'id'          => 'keysrequired',
		    'value'       => '1',
		    'checked'     => $tmp_keys_required
		);
		

		$tmp_meetonsite = 1;
		
		
		$meetonsite = array(
		    'name'        => 'meetonsite',
		    'id'          => 'meetonsite',
		    'value'       => '1',
		    'checked'     => set_value('meetonsite',  $tmp_meetonsite),
		    
		);

		echo div_open('','clearfix'); echo div_x();


		echo div_open();
			echo form_checkbox($meetonsite);
			$checklabelattr = array(
			    'class' => 'checklabel'
			);
			echo form_label('Is a Meet Onsite Required?', 'meetonsite',$checklabelattr);
		echo div_x();

		echo '<div id="autoUpdate" class="autoUpdate">';
		
		echo p("<strong>Onsite Representative:</strong> (if applicable)");
			$onsiterep = array(
				'name'	=> 'onsitecontact',
				'id'	=> 'onsitecontact',
				'size'	=> '30',
				'value'	=> set_value('onsitecontact', $building->onsitecontact)
				
			);
			echo div_open();
				echo form_label('Name:', 'onsitecontact');
				echo form_input($onsiterep);
			echo div_x();
			
			
			$osrcontactnumber = array(
				'name'	=> 'onsitecontactphone',
				'id'	=> 'onsitecontactphone',
				'size'	=> '16',
				'value'	=> set_value('onsitecontactphone', $building->onsitecontactphone)
			);
			echo div_open();
				echo form_label('Contact Number:', 'onsitecontactphone');
				echo form_input($osrcontactnumber);
			echo div_x();
			

		echo '</div>';

		echo div_open('','clearfix'); echo div_x();


		
		$tmp_buildingmngr =1;
		
		
		$buildingmngr = array(
		    'name'        => 'buildingmngr',
		    'id'          => 'buildingmngr',
		    'value'       => '1',
		    'checked'     => set_value('buildingmngr',  $tmp_buildingmngr)
		);

		echo div_open();
			echo form_checkbox($buildingmngr);
			$checklabelattr = array(
			    'class' => 'checklabel'
			);
			echo form_label('Is there a Building Manager?', 'buildingmngr',$checklabelattr);
		echo div_x();


		echo '<div id="autoBMan" class="autoBMan">';

		echo p("<strong>Building Manager:</strong> (if applicable)");
			$buildingmanager = array(
				'name'	=> 'buildingmanager',
				'id'	=> 'buildingmanager',
				'size'	=> '30',
				'value'	=> set_value('buildingmanager', $building->buildingmanager)
			);
			echo div_open();
				echo form_label('Name:', 'buildingmanager');
				echo form_input($buildingmanager);
			echo div_x();
			
			$bmcontactnumber = array(
				'name'	=> 'buildingmanagerphone',
				'id'	=> 'buildingmanagerphone',
				'size'	=> '16',
				'value'	=> set_value('buildingmanagerphone', $building->buildingmanagerphone)
			);
			echo div_open();
				echo form_label('Contact Number:', 'buildingmanagerphone');
				echo form_input($bmcontactnumber);
			echo div_x();

		echo '</div>';


		echo div_open();
			echo form_checkbox($keysrequired);
			$checklabelattr = array(
			    'class' => 'checklabel'
			);
			echo form_label('Keys are required for access to common property areas', 'keysrequired',$checklabelattr);
		echo div_x();


		$tmp_keydetails = "";
		if($this->input->post('keydetails')) $tmp_keydetails  = $this->input->post('keydetails');
		else  $tmp_keydetails = $building->keydetails;
			
		$keydetails = array(
			'name'	=> 'keydetails',
			'id'	=> 'keydetails',
			'rows'	=> '3',
			'cols' 	=> '35',
			'value'	=> set_value('keydetails')
		);
		echo div_open();
			echo form_label('Key Details:', 'keydetails');
			?>
			<textarea rows=3 cols=35 name=keydetails id=keydetails><?= $tmp_keydetails?></textarea>
			<?php
		echo div_x();
				
	echo form_fieldset_close();

	echo form_fieldset('Strata Manager');
		echo div_open('', 'left');
			$stratamanagername = array(
				'name'	=> 'mgmt_contact',
				'id'	=> 'mgmt_contact',
				'size'	=> '30',
				'value'	=> set_value('mgmt_contact', $building->mgmt_contact)
			);
			echo div_open();
				echo form_label('Name:', 'mgmt_contact');
				echo form_input($stratamanagername);
				echo "<span class='required'>*</span>";
			echo div_x();
			
			$stratamanagerphone = array(
				'name'	=> 'mgmt_contact_phone',
				'id'	=> 'mgmt_contact_phone',
				'size'	=> '16',
				'value'	=> set_value('mgmt_contact_phone', $building->mgmt_contact_phone)
			);
			echo div_open();
				echo form_label('Phone:', 'mgmt_contact_phone');
				echo form_input($stratamanagerphone);
			echo div_x();
				
		echo div_x();

		echo div_open('', 'right');
			echo div_open();
				echo form_label('Company:', 'correspondcompany');
				echo "<span class=fixedvalue>".$mgmtco->name."</span>";
			echo div_x();
			
			if ($building->mgmt_contact_email != ""){
				$stratamanageremail = array(
				'name'	=> 'mgmt_contact_email',
				'id'	=> 'mgmt_contact_email',
				'size'	=> '30',
				'value'	=> set_value('mgmt_contact_email', $building->mgmt_contact_email)
				);
			} else {		
				$stratamanageremail = array(
				'name'	=> 'mgmt_contact_email',
				'id'	=> 'mgmt_contact_email',
				'size'	=> '30',
				'value'	=> set_value('mgmt_contact_email', $mgmtco->email)
				);
			}
			echo div_open();
				echo form_label('Email:', 'mgmt_contact_email');
				echo form_input($stratamanageremail);
			echo div_x();
				
		echo div_x();		

	echo form_fieldset_close();

	

	echo form_fieldset('Correspondence To');

		echo div_open();
			$labelwide = array(
				'class'	=> 'checkboxlabel'
			);
			$usestratamanager = array(
    			'name'        => 'usestratamanager',
    			'id'          => 'usestratamanager',
    			'value'       => 1,
    			'checked'     => FALSE
    		);
			echo form_label('Send all correspondence to Strata Manager:', 'usestratamanager', $labelwide);
			echo form_checkbox($usestratamanager);
		echo div_x();
		echo div_open('correspondence');
			echo div_open('', 'left');
				$correspondname = array(
					'name'	=> 'correspondname',
					'id'	=> 'correspondname',
					'size'	=> '30',
					'value'	=> set_value('correspondname', $mgmtco->contact_person_name)
				);
				echo div_open();
					echo form_label('Name:', 'correspondname');
					echo form_input($correspondname);
					echo "<span class='required'>*</span>";
				echo div_x();
				
				$correspondphone = array(
					'name'	=> 'correspondphone',
					'id'	=> 'correspondphone',
					'size'	=> '16',
					'value'	=> set_value('correspondphone', $mgmtco->contact_person_phone)
				);
				echo div_open();
					echo form_label('Phone:', 'correspondphone');
					echo form_input($correspondphone);
				echo div_x();
					
			echo div_x();
	
			echo div_open('', 'right');
				
				$correspondemail = array(
					'name'	=> 'correspondemail',
					'id'	=> 'correspondemail',
					'size'	=> '30',
					'value'	=> set_value('correspondemail', $mgmtco->contact_person_email)//$building->correspondemail)
				);
				echo div_open();
					echo form_label('Email:', 'correspondemail');
					echo form_input($correspondemail);
					echo "<span class='required'>*</span>";
				echo div_x();
					
			echo div_x();		
		echo div_x(); 

	echo form_fieldset_close();

	echo div_open('notes');
		echo p("<span class='required'>*</span><span class='definition'> - required information</span>");
	echo div_x();
	echo div_open('buttons');
		$submit = array(
			'name'	=> 'submit',
			'id'	=> 'submit',
			'class'	=> 'awesome large dkgreen',
			'type'	=> 'submit',
		    'content' => 'Save &amp; Continue'
		);
		$reset = array(
			'name'	=> 'reset',
			'id'	=> 'reset',
			'class'	=> 'awesome large red',
			'type'	=> 'reset',
		    'content' => 'Reset Screen'
		);
		echo form_button($reset);
		echo form_button($submit);

	echo div_x();

	echo div_open('','clearfix');	echo div_x();

	
	echo form_close();
?>
<script>
	$(function() {
		$('tr:even').addClass('alt');

		$('#help').click(function() {
			var src = "/clients/help/page/3";
			$.ajax ({
				type:		'GET',
				url:		src,
				success:	function(data) {
					$("#pop-up .access-form div").html(data);
				}
			});


			$("#pop-up").show();
			return false;
		});
		$('.close-the-window').click(function() {
			$("#pop-up").hide();
		});
		
		$("#state").change(function() {
			var state = $(this).val();
			if (state == 351) { // QLD
				$("#plannumberlabel").html('CTS Number:');
			} else if (state == 350) { // NSW
				$("#plannumberlabel").html('Strata Plan Number:');
			} else if (state == 352) { // NSW
				$("#plannumberlabel").html('Plan of Subdivision:');
			} else {
				$("#plannumberlabel").html('Plan Number:');
			}
			var html = "/data/buildingtypes/"+state;
			$.ajax({
				url: html,
 				success: function(data){
    				$("#buildingtype").html(data);
  				}
			});
		});
		
		$("#usestratamanager").click(function() {
			if($(this).attr("checked") == true) {
				var mgmt_contact = $('#mgmt_contact').val();
				var mgmt_contact_phone = $('#mgmt_contact_phone').val();
				var mgmt_contact_email = $('#mgmt_contact_email').val();
				$('#correspondname').val(mgmt_contact);
				$('#correspondphone').val(mgmt_contact_phone);
				$('#correspondemail').val(mgmt_contact_email);
			} else {
				$('#correspondname').val('');
				$('#correspondphone').val('');
				$('#correspondemail').val('');
			}
			
		});

		$('#meetonsite').change(function(){
			if(this.checked)
			$('#autoUpdate').fadeIn('slow');
			else
			$('#autoUpdate').fadeOut('slow');

			});

		$('#buildingmngr').change(function(){
			if(this.checked)
			$('#autoBMan').fadeIn('slow');
			else
			$('#autoBMan').fadeOut('slow');

			});
		
	});

</script>
