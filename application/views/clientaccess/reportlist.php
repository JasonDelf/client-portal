<script>
	$(function() {
		$('tr:even').addClass('alt');

		$('#help').click(function() {
			var src = "/help/page/2";
			$.ajax ({
				type:		'GET',
				url:		src,
				success:	function(data) {
					$("#pop-up .access-form div").html(data);
				}
			});


			$("#pop-up").show();
			return false;
		});
		$('.close-the-window').click(function() {
			$("#pop-up").hide();
		});
		
		//highlight report and add to selection for processing
		$('.reportbutton').click(function() {
			var reportid = $(this).attr('id');
			$(this).toggleClass('marked');
			return false;
		});
		
		//reset all reports back to unselected
		$("#reset").click(function() {
			$('.reportbutton').each(function() {
				$(this).removeClass('marked');
			});
			$("#currentselection").val('');

		});
		
		$("#changebldg").click(function() {
			window.location='/building/reset';
		});
		
		$("#currentselection").val('');
		
		$("#continue").click(function() {
			var selected = '';
			$('.reportbutton').each(function() {
				if ($(this).hasClass('marked')) {
					if (selected != '') {
						selected += ',';
					} 
					selected += $(this).attr('id');
				}
			});
			$("#currentselection").val(selected);

			$("#frmreports").submit();			
		});
		
	});
</script>
<div class='col66'>
	<?=heading("Reports", 1)?>
</div>
<div class='col33 rightalign'>
	<?php
		$helpbtn = array(
			'name'	=> 'help',
			'id'	=> 'help',
			'src'	=> 'library/images/admin/help.png'
		);
		$anchor = array(
			'title' => ' Help ',
			'id'	=> 'help'
		);
		echo anchor('#', img( $helpbtn), $anchor);
	?>
</div>
<div class='clearfix'></div>
<p>Please select 1 to 4 reports, then click on the "Continue with Request" button.</p>

<div id="reportlist">
<?php
	$client = $this->session->userdata('clientid');
	$buildingid = $this->session->userdata('buildingid');
	$building = $this->buildings->get($buildingid );
	$attr = array(
		'id'	=> 'frmreports'
	);
	echo form_open('report/selection/1', $attr);	
		if ($this->session->userdata('selection')) {
			$reportchosen = $this->session->userdata('selection');
            if (empty($reportchosen[0]))
                print_r("Please choose at least one report");
            
			foreach ($reportchosen as $rc) {
                $chosenid[] = $rc->id;
			}
		} else {
			$chosenid = array();
		}
		foreach ($reports as $report) {
			if (in_array($report->id, $chosenid )) {
				$class = 'reportbutton marked';
			} else {
				$class = 'reportbutton';
			}
		
			echo anchor('',$report->report_name, array('class' => $class, 'id'	=> $report->id));
		}
		
		?>
		<input type="hidden" id="currentselection" name="currentselection" />
		<?php	
		echo div_open('','clearfix');
		echo div_x();	
		echo div_open('buttons','rightalign');
			$changebldg = array(
				'id'	=> 'changebldg',
				'name'	=> 'changebldg',
				'content'	=> 'Change Building',
				'class'	=> 'awesome large blue'
			);
			echo form_button($changebldg);
			$reset = array(
				'id'	=> 'reset',
				'name'	=> 'reset',
				'content'	=> 'Reset Selection',
				'class'	=> 'awesome large orange'
			);
			echo form_button($reset);
			$continue = array(
				'id'	=> 'continue',
				'name'	=> 'continue',
				'type'	=> 'button',
				'content'	=> 'Continue with Request',
				'class'	=> 'awesome large dkgreen'
			);
			echo form_button($continue);
		echo div_x();
		echo div_open('','clearfix');
		echo div_x();	

	echo form_close();
?>
</div>

