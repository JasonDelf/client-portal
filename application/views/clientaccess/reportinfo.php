<script>
	$(function() {
		$(".datepick" ).datepicker( {dateFormat: 'dd/mm/yy' });
	});
</script>
<?php
	$client = $this->session->userdata('clientid');
	$buildingid = $this->session->userdata('buildingid');
	$buildings = $this->buildings->get($buildingid );

	$formattr = array(
			'id'	=> 'reportinfo'.$report->id,
			'class'	=> 'reportinfo'
		);
		$optiongrouplabelattr = array(
			    'class' => 'optiongrplabel'
			);
		echo div_open('submitresponse'.$order, 'message');
		echo div_x();
		echo form_open('report/information/'.$report->id, $formattr);
		echo form_fieldset();
		?>
		<div id="requesttype">
			<label for="request1<?=$order?>">Quote</label><input type="radio" name="request<?=$order?>" id="request1<?=$order?>" value="Quote" <?=set_radio('request'.$order, 'Quote', ($request == 'Quote'))?>>
			<label for="request2<?=$order?>">Order</label><input type="radio" name="request<?=$order?>" id="request2<?=$order?>" value="Order" <?=set_radio('request'.$order, 'Order', ($request == 'Order'))?>>
			<br/><br/>
			<?php
				
			?>
			<label for="requestdate" class="wide">Report requested by:</label><input type="text" class="datepick" name="requestby<?=$order?>" id="requestby<?=$order?>" size="14" value="<?php echo set_value('requestby'.$order, $requestby);   ?>" /><span class='datenote'>Leave blank if no deadline</span>
		</div>
		<input type="hidden" name="orderid" id="orderid" value="<?=$order?>" />
		<?php
		foreach ($reportquestions as $reportquestion) {
			$qtype = $reportquestion->questiontype;
			
			$input = array(
				'name'	=> $reportquestion->questiontag,
				'class'	=> 'input'
			);
			if ($qtype == 'textsmall') {
				$input['size'] = '6';
			} elseif ($qtype == 'textmed') {
				$input['size'] = '20';
			} elseif ($qtype == 'textlarge') {
				$input['size'] = '40';
			}
			echo "<div";
			if ($reportquestion->dependson > 0) {
				echo " class='tip' trigger='".$reportquestion->dependsif."'";
			}
			echo ">";
			echo form_label($reportquestion->question, $reportquestion->questiontag);
			if ($reportquestion->requiredif) {
				echo "<input type='hidden' class='hidden' name='req".$reportquestion->questiontag."' id='req".$reportquestion->questiontag."' value='1' />";
			}
	
			if (in_array($qtype, array('textsmall','textmed','textlarge'))) {
				$input['id'] = $reportquestion->questiontag;
				$input['value'] = set_value($reportquestion->questiontag);
				if (isset($storedanswers)) {
					if (sizeof($storedanswers) > 0) {
						$default = ($storedanswers[$reportquestion->questiontag] ? $storedanswers[$reportquestion->questiontag]: '');
					} else {
						$default = '';
					}
				} else {
					$default = '';
				}
				$input['value'] = set_value($reportquestion->questiontag, $default);
				echo form_input($input);
				if (($reportquestion->required) || ($reportquestion->requiredif)) echo "<span class='required'>*</span>";
			} elseif ($qtype == 'textarea') {
				$input['id'] = $reportquestion->questiontag;
				$input['rows'] = '3';
				$input['cols'] = '60';
				if (isset($storedanswers)) {
					if (sizeof($storedanswers) > 0) {
						$default = ($storedanswers[$reportquestion->questiontag] ? $storedanswers[$reportquestion->questiontag]: '');
					} else {
						$default = '';
					}
				} else {
					$default = '';
				}
				$input['value'] = set_value($reportquestion->questiontag, $default);
				echo form_textarea($input);
				if ($reportquestion->required) echo "<span class='required'>*</span>";
			} elseif ($qtype == 'singleselect') {
				$options = explode(',',$reportquestion->options);
				for ($i = 0; $i < sizeof($options); $i ++) {
					$input['value'] = $options[$i];
					$default = FALSE;
					if (sizeof($storedanswers) > 0) {
						if (isset($storedanswers[$reportquestion->questiontag]) && ($storedanswers[$reportquestion->questiontag] == $options[$i])) {
							$default = TRUE;
						}
					}		

					if ($default) {
						$input['checked'] = TRUE;//set_radio($reportquestion->questiontag, $options[$i], TRUE);
					} else {
						$input['checked'] = FALSE;//set_radio($reportquestion->questiontag, $options[$i], FALSE);				
					}
					$input['id'] = $reportquestion->questiontag.$i;
					//$input['checked'] = set_radio($reportquestion->questiontag, $options[$i]);
					echo div_open('','optiongrp');
						echo form_label($options[$i], $input['id'],$optiongrouplabelattr);
						echo form_radio($input);
					echo div_x();
				}
				if ($reportquestion->required) echo "<span class='required'>*</span>";
			} elseif ($qtype == 'multiselect') {
				$options = explode(',',$reportquestion->options);
				for ($i = 0; $i < sizeof($options); $i ++) {
					$input['id'] = $reportquestion->questiontag.$i;
					$input['value'] = $options[$i];
					$input['checked'] = set_checkbox($reportquestion->questiontag, $options[$i]);
					echo div_open('','optiongrp');
						echo form_label($options[$i], $input['id'],$optiongrouplabelattr);
						echo form_checkbox($input);
					echo div_x();
				}
			} elseif ($qtype == "date") {
				if (isset($storedanswers)) {
					if (sizeof($storedanswers) > 0) {
						$default = ($storedanswers[$reportquestion->questiontag] ? $storedanswers[$reportquestion->questiontag]: '');
					} else {
						$default = '';
					}
				}
	
				$input['id'] = $reportquestion->questiontag;
				$input['class'] = 'datefield';
				$input['size'] = '15';
				$input['value'] = set_value($reportquestion->questiontag, $default);
				echo form_input($input);
				
			
			} elseif ($qtype == "dropdown") {
				if (isset($storedanswers)) {
					if (sizeof($storedanswers) > 0) {
						$default = ($storedanswers[$reportquestion->questiontag] ? $storedanswers[$reportquestion->questiontag]: '');
					} else {
						$default = '';
					}
				}
				$input['id'] = $reportquestion->questiontag;
				$alloptions = explode(",",$reportquestion->options);
				foreach ($alloptions as $entry) {
					$options[$entry] = $entry;
				}
				echo form_dropdown($reportquestion->questiontag, $options, $default);
			}
			if ($tips[$reportquestion->id]) {
				echo "<div id='tip".$reportquestion->id."' class='tip warning' trigger='".$tips[$reportquestion->id][0]->answer."'>";
					echo p($tips[$reportquestion->id][0]->tip);
				echo "</div>";
			}
			echo "</div>";
			
		}
		
		echo form_fieldset_close();
		echo p("<span class='required'>*</span> - Required information");
	
		echo div_open('','clearfix');
		echo div_x();
	
		echo div_open('buttons');
			echo div_open('','notice');
				echo p("You need to <strong>save</strong> any entered details on this screen before moving to another report tab");
			echo div_x();
			$reset = array(
				'name'	=> 'reset',
				'id'	=> 'reset'.$order,
				'class'	=> 'awesome large orange',
				'type'	=> 'reset',
			    'content' => 'Reset Screen'
			);
	
			$cancel = array(
				'name'	=> 'cancel',
				'id'	=> 'cancel'.$order,
				'class'	=> 'awesome large red cancel',
				'type'	=> 'button',
			    'content' => 'Cancel this Report'
			);
			
			$submit = array(
				'name'	=> 'submit',
				'id'	=> 'submit'.$order,
				'class'	=> 'awesome large dkgreen submit',
				'type'	=> 'submit',
			    'content' => 'Save details'
			);
	
			echo form_button($cancel);	
			echo form_button($reset);
			echo form_button($submit);
	
		echo div_x();
	
		
		echo form_close();
	
?>
<div class='clearfix'></div>

<script>
	$(function() {

			$(".tip").hide();
			$(".message").hide();
			
			var label = '';
				
			//check if anything should be revealed when page loads (such as coming back to page, or reloading)
		
			$('fieldset div').each(function(i) {
				getlabel = $(this).children('.optiongrp').children('input:checked').prev().html();
				if (getlabel != null) {
					label = getlabel;
				}
			});
				

			$('fieldset div.tip').each(function(i) {
				if ($(this).attr('trigger') == label) {
					$(this).show();
				} else {
					$(this).children('.input').val('');
				}
			});
			

		$('.cancel').click(function() {
			var buttonid = $(this).attr('id');
			var orderid = buttonid.substr(6, 5);
			src = "/report/cancel/"+orderid;
			$.ajax ({
				type:		'GET',
				url:		src,
				success:	function(data) {
					$("#pop-up .access-form div").html(data);
				}
			});
	
	
			$("#pop-up").show();
			return false;

		});
		
	    var options = { 
			success:	showResponse
		};

	    $('.reportinfo').ajaxForm(options); 
	
		function showResponse(responseText, statusText, xhr, $form) {
			var nexttab = $("#submitresponse"+<?=$order?>).parent().next().attr('id');
			$("#submitresponse"+<?=$order?>).html(responseText).show('slow').delay(2000).hide('slow').delay(500);//.parent().parent().tabs('select', nexttab);
		}
		
		//show relevant tip/box when an option button is selected
		
		$('.optiongrp input').click(function() {
			var label = $(this).prev().html();
			
			//tip box check
			$(this).parent().siblings().filter(function() {
				return ($(this).hasClass('tip'));
			}).hide();
			$(this).parent().siblings().filter(function() {
				return ($(this).attr('trigger') == label);
			}).show();
	
			// extra questions to show or hide
			$(this).parent().parent().siblings().filter(function() {
				return ($(this).hasClass('tip'));
			}).hide();
			$(this).parent().parent().siblings().filter(function() {
				return ($(this).attr('trigger') == label);
			}).show();
			
		});
		
		$( ".datefield" ).datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "d M, yy"
		});
		
	});
	
	
</script>

