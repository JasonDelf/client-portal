<?php
	
	echo heading("Order complete", 1);

	echo validation_errors();	
	
		echo p("Thank you for ordering your reports from ".COMPANY.". You should receive an email with the details you have supplied to us, for your records.");
		echo br(2);
		foreach ($reports as $report) {
			echo p($report);
		}
		echo br(2);
		echo div_open('','centred');
			$changebuilding = array(
				'name'	=> 'submit',
				'id'	=> 'submit',
				'class'	=> 'awesome large dkgreen',
				'type'	=> 'submit',
			    'content' => 'Order Request'
			);
			echo anchor("/clients/", "Choose Another Building", $changebuilding);
			echo br(2);

		echo div_x();
		echo div_open('','clearfix');
		echo div_x();

?>


