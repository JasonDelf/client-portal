<?=br(2)?>
<p class='notice'>
	This building is currently in our system managed by a different company. Do you wish to take over management of this building?
</p>
<?php
	foreach ($building as $found) {
	  echo "<div class='bldgblock'>";
		echo "<div class='bldgdetails'>";
			echo $found->building_name.br();
			echo $found->building_address.br();
			$state = $this->listitems->get_by('id', $found->building_state)->itemvalue;
			echo $found->building_suburb.nbs().$state.nbs().$found->building_postcode.br();
		echo "</div>";
		
		echo "<div class='bldgbtns'>";
			$takeover = array(
				'id'	=> $found->id,
				'name'	=> 'takeover'.$found->id,
				'content'	=> 'Takeover',
				'type'	=> 'button',
				'class'	=> 'awesome large dkgreen takeover'
			);
			echo form_button($takeover);
		echo "</div>";
	  echo "</div>";
	}
	
	echo "<div id='createbtn'>";
		$new = array(
			'id'	=> 'new',
			'name'	=> 'new',
			'content'	=> 'Create a new building',
			'type'	=> 'button',
			'class'	=> 'awesome large blue'
		);
		echo form_button($new);
	echo "</div>";
?>
<script>
	$(function() {
		$(".takeover").click(function() {
			var building = $(this).attr('id');
			var href='/building/changemanager/'+building;
			alert(href);
			$.ajax ({
				type:		'GET',
				url:		href,
				success:	function(data) {
					$("#results").html(data);
					$("#mainbody").load('clientaccess/propertylist');

				}
			});

		});
	
		$("#new").click(function() {
			var plan = $("#plannumber").val();
			var href='../building/newb/'+plan;
			window.location = href;
		});
	});
	
</script>

<!--
<script>
	$(function() {
		$("#new").live('click',function() {
			var plan = $("#plannumber").val();
			var href='../building/newb/'+plan;
			window.location = href;
		});
	
		$("#takeover").live('click',function() {
			var href='/building/changemanager/<?=$building->id?>';
			$.ajax ({
				type:		'GET',
				url:		href,
				success:	function(data) {
					$("#results").html(data);
					$("#mainbody").load('clientaccess/propertylist');

				}
			});


		});
	});
	
	

</script>
-->