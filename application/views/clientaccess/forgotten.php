<?= doctype('html5')?>
<head>
	<title>Client Services :: by Solutions in Engineering</title>
	<?= link_tag('reset')?>
	<?= link_tag('login')?>
	<?= link_tag('buttons')?>
	<?= script_tag('jquery')?>
</head>
<body>
	<?php
		echo div_open('login', 'login');
		echo form_open('clients/forgotten');
		echo validation_errors();
		if (isset($message)) {
			echo div_open('', 'error');
			echo p($message);
			echo div_x();
		}
		echo "<div>";
		echo p("Enter your registered email address, and we will send your login details along with a new password for your account");
		echo form_label('Email address','email');
		$email = array(
			'id'	=> 'email',
			'name'	=> 'email',
			'size'	=> '40'
		);
		echo form_input($email);
		echo "</div>";
		echo "<div>";
		echo form_label('&nbsp;');
		$reset = array(
			'id'		=> 'btnNewPW',
			'name'		=> 'btnNewPW',
			'type'		=> 'submit',
			'class'		=> 'awesome dkgreen large',
			'content'	=> 'Get Login Details'
		);
		echo form_button($reset);

		echo "</div>";
		echo form_close();
	
		echo div_x();
	?>
</body>
</html>