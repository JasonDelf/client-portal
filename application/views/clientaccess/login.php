<?= doctype('html5')?>
<head>
	<title>Client Services :: by Solutions in Engineering</title>
	<?= link_tag('reset')?>
	<?= link_tag('login')?>
	<?= link_tag('buttons')?>
	<?= script_tag('jquery.min')?>
</head>
<body>
	<?php
		echo div_open('login', 'login');
		echo form_open('clients/login');
		if (isset($message) && ($message != "")) {
			echo div_open('errormsg');
			echo p($message);
			echo div_x();
		}
		echo "<div>";
		echo form_label('Username','username');
		$username = array(
			'id'	=> 'username',
			'name'	=> 'username',
			'size'	=> '20'
		);
		echo form_input($username);
		echo "</div><div>";
		echo form_label('Password','password');
		$password = array(
			'id'	=> 'password',
			'name'	=> 'password',
			'size'	=> '20'
		);
		echo form_password($password);
		echo "</div><div>";
		echo form_label('&nbsp;');
		$login = array(
			'id'		=> 'btnLogin',
			'name'		=> 'btnLogin',
			'type'		=> 'submit',
			'class'		=> 'awesome dkgreen large',
			'content'	=> 'Log in'
		);
		echo form_button($login);
		echo "<br/>";
		echo anchor('clients/forgotten',"Forgotten or don't know your username and/or password?");
		echo "</div>";
		echo form_close();
	
		echo div_x();
	?>
</body>
</html>