<?php
	echo heading("Help", 2);
	echo heading("Report Questions", 3);
	echo p("Add new questions that need to be asked of clients for this report.");
	echo p("Edit existing questions by clicking on the question and setting the appropriate options.");
	echo p("Drag the questions into the required order.");
	?>
