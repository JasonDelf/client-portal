<?php
	echo heading("Help", 2);
	echo heading("Property Details", 3);
	echo p("This screen shows details about your property as currently stored in our system. Please check that this is all correct and make any necessary alterations.");
	echo p("We also require details about any onsite meetings necessary for this report, as well as to whom the completed report should be sent.");
	?>
