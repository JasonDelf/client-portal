<?php
	echo heading("Help", 2);
	echo heading("Building Plans", 3);
	echo p("To perform an inspection on your property, and therefore the required report, we need to have a copy of the plans for this building.");
	echo p("It is likely that we already have a copy of the plans from a previous report. If so, select the first option below.");
	echo p("<strong>Note:</strong> The list of plans on file currently only shows plans that have been submitted through this new system.  Over time, this will be updated to include all plans on file.");
	echo p("If you are uploading a plan, please give the file an appropriate name for easy reference.");
	?>
