	<?= link_tag('main')?>
	<?= link_tag('buttons')?>
	<?= script_tag('jquery.min')?>
	<?= script_tag('jquery.form')?>
<body class='popup'>
<?php
	echo heading("Account Details", 1);
	echo validation_errors();	
?>
<?php
	$formattr = array(
		'id'	=> 'accountform'
	);
	if (isset($message)) {
		echo div_open("message");
			echo p($message);
		echo div_x();
	}
	echo div_open('response');
		echo p("Details saved");
	echo div_x();
	
	echo form_open('clients/account', $formattr);
		echo form_fieldset("Company Details");
			echo div_open();
				$clientname = array(
					'id'	=> 'name',
					'name'	=> 'name',
					'size'	=> '30',
					'value'	=> $user->name
				);
				echo form_label("Name", 'name');
				echo form_input($clientname);
				echo "<span class='required'>*</span>";
			echo div_x();
	
			echo div_open();
				$address = array(
					'id'	=> 'address',
					'name'	=> 'address',
					'size'	=> '50',
					'value'	=> $user->address
				);
				echo form_label("Address", 'address');
				echo form_input($address);
				echo "<span class='required'>*</span>";
			echo div_x();
	
			echo div_open();
				$suburb = array(
					'id'	=> 'suburb',
					'name'	=> 'suburb',
					'size'	=> '30',
					'value'	=> $user->suburb
				);
				echo form_label("Suburb", 'suburb');
				echo form_input($suburb);
				echo "<span class='required'>*</span>";
			echo div_x();
	
			echo div_open('','half');
				$statelistid = $this->lists->get_by('listname', 'States')->id;			
				$statelistitems = $this->listitems->get_many_by('listid', $statelistid);
				foreach ($statelistitems as $rowitem) {
					$states[$rowitem->id] = $rowitem->itemvalue;
				}
				echo form_label("Country/State", 'state');
				echo form_dropdown('state', $states, $user->state);
				echo "<span class='required'>*</span>";
			echo div_x();
	
			echo div_open('', 'half');
				$postcode = array(
					'id'	=> 'postcode',
					'name'	=> 'postcode',
					'size'	=> '4',
					'maxlength'	=> '4',
					'value'	=> $user->postcode
				);
				echo form_label("Postcode", 'postcode');
				echo form_input($postcode);
				echo "<span class='required'>*</span>";
			echo div_x();
				
			echo div_open();
				echo br(1);
				$phone1 = array(
					'id'	=> 'phone1',
					'name'	=> 'phone1',
					'size'	=> '15',
					'value'	=> $user->phone1
				);
				echo form_label("Phone 1", 'phone1');
				echo form_input($phone1);
				echo "<span class='required'>*</span>";
			echo div_x();
						
			echo div_open();
				$phone2 = array(
					'id'	=> 'phone2',
					'name'	=> 'phone2',
					'size'	=> '15',
					'value'	=> $user->phone2
				);
				echo form_label("Phone 2", 'phone2');
				echo form_input($phone2);
			echo div_x();

			echo div_open();
				$fax = array(
					'id'	=> 'fax',
					'name'	=> 'fax',
					'size'	=> '15',
					'value'	=> $user->fax
				);
				echo form_label("Fax", 'fax');
				echo form_input($fax);
			echo div_x();

			echo div_open();
				$email = array(
					'id'	=> 'email',
					'name'	=> 'email',
					'size'	=> '50',
					'value'	=> $user->email
				);
				echo form_label("Email", 'email');
				echo form_input($email);
				echo "<span class='required'>*</span>";
			echo div_x();

			echo div_open();
				echo br(1);
				$contact_name = array(
					'id'	=> 'contact_person_name',
					'name'	=> 'contact_person_name',
					'size'	=> '30',
					'value'	=>  $user->contact_person_name
				);
				echo form_label("Contact Person - Name", 'contact_person_name');
				echo form_input($contact_name);
			echo div_x();
			echo div_open();
				$contact_phone = array(
					'id'	=> 'contact_person_phone',
					'name'	=> 'contact_person_phone',
					'size'	=> '15',
					'value'	=>  $user->contact_person_phone
				);
				echo form_label("Contact Person - Phone", 'contact_person_phone');
				echo form_input($contact_phone);
			echo div_x();
			echo div_open();
				$contact_email = array(
					'id'	=> 'contact_person_email',
					'name'	=> 'contact_person_email',
					'size'	=> '50',
					'value'	=>  $user->contact_person_email
				);
				echo form_label("Contact Person - Email", 'contact_person_email');
				echo form_input($contact_email);
			echo div_x();

			echo div_open();
				echo br(2);
				$reset = array(
					'id'	=> 'reset',
					'name'	=> 'reset',
					'type'	=> 'reset',
					'content'	=> 'Reset',
					'class'	=> 'awesome medium orange'
				);
				$submit = array(
					'id'	=> 'submit',
					'name'	=> 'submit',
					'type'	=> 'submit',
					'content'	=> 'Save',
					'class'	=> 'awesome medium dkgreen'
				);
				echo form_label("", '');
				echo form_button($reset).nbs(4).form_button($submit);
			echo div_x();
			
		echo form_fieldset_close();
	echo form_close();
	
	$formattr = array(
		'id'	=> 'passwordform'
	);
	
	echo form_open('clients/password', $formattr);
		echo div_open('response2');
			echo p("Password has been changed");
		echo div_x();

		echo form_fieldset("Login Details");
			echo div_open();
				$username= array(
					'id'	=> 'username',
					'name'	=> 'username',
					'disabled'	=> 'disabled',
					'size'	=> '30',
					'value'	=> $account->username
				);
				echo form_label("Username", 'username');
				echo form_input($username);
			echo div_x();

			echo div_open('changepwdbutton');
				$change = array(
					'id'	=> 'change',
					'name'	=> 'change',
					'title'	=> 'Change Password',
					'class'	=> 'awesome medium blue'
				);
				echo form_label("", '');
				echo anchor('#', "Change Password", $change);
			echo div_x();

			echo div_open('changepwd');
				echo div_open();
					$oldpwd= array(
						'id'	=> 'oldpassword',
						'name'	=> 'oldpassword',
						'size'	=> '20'
					);
					echo form_label("Current password", 'oldpassword');
					echo form_password($oldpwd);
				echo div_x();
				echo div_open();
					$newpwd= array(
						'id'	=> 'newpassword',
						'name'	=> 'newpassword',
						'size'	=> '20'
					);
					echo form_label("New password", 'newpassword');
					echo form_password($newpwd);
				echo div_x();
				echo div_open();
					$confirmpwd= array(
						'id'	=> 'confirmpassword',
						'name'	=> 'confirmpassword',
						'size'	=> '20'
					);
					echo form_label("Confirm password", 'confirmpassword');
					echo form_password($confirmpwd);
				echo div_x();
	
				echo div_open('');
					echo br();
					$change = array(
						'id'	=> 'savechange',
						'name'	=> 'savechange',
						'content'	=> 'Save New Password',
						'class'	=> 'awesome medium dkgreen',
						'type'	=> 'Submit'
					);
					echo form_label("", '');
					echo form_button($change); //'/account/password', "Change Password", $change);
				echo div_x();
			
			echo div_x();

		echo form_fieldset_close();
	
	echo form_close();

		echo form_fieldset("Individual Accounts");
			echo div_open('');
				$idaccounts = array(
					'id'	=> 'idaccounts',
					'name'	=> 'idaccounts',
					'class'	=> 'awesome medium dkblue'
				);
				echo anchor('clients/idaccounts', "Manage Individual Accounts", $idaccounts); //'/account/password', "Change Password", $change);
			echo div_x();

		echo form_fieldset_close();
	
	
	echo form_close();

?>
</body>
<script>
	$(function() {
		$("#change").click(function() {
			$(this).hide();
			$("#changepwd").show('fast');
			return false;	
		});
	
	    var options = { 
			success:	showResponse
		};

	    $('#accountform').ajaxForm(options); 
	
		function showResponse(responseText, statusText, xhr, $form) {
			$("#response").html(responseText).show('slow').delay(2000).hide('slow').delay(500);//.parent().parent().tabs('select', nexttab);
		}


	    var options2 = { 
			success:	showResponse2
		};

	    $('#passwordform').ajaxForm(options2); 
	
		function showResponse2(responseText, statusText, xhr, $form) {
			$("#response2").html(responseText).show('slow').delay(2000).hide('slow').delay(500);//.parent().parent().tabs('select', nexttab);
			$("#changepwd").hide();
			$("#change").show();
		}

	});
</script>
