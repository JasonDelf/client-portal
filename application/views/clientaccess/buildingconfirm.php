<?php
		echo br(2);

		$message = "<table width=95% id='confirmtable'>";

		$message .= "<tr><th width=40%>Building Name</th><td>".$building->building_name."</td>";
		$message .= "<tr><th width=40%>CTS/SP Number</th><td>".$building->building_cts."</td>";
		$message .= "<tr><th width=40%>Address</th><td>".$building->building_address."</td>";
		$message .= "<tr><th width=40%>Town/Suburb</th><td>".$building->building_suburb."</td>";
		$message .= "<tr><th width=40%>Postcode</th><td>".$building->building_postcode."</td>";
		$message .= "<tr><th width=40%>CTS/SP Number</th><td>".$building->building_cts."</td>";
		$message .= "<tr><th width=40%>Building Type</th><td>".$buildingtype."</td>";
		$message .= "<tr><th width=40%>Number of Units</th><td>".$building->building_num_units."</td>";
		$message .= "<tr><th width=40%>Construction Year</th><td>".$building->building_constructiondate."</td>";
		$message .= "<tr><th width=40%>Onsite Representative</th><td>".$building->onsitecontact." - ".$building->onsitecontactphone."</td>";
		$message .= "<tr><th width=40%>Building Manager</th><td>".$building->buildingmanager." - ".$building->buildingmanagerphone."</td>";
		$message .= "<tr><th width=40%>Meeting Onsite?</th><td>";
		if ($building->meetonsite == 1) {
			$message .= "Yes";
		} else {
			$message .= "No";
		}
		$message .= "</td>";

		$message .= "<tr><th width=40%>&nbsp;</th><td>&nbsp;</td>";

		$message .= "<tr><th width=40%>Management</th><td>".$management->name."</td>";
		$message .= "<tr><th width=40%>Phone</th><td>".$management->phone1."</td>";
		$message .= "<tr><th width=40%>Fax</th><td>".$management->fax."</td>";
		$message .= "<tr><th width=40%>Address</th><td>".$management->address."</td>";
		$message .= "<tr><th width=40%>Town/Suburb</th><td>".$management->suburb."</td>";
		if (isset($management->state)) {
			//print_r($management->state);
			$message .= "<tr><th width=40%>State</th><td>".$this->listitems->get($management->state)->itemvalue."</td>";
		}
		$message .= "<tr><th width=40%>Postcode</th><td>".$management->postcode."</td>";
		$message .= "<tr><th width=40%>Email</th><td>".$management->email."</td>";

		$message .= "</table>";

		echo $message;

?>


<script>
	$(function() {
		$('tr:even').addClass('alt');
	});
</script>