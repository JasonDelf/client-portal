	<?= link_tag('main')?>
	<?= link_tag('buttons')?>
<?php
	echo heading("Account Details", 1);
	echo validation_errors();	
?>
<?php

	$formattr = array(
		'id'	=> 'accountform'
	);
	if ($message != '') {
		echo div_open("message");
			echo p($message);
		echo div_x();
	}
	echo form_open('/account/password', $formattr);
		echo form_fieldset("Change Password");
			echo div_open();
				$oldpw = array(
					'id'	=> 'oldpw',
					'name'	=> 'oldpw',
					'size'	=> '30'
				);
				echo form_label("Current Password:", 'oldpw');
				echo form_password($oldpw);
				echo "<span class='required'>*</span>";
			echo div_x();
	
			echo div_open();
				$newpw = array(
					'id'	=> 'newpw',
					'name'	=> 'newpw',
					'size'	=> '30'
				);
				echo form_label("New Password:", 'newpw');
				echo form_password($newpw);
				echo "<span class='required'>*</span>";
			echo div_x();
	
			echo div_open();
				$conpw = array(
					'id'	=> 'conpw',
					'name'	=> 'conpw',
					'size'	=> '30'
				);
				echo form_label("Confirm Password:", 'conpw');
				echo form_password($conpw);
				echo "<span class='required'>*</span>";
			echo div_x();
	
			echo div_open();
				echo br(2);
				$reset = array(
					'id'	=> 'reset',
					'name'	=> 'reset',
					'type'	=> 'reset',
					'content'	=> 'Reset',
					'class'	=> 'awesome medium orange'
				);
				$submit = array(
					'id'	=> 'submit',
					'name'	=> 'submit',
					'type'	=> 'submit',
					'content'	=> 'Save',
					'class'	=> 'awesome medium dkgreen'
				);
				echo form_label("", '');
				echo form_button($reset).form_button($submit);
			echo div_x();
			
		echo form_fieldset_close();
	
	echo form_close();
	
	if ($this->session->userdata('accesslevel') != 9) {

		$back = array(
			'id'	=> 'back',
			'name'	=> 'back',
			'title'	=> 'Back to Account Details',
			'class'	=> 'awesome medium dkblue'
		);
		echo form_label("", '');
		echo anchor('/account', 'Back to Account Details', $back);
	}
?>


