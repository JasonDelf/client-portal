	<?= link_tag('main')?>
	<?= link_tag('buttons')?>
<?php
	echo heading("Account Details", 1);
	echo validation_errors();	
?>
<?php
	$formattr = array(
		'id'	=> 'accountform'
	);
	if ($message != '') {
		echo div_open("message");
			echo p($message);
		echo div_x();
	}
	echo form_open('/account', $formattr);
		echo form_fieldset("Company Details");
			echo div_open();
				$clientname = array(
					'id'	=> 'name',
					'name'	=> 'name',
					'size'	=> '30',
					'value'	=> $user->name
				);
				echo form_label("Name", 'name');
				echo form_input($clientname);
				echo "<span class='required'>*</span>";
			echo div_x();
	
			echo div_open();
				$address = array(
					'id'	=> 'address',
					'name'	=> 'address',
					'size'	=> '50',
					'value'	=> $user->address
				);
				echo form_label("Address", 'address');
				echo form_input($address);
				echo "<span class='required'>*</span>";
			echo div_x();
	
			echo div_open();
				$suburb = array(
					'id'	=> 'suburb',
					'name'	=> 'suburb',
					'size'	=> '30',
					'value'	=> $user->suburb
				);
				echo form_label("Suburb", 'suburb');
				echo form_input($suburb);
				echo "<span class='required'>*</span>";
			echo div_x();
	
			echo div_open('','half');
				$statelistid = $this->lists->get_by('listname', 'States')->id;			
				$statelistitems = $this->listitems->get_many_by('listid', $statelistid);
				foreach ($statelistitems as $rowitem) {
					$states[$rowitem->id] = $rowitem->itemvalue;
				}
				echo form_label("State", 'state');
				echo form_dropdown('state', $states, $user->state);
				echo "<span class='required'>*</span>";
			echo div_x();
	
			echo div_open('', 'half');
				$postcode = array(
					'id'	=> 'postcode',
					'name'	=> 'postcode',
					'size'	=> '4',
					'maxlength'	=> '4',
					'value'	=> $user->postcode
				);
				echo form_label("Postcode", 'postcode');
				echo form_input($postcode);
				echo "<span class='required'>*</span>";
			echo div_x();
				
			echo div_open();
				echo br(2);
				$phone1 = array(
					'id'	=> 'phone1',
					'name'	=> 'phone1',
					'size'	=> '15',
					'value'	=> $user->phone1
				);
				echo form_label("Phone 1", 'phone1');
				echo form_input($phone1);
				echo "<span class='required'>*</span>";
			echo div_x();
						
			echo div_open();
				$phone2 = array(
					'id'	=> 'phone2',
					'name'	=> 'phone2',
					'size'	=> '15',
					'value'	=> $user->phone2
				);
				echo form_label("Phone 2", 'phone2');
				echo form_input($phone2);
			echo div_x();

			echo div_open();
				$fax = array(
					'id'	=> 'fax',
					'name'	=> 'fax',
					'size'	=> '15',
					'value'	=> $user->fax
				);
				echo form_label("Fax", 'fax');
				echo form_input($fax);
			echo div_x();

			echo div_open();
				$email = array(
					'id'	=> 'email',
					'name'	=> 'email',
					'size'	=> '50',
					'value'	=> $user->email
				);
				echo form_label("Email", 'email');
				echo form_input($email);
				echo "<span class='required'>*</span>";
			echo div_x();

			echo div_open();
				echo br(2);
				$reset = array(
					'id'	=> 'reset',
					'name'	=> 'reset',
					'type'	=> 'reset',
					'content'	=> 'Reset',
					'class'	=> 'awesome medium orange'
				);
				$submit = array(
					'id'	=> 'submit',
					'name'	=> 'submit',
					'type'	=> 'submit',
					'content'	=> 'Save',
					'class'	=> 'awesome medium dkgreen'
				);
				echo form_label("", '');
				echo form_button($reset).form_button($submit);
			echo div_x();
			
		echo form_fieldset_close();

		echo form_fieldset("Login Details");
			echo div_open();
				$username= array(
					'id'	=> 'username',
					'name'	=> 'username',
					'disabled'	=> 'disabled',
					'size'	=> '30',
					'value'	=> $login->username
				);
				echo form_label("Username", 'username');
				echo form_input($username);
			echo div_x();

			echo div_open();
				$change = array(
					'id'	=> 'change',
					'name'	=> 'change',
					'title'	=> 'Change Password',
					'class'	=> 'awesome medium blue'
				);
				echo form_label("", '');
				echo anchor('/account/password', "Change Password", $change);
			echo div_x();

		echo form_fieldset_close();
	
	echo form_close();

?>


