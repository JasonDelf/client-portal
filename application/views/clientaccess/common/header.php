<?php
	echo doctype('html5');
?>
<head>
	<title><?=$pagetitle?></title>
	<?= link_tag('reset')?>
	<?= link_tag('clientbuttons')?>
	<?= link_tag('clientmain')?>
	<?= link_tag('clientpopup')?>
	<?= link_tag('jquery-ui')?>
	<?= link_tag('fancybox/jquery.fancybox', 'library/js/')?>
	
	<?=script_tag('jquery.min')?>
	<?= script_tag('jquery-ui.min')?>
	<?= script_tag('jquery.form')?>
	<?= script_tag('fancybox/jquery.fancybox')?>
	<?= script_tag('fancybox/jquery.easing')?>
	<?= script_tag('fancybox/jquery.mousewheel')?>

	<script>
		$(function() {
		
			$("a#account").fancybox({
				'autoScale'				: true,
				'hideOnOverlayClick'	: false,
				'titleShow'				: false,
				'type'					: 'iframe',
				'width'					: 800,
				'height'				: 700
			});

		})
	</script>
	<script type="text/javascript">

  		var _gaq = _gaq || [];
  		_gaq.push(['_setAccount', 'UA-3316354-11']);
  		_gaq.push(['_trackPageview']);

  		(function() {
    		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  		})();

	</script>
</head>
<body>
	<?php $this->load->view('clientaccess/common/popup');?>

	<div id="container">
		<header>
			<div id="titles"></div>
			<div id="tagline">
				<div id="welcome">
					<?php
					if ($this->session->userdata('accesslevel') == 5) {
						$username = $this->session->userdata('fullname').", ";
					} else {
						$username = "Main Account, ";
					}
					
					echo heading("Welcome, ".$username.$this->session->userdata('clientname'),2);
					?>
				</div>
				<div id="headerbuttons">
					<?php
						$accountdetails = array(
							'title'	=> 'Account',
							'class'	=> 'awesome large orange',
							'id'	=> 'account'
						);
						$logout = array(
							'title'	=> 'Logout',
							'class'	=> 'awesome large red'
						);
						echo anchor("/clients/account", "Account", $accountdetails);
					?>
					<?=anchor("/clients/logout", "Logout", $logout)?>
				</div>
				<div class='clearfix'></div>
			</div>
			<div class='clearfix'></div>
		</header>
		<?=div_open("mainbody")?>