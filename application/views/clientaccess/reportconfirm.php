<?php
		echo div_open('buttons');
			$delete = array(
				'id'	=> 'delreport'.$order->id,
				'name'	=> 'delreport',
				'content' => 'Cancel this report',
				'class'	=> 'awesome large red delreport'
			);
			echo form_button($delete);
			$edit = array(
				'id'	=> 'editreport'.$order->id,
				'name'	=> 'editreport',
				'content' => 'Edit these details',
				'class'	=> 'awesome large blue editreport'
			);
			echo form_button($edit);
		echo div_x();
		echo div_open('','clearfix');
		echo div_x();
			echo br();
		
		$message = '';

		$message .= "<table width=95% id='confirmtable'>";
		
		$message .= "<tr><th colspan=2 class='heading'>".$order->request."</th></tr>";
		if ($order->requestby == "") {
			$requestby = "no set date";
		} else {
			$requestby = $order->requestby;
		}
		$message .= "<tr><th width=40%>Request By</th><td>".$requestby."</td>";

		switch ($order->buildingplans) {
			case "usefiled":
				$plans = "Use plans already on file";
				break;
			case "electronic":
				$plans = "We will send through electronic copies to ".COMPANY;
				break;
			case "sendthrough":
				$plans = "We will fax or email plans to ".COMPANY;
				break;
			case "noplans":
				$plans = "We request ".COMPANY." to obtain plans at our expense";
				break;
			default:
				$plans = "We request ".COMPANY." to obtain plans at our expense";
				break;
			
		}
		
		$message .= "<tr><th width=40%>Building Plans</th><td>".$plans."</td>";
		if ($order->meetonsite == "0") {
			$meetonsite = "No meeting required";
		} else {
			$meetonsite = $order->meetonsite;
		}	
		$message .= "<tr><th width=40%>Onsite Contact</th><td>".$meetonsite."</td>";

		foreach($answers as $answer) {
			$message .= "<tr><th width=40%>".$questions[$answer->id]."</th><td>".$answer->answer."</td>";
		}

		$message .= "</table>";

		echo $message;
		echo br(2);
?>

<script>
	$(function() {
		$('tr:even').addClass('alt');
	
		$(".delreport").click(function() {
			var buttonid = $(this).attr('id');
			var orderid = buttonid.substr(9, 5);
			src = "/report/cancel2/"+orderid;
			$.ajax ({
				type:		'GET',
				url:		src,
				success:	function(data) {
					$("#pop-up .access-form div").html(data);
				}
			});
	
	
			$("#pop-up").show();
			return false;

			
		});
	
		$(".editreport").click(function() {
			window.location="/report/selectionmade";
		});
	});
</script>