<script>
	$(function() {
		$('tr:even').addClass('alt');

		$('#help').click(function() {
			var src = "/clients/help/page/4";
			$.ajax ({
				type:		'GET',
				url:		src,
				success:	function(data) {
					$("#pop-up .access-form div").html(data);
				}
			});


			$("#pop-up").show();
			return false;
		});
		$('.close-the-window').click(function() {
			$("#pop-up").hide();
		});
	
	});
</script>
<div class='col66'>
	<?=heading("Building Plans for ".$building->building_name, 1)?>
</div>
<div class='col33 rightalign'>
	<?php
		$helpbtn = array(
			'name'	=> 'help',
			'id'	=> 'help',
			'src'	=> 'library/images/admin/help.png'
		);
		$anchor = array(
			'title' => ' Help ',
			'id'	=> 'help'
		);
		echo anchor('#', img( $helpbtn), $anchor);
	?>
</div>
<div class='clearfix'></div>

<?php
	$client = $this->session->userdata('clientid');
	echo validation_errors();	
	echo form_open_multipart('report/details/');
	echo div_open('','left');
		echo form_fieldset('Building Plans');
			echo p("<span class='required'>*</span> One answer required");
			$optiongrouplabelattr = array(
			    'class' => 'optiongrplabelwide'
			);
			$meet1 = array(
			    'name'        => 'plans',
			    'id'          => 'electronic',
		    	'value'       => 'electronic',
		    	'checked'     => FALSE
			);
			$meet2 = array(
			    'name'        => 'plans',
			    'id'          => 'sendthrough',
		    	'value'       => 'sendthrough',
		    	'checked'     => FALSE
			);
			$meet3 = array(
			    'name'        => 'plans',
			    'id'          => 'noplans',
		    	'value'       => 'noplans',
		    	'checked'     => FALSE
			);
			$meet4 = array(
			    'name'        => 'plans',
			    'id'          => 'usefiled',
		    	'value'       => 'usefiled',
		    	'checked'     => TRUE
			);
			echo div_open('','optiongrpwide');
				echo form_radio($meet4);
				echo form_label('Please use the building plans that Solutions in Engineering currently has on file.', 'usefiled',$optiongrouplabelattr);
			echo div_x();
			echo div_open('','optiongrpwide');
				echo form_radio($meet1);
				echo form_label('I have the building plans/documents in electronic format to upload.', 'electronic',$optiongrouplabelattr);
			echo div_x();
			echo div_open('','optiongrpwide');
				echo form_radio($meet2);
				echo form_label('I have the building plans and will fax or email them through to Solutions in Engineering within 24 hours', 'sendthrough',$optiongrouplabelattr);
			echo div_x();
			echo div_open('','optiongrpwide');
				echo form_radio($meet3);
				echo form_label('I do not have the plans, and request Solutions in Engineering to obtain these at my expense.', 'noplans',$optiongrouplabelattr);
			echo div_x();
				
		echo form_fieldset_close();

	echo div_x();

	echo div_open('','right');
		echo div_open('planlist');
			$uploadlabel = array(
				'class'	=> 'uploadlabel'
			);
			$image_properties = array(
	    		'src' => '/library/images/admin/info.png',
          		'alt' => 'Information icon',
          		'class' => 'icon',
          		'width' => '20',
          		'height' => '20',
          		'title' => 'Information',
          	);

			echo p(img($image_properties)."PDF, JPEG, GIF, PNG or ZIP files only. Max file size 4 MB",'filesize','specialnote');
			echo br();
			echo div_open();
				$plans = array(
					'name'	=> 'uploadplans',
					'id'	=> 'uploadplans',
					'size'	=> '20',
					'value'	=> ''
				);
				echo form_label('File Path:', 'plans');//, $uploadlabel);
				echo form_upload($plans);
			echo div_x();
			
			echo div_open();
				$plantitle = array(
					'name'	=> 'plantitle',
					'id'	=> 'plantitle',
					'size'	=> '30'
				);
				echo form_label('Plan Name:', 'plantitle');
				echo form_input($plantitle);
			echo div_x();
		echo div_x();
		
	echo div_x();
	
	echo div_open('','clearfix');
	echo div_x();

	echo div_open('currentplans','clearfix');
		echo form_fieldset('Current Plans on file');
			$image_properties = array(
	    		'src' => '/library/images/admin/info.png',
          		'alt' => 'Information icon',
          		'class' => 'icon',
          		'width' => '20',
          		'height' => '20',
          		'title' => 'Information',
          	);
			echo p(img($image_properties)."<strong>Note: </strong>This list currently contains only plans that have been submitted through this system.",'','specialnote');
			echo br();
			echo p(img($image_properties)."<strong>Note: </strong>Future options will allow you to view and delete these reports as required.",'','specialnote');
			echo br();
			if (sizeof($buildingplans) > 0) {
				foreach ($buildingplans as $plan) {
					echo "<span>".$plan->plantitle."</span>";
					echo "<span>".date("d M, Y h:i:s", strtotime($plan->submitted))."</span>";
					echo "<span>".$plan->filesize." kB</span>";
					echo br();
				}
			} else {
				echo p("No plans currently stored");
			}
		echo form_fieldset_close();
	echo div_x();

		
	echo div_open('buttons');
		$submit = array(
			'name'	=> 'submit',
			'id'	=> 'submit',
			'class'	=> 'awesome large dkgreen',
			'type'	=> 'submit',
		    'content' => 'Save &amp; Continue',
		    'value'	=> 'upload'
		);
		$submit2 = array(
			'name'	=> 'submit2',
			'id'	=> 'submit2',
			'class'	=> 'awesome large dkgreen',
			'type'	=> 'submit',
		    'content' => 'Continue without uploading a file',
		    'value'	=> 'noupload'
		);

		$back = array(
			'name'	=> 'back',
			'id'	=> 'back',
			'class'	=> 'awesome large orange',
			'type'	=> 'submit',
		    'content' => 'Back One Screen',
			'value' => 'back'
		);
		$reset = array(
			'name'	=> 'reset',
			'id'	=> 'reset',
			'class'	=> 'awesome large red',
			'type'	=> 'reset',
		    'content' => 'Reset Screen'
		);

	
		echo form_button($reset);
		echo form_button($back);
		echo form_button($submit);
		echo form_button($submit2);

	echo div_x();
	echo div_open('','clearfix');
	echo div_x();
	
	
	echo form_close();
?>


