<?=br(2)?>
<p class='notice'>
	This building is not in our system. Do you wish to add this building?
</p>
<?php
	$takeover = array(
		'id'	=> 'takeover2',
		'name'	=> 'takeover2',
		'content'	=> 'Yes, this is a new building we are managing',
		'type'	=> 'button',
		'class'	=> 'awesome large dkgreen'
	);
	echo form_button($takeover).nbs(4);
	$leave = array(
		'id'	=> 'leave2',
		'name'	=> 'leave2',
		'content'	=> 'No, this is not our building',
		'type'	=> 'button',
		'class'	=> 'awesome large red'
	);
	echo form_button($leave);

?>
<script>
	$(function() {
		$("#leave2").click(function() {
			$("#results").html('');
			$("#plannumber").val('');
		});
	
		$("#takeover2").click(function() {
			var plan = $("#plannumber").val();
			var href='../building/newb/'+plan;
			window.location = href;
		});
	});
	
</script>