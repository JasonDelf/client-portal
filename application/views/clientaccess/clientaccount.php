	<?= link_tag('main')?>
	<?= link_tag('buttons')?>
	<?= script_tag('jquery.min')?>
	<?= script_tag('jquery.form')?>
<body class='popup'>
<?php
	echo heading("Account Details", 1);
	echo validation_errors();	
?>
<?php
	$formattr = array(
		'id'	=> 'accountform'
	);
	if (isset($message)) {
		echo div_open("message");
			echo p($message);
		echo div_x();
	}
	echo div_open('response');
		echo p("Details saved");
	echo div_x();
	
	$formattr = array(
		'id'	=> 'passwordform'
	);
	
	echo form_open('clients/password/5', $formattr);
		echo div_open('response2');
			echo p("Password has been changed");
		echo div_x();

		echo form_fieldset("Login Details");

			echo div_open();
				$fullname= array(
					'id'	=> 'fullname',
					'name'	=> 'fullname',
					'disabled'	=> 'disabled',
					'size'	=> '30',
					'value'	=> $account->fullname
				);
				echo form_label("Full name", 'fullname');
				echo form_input($fullname);
			echo div_x();

			echo div_open();
				$username= array(
					'id'	=> 'username',
					'name'	=> 'username',
					'disabled'	=> 'disabled',
					'size'	=> '30',
					'value'	=> $account->username
				);
				echo form_label("Username", 'username');
				echo form_input($username);
			echo div_x();

			echo div_open('changepwdbutton');
				$change = array(
					'id'	=> 'change',
					'name'	=> 'change',
					'title'	=> 'Change Password',
					'class'	=> 'awesome medium blue'
				);
				echo form_label("", '');
				echo anchor('#', "Change Password", $change);
			echo div_x();

			echo div_open('changepwd');
				echo div_open();
					$oldpwd= array(
						'id'	=> 'oldpassword',
						'name'	=> 'oldpassword',
						'size'	=> '20'
					);
					echo form_label("Current password", 'oldpassword');
					echo form_password($oldpwd);
				echo div_x();
				echo div_open();
					$newpwd= array(
						'id'	=> 'newpassword',
						'name'	=> 'newpassword',
						'size'	=> '20'
					);
					echo form_label("New password", 'newpassword');
					echo form_password($newpwd);
				echo div_x();
				echo div_open();
					$confirmpwd= array(
						'id'	=> 'confirmpassword',
						'name'	=> 'confirmpassword',
						'size'	=> '20'
					);
					echo form_label("Confirm password", 'confirmpassword');
					echo form_password($confirmpwd);
				echo div_x();
	
				echo div_open('');
					echo br();
					$change = array(
						'id'	=> 'savechange',
						'name'	=> 'savechange',
						'content'	=> 'Save New Password',
						'class'	=> 'awesome medium dkgreen',
						'type'	=> 'Submit'
					);
					echo form_label("", '');
					echo form_button($change); //'/account/password', "Change Password", $change);
				echo div_x();
			
			echo div_x();

		echo form_fieldset_close();
	
	echo form_close();

?>
</body>
<script>
	$(function() {
		$("#change").click(function() {
			$(this).hide();
			$("#changepwd").show('fast');
			return false;	
		});
	
	    var options = { 
			success:	showResponse
		};

	    $('#accountform').ajaxForm(options); 
	
		function showResponse(responseText, statusText, xhr, $form) {
			$("#response").html(responseText).show('slow').delay(2000).hide('slow').delay(500);//.parent().parent().tabs('select', nexttab);
		}


	    var options2 = { 
			success:	showResponse2
		};

	    $('#passwordform').ajaxForm(options2); 
	
		function showResponse2(responseText, statusText, xhr, $form) {
			$("#response2").html(responseText).show('slow').delay(2000).hide('slow').delay(500);//.parent().parent().tabs('select', nexttab);
			$("#changepwd").hide();
			$("#change").show();
		}

	});
</script>
