		echo div_open();
			$optiongrouplabelattr = array(
			    'class' => 'optiongrplabel'
			);
			echo p("Person to meet onsite:<span class='required'>*</span>");
			$meet1 = array(
			    'name'        => 'meetonsite',
			    'id'          => 'bmmeetonsite',
		    	'value'       => 'meetmanager',
		    	'checked'     => FALSE
			);
			$meet2 = array(
			    'name'        => 'meetonsite',
			    'id'          => 'osrmeetonsite',
		    	'value'       => 'meetosr',
		    	'checked'     => FALSE
			);
			$meet3 = array(
			    'name'        => 'meetonsite',
			    'id'          => 'nomeetonsite',
		    	'value'       => 'nomeeting',
		    	'checked'     => TRUE
			);
			echo div_open('','optiongrp');
				echo form_label('No Meeting is Required:', 'nomeetonsite',$optiongrouplabelattr);
				echo form_radio($meet3);
			echo div_x();
			echo div_open('','optiongrp');
				echo form_label('Building Manager:', 'bmmeetonsite',$optiongrouplabelattr);
				echo form_radio($meet1);
			echo div_x();
			echo div_open('','optiongrp');
				echo form_label('Onsite Representative:', 'osrmeetonsite',$optiongrouplabelattr);
				echo form_radio($meet2);
			echo div_x();

		echo div_x();





				$data2['meetonsite'] = $this->input->post('meetonsite');





			$this->form_validation->set_rules('meetonsite', 'Person to Meet Onsite', 'required|xss_clean');
