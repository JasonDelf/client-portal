<div class='col66'><?=heading($buildingname,1)?></div>
<div class='col33 rightalign'>
	<?php
		$helpbtn = array(
			'name'	=> 'help',
			'id'	=> 'help',
			'src'	=> 'library/images/admin/help.png'
		);
		$anchor = array(
			'title' => ' Help ',
			'id'	=> 'help'
		);
		echo anchor('#', img( $helpbtn), $anchor);
	?>
</div>
<div class='clearfix'></div>
<div id="tabs">
	<ul>
	<?php
		foreach ($reports as $report) {
			echo "<li><a href='/report/information/".$report->id."/".$orders[$report->id]."'>".$report->report_name."</a></li>";
		}
	?>	
	</ul>
</div>
<div id="buttons">
	<?php
		$add = array(
			'name'	=> 'add',
			'id'	=> 'add',
			'class'	=> 'awesome large blue',
			'type'	=> 'button',
		    'content' => 'Add Another Report'
		);

		$submit = array(
			'name'	=> 'continue',
			'id'	=> 'continue',
			'class'	=> 'awesome large dkgreen',
			'type'	=> 'button',
		    'content' => 'Continue'
		);
		echo form_button($add);
		echo form_button($submit);

	?>
</div>
<div class='clearfix'>&nbsp;</div>

<script>
	$(function() {
		var $tabs = $( "#tabs" ).tabs({
			ajaxOptions: {
				error: function( xhr, status, index, anchor ) {
					$( anchor.hash ).html(
						"Couldn't load this tab. We'll try to fix this as soon as possible. " +
						"If this wouldn't be a demo." );
				}
			}
		});
		
		$('#help').click(function() {
			var src = "/help/page/5";
			$.ajax ({
				type:		'GET',
				url:		src,
				success:	function(data) {
					$("#pop-up .access-form div").html(data);
				}
			});
	
	
			$("#pop-up").show();
			return false;
		});
		$('.close-the-window').click(function() {
			$("#pop-up").hide();
		});

		$("#continue").click(function() {
			window.location="/report/delivery";
		});
		
		$("#add").click(function() {
			window.location = "/report/selection";
		});
		
	});
</script>
