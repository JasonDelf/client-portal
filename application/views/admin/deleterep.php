<script>
	$(function() {
		$("#reset").click(function(){
			var href = "/admin/report/detail/" + <?=$report->id?>;
			location.href = href;
		});
	});
</script>
<?php
	echo form_open('admin/report/delete/'.$report->id);
		echo form_hidden('confirm', '1');
		echo p("Are you sure you want to delete this report?").br();
		echo p($report->report_name,'','indented');
			
		echo "<div>";
			echo br(2);
			$reset = array(
				'name'	=> 'reset',
				'id'	=> 'reset',
				'type'	=> 'reset',
				'content'	=> 'No, keep it',
				'class'	=> 'awesome medium orange'
			);
			$submit = array(
				'name'	=> 'submit',
				'id'	=> 'submit',
				'type'	=> 'submit',
				'content'	=> 'Yes, delete it',
				'class'	=> 'awesome medium dkgreen'
			);
			echo form_button($reset);
			echo form_button($submit);
		echo "</div>";
	
	echo form_close();
?>
<div class='clearfix'></div>