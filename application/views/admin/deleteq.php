<script>
	$(function() {
		$("#reset").click(function(){
			var href = "/admin/report/questionedit/" + <?=$question->id?>;
			location.href = href;
		});
	});
</script>
<?php
	echo form_open('admin/report/questiondelete/'.$question->id);
		echo form_hidden('confirm', '1');
		echo p("Are you sure you want to delete this question?").br();
		echo p($question->question,'','indented');
			
		echo "<div>";
			echo br(2);
			$reset = array(
				'name'	=> 'reset',
				'id'	=> 'reset',
				'type'	=> 'reset',
				'content'	=> 'No, keep it',
				'class'	=> 'awesome medium orange'
			);
			$submit = array(
				'name'	=> 'submit',
				'id'	=> 'submit',
				'type'	=> 'submit',
				'content'	=> 'Yes, delete it',
				'class'	=> 'awesome medium dkgreen'
			);
			echo form_button($reset);
			echo form_button($submit);
		echo "</div>";
	
	echo form_close();
?>
<div class='clearfix'></div>