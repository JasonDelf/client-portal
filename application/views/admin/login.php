<?= doctype('html5')?>
<head>
	<title>Client Services :: by Solutions IE :: ADMIN ENTRY</title>
	<?= link_tag('reset')?>
	<?= link_tag('login')?>
	<?= link_tag('buttons')?>
	<?= script_tag('jquery')?>
</head>
<body>
	<?php
		echo div_open('login', 'login admin');
		echo form_open('admin/login');
		echo "<div>";
		echo p("ADMIN LOGIN");
		echo form_label('Username','username');
		$username = array(
			'id'	=> 'username',
			'name'	=> 'username',
			'size'	=> '20'
		);
		echo form_input($username);
		echo "</div><div>";
		echo form_label('Password','password');
		$password = array(
			'id'	=> 'password',
			'name'	=> 'password',
			'size'	=> '20'
		);
		echo form_password($password);
		echo "</div><div>";
		echo form_label('&nbsp;');
		$login = array(
			'id'		=> 'btnLogin',
			'name'		=> 'btnLogin',
			'type'		=> 'submit',
			'class'		=> 'awesome dkgreen large',
			'content'	=> 'Log in'
		);
		echo form_button($login);
		echo "</div>";
		echo form_close();
	
		echo div_x();
	?>
</body>
</html>