<script language="javascript">
	function drop_user()
	{
		
		if(confirm('Do you want to drop this user?'))
		{
			clientid = document.getElementById('users').value;
			if(clientid==''||clientid=='0'){
				
				alert("Please select a user to delete");	
				return;
					
			}
			document.frmUser.clientid.value = document.getElementById('users').value;
		    document.frmUser.submit();
		}
	}
</script>
<?php


	$homedetails = array(
		'title'	=> 'Home',
		'class'	=> 'awesome large blue'
	);

	echo anchor("/admin", "Dashboard", $homedetails).nbs(4);

	echo br(2);
	echo heading("Current users",1);

	echo "<div class='required'>$message</div>";
	echo br(2);

	echo form_open('admin/users',array('name' => 'frmUser', 'id' => 'frmUser'));
		

	$array_user[0] = "";
	foreach ($users as $user) {
		$array_user[$user->id] = $user->username;
	}

					
		echo div_open();
			
			echo form_label('Users :', 'users');
			echo form_dropdown('users', $array_user, '', 'id=users');
			
			$submit = array(
				'id'	=> 'user_button',
				'name'	=> 'user_button',
				'content'	=> 'Drop',
				'onclick'	=> ' drop_user()',
				'type'	=> 'button',
				'class'	=> 'awesome medium dkgreen'
			);	
		echo "&nbsp;";	
		echo form_button($submit);
	echo div_x();
	
			echo form_hidden('clientid','');
			echo form_hidden('user_action','del');	
	echo form_close();
 	echo br(5);
?>
<div class='clearfix'></div>