<div id='main' class='grid_10'>
	<h1>Newsletter Archives</h1>
	<div class='archivecolumn'>
		<h3>NSW</h3>
		<ul class='newsletterlist'>
			<li><a href="../../library/docs/pdf/NSWApril2011.pdf">April 2011</a> <img src="/library/images/pdfsmall.png" alt="adobe reader icon" /> (434 kB)</li>
			<li><a href="/library/docs/pdf/NSW-September09.pdf">September 2009</a> <img src="/library/images/pdfsmall.png" /> (115 kB)</li>
			<li><a href="/library/docs/pdf/NSW-December08.pdf">December 2008</a> <img src="/library/images/pdfsmall.png" /> (418 kB)</li>
			<li><a href="/library/docs/pdf/NSW-October08.pdf">October 2008</a> <img src="/library/images/pdfsmall.png" /> (61 kB)</li>
			<li><a href="/library/docs/pdf/NSW-September08.pdf">September 2008</a> <img src="/library/images/pdfsmall.png" /> (123 kB)</li>
			<li><a href="/library/docs/pdf/NSW-July08.pdf">July 2008</a> <img src="/library/images/pdfsmall.png" /> (106 kB)</li>
			<li><a href="/library/docs/pdf/NSW-June08.pdf">June 2008</a> <img src="/library/images/pdfsmall.png" /> (274 kB)</li>
			<li><a href="/library/docs/pdf/NSW-April08.pdf">April 2008</a> <img src="/library/images/pdfsmall.png" /> (164 kB)</li>
		</ul>
  </div>

	<div class='archivecolumn'>
		<h3>QLD</h3>
		<ul class='newsletterlist'>
			<li><a href="../../library/docs/pdf/QLDApril2011.pdf">April 2011</a> <img src="/library/images/pdfsmall.png" alt="adobe reader icon" /> (435 kB)</li>
			<li><a href="/library/docs/pdf/QLD-September09.pdf">September 2009</a> <img src="/library/images/pdfsmall.png" /> (238 kB)</li>
			<li><a href="/library/docs/pdf/QLD-October08.pdf">October 2008</a> <img src="/library/images/pdfsmall.png" /> (393 kB)</li>
			<li><a href="/library/docs/pdf/QLD-September08.pdf">September 2008</a> <img src="/library/images/pdfsmall.png" /> (106 kB)</li>
			<li><a href="/library/docs/pdf/QLD-August08.pdf">August 2008</a> <img src="/library/images/pdfsmall.png" /> (74 kB)</li>
			<li><a href="/library/docs/pdf/QLD-June08.pdf">June 2008</a> <img src="/library/images/pdfsmall.png" /> (53 kB)</li>
			<li><a href="/library/docs/pdf/QLD-April08.pdf">April 2008</a> <img src="/library/images/pdfsmall.png" /> (111 kB)</li>
		</ul>
  </div>

	<div class='archivecolumn'>
		<h3>VIC</h3>
		<ul class='newsletterlist'>
			<li><a href="../../library/docs/pdf/VICApril2011.pdf">April 2011</a> <img src="/library/images/pdfsmall.png" alt="adobe reader icon" /> (434 kB)</li>
			<li><a href="/library/docs/pdf/VIC-November08.pdf">November 2008</a> <img src="/library/images/pdfsmall.png" /> (422 kB)</li>
			<li><a href="/library/docs/pdf/VIC-September08.pdf">September 2008</a> <img src="/library/images/pdfsmall.png" /> (66 kB)</li>
			<li><a href="/library/docs/pdf/VIC-August08.pdf">August 2008</a> <img src="/library/images/pdfsmall.png" /> (152 kB)</li>
			<li><a href="/library/docs/pdf/VIC-July08.pdf">July 2008</a> <img src="/library/images/pdfsmall.png" /> (90 kB)</li>
			<li><a href="/library/docs/pdf/VIC-June08.pdf">June 2008</a> <img src="/library/images/pdfsmall.png" /> (61 kB)</li>
		</ul>
  </div>

</div>
