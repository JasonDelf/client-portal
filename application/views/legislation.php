<script>
	$(function() {
		$(".links li:odd").addClass('alt');

	});
</script>
<div id='main' class='grid_7'>
	<h1>Legislation Links</h1>

	<p>We have collected a range of useful links to legal, regulatory and industry sources. Please feel free to browse these at your leisure. </p>
	<br />
	<ul class='links'>
		<li>The Department of Infrastructure and Planning<br /><a href="http://www.dip.qld.gov.au/sustainable-housing/sustainability-declaration.html" target="_blank">www.dip.qld.gov.au/sustainable-housing/sustainability-declaration.html</a><br />Sustainablity Declaration Form</li>
		<li>National Community Titles Institute (NCTI)<br /><a href="http://www.ncti.org.au" target="_blank">www.ncti.org.au</a><br />E-Mail: <a href="mailto:info@mccallspr.com">info@mccallspr.com</a></li>
		<li>Community Titles Institute of Queensland (CTIQ)<br /><a href="http://www.ctiq.org.au" target="_blank">www.ctiq.org.au</a><br />E-Mail: <a href="mailto:ceo@ctiq.org.au">ceo@ctiq.org.au</a></li>
		<li>Institute of Strata Title Management - NSW (ISTM)<br /><a href="http://www.istm.org.au" target="_blank">www.istm.org.au</a><br />E-Mail: <a href="mailto:inquiries@istm.org.au">inquiries@istm.org.au</a></li>
		<li>Institute of Body Corporate Managers - Vic (IBCM)<br /><a href="http://www.bodycorp.org" target="_blank">www.bodycorp.org</a><br />E-Mail: <a href="mailto:info@bodycorp.org">info@bodycorp.org</a></li>
		<li>Strata Title Institute of Western Australia (STIWA)<br /><a href="http://www.stiwa.com.au" target="_blank">www.stiwa.com.au</a><br />E-Mail: <a href="mailto:stiwa@bigpond.net.au">stiwa@bigpond.net.au</a></li>
		<li>Workplace Health & Safety Act, Queensland<br/><a href="http://www.legislation.qld.gov.au/LEGISLTN/ACTS/2011/11AC018.pdf" target="_blank">www.legislation.qld.gov.au/legisltn/current/w/WorkplHSaA95.pdf</a></li>
		<li>Qld. Fire & Rescue Service - Building Fire Safety<br /><a href="http://www.fire.qld.gov.au/buildingsafety/" target="_blank">www.fire.qld.gov.au/buildingsafety/</a></li>
		<li>Australian Building Codes Board<br /><a href="http://www.abcb.gov.au/" target="_blank">www.abcb.gov.au/</a></li>
		<li>Home Warranty Insurance - National Info Table<br /><a href="http://www.consumer.gov.au/html/home_builder/html/4Appendix.html" target="_blank">www.consumer.gov.au/html/home_builder/html/4Appendix.html</a></li>
		<li>NSW Fire Brigades - Business Guidelines and Regulations<br /><a href="http://www.nswfb.nsw.gov.au/business/guidelines_essential_services.htm" target="_blank">www.nswfb.nsw.gov.au/business/guidelines_essential_services.htm</a></li>
		<li>NSW Legislation<br /><a href="http://www.legislation.nsw.gov.au/maintop/search/sessional" target="_blank">www.legislation.nsw.gov.au/maintop/search/sessional</a></li>
		<li>NSW Office of Fair Trading<br /><a href="http://www.fairtrading.nsw.gov.au/" target="_blank">www.fairtrading.nsw.gov.au/</a></li>
		<li>Victorian Legislation & Parliamentary Documents<br /><a href="http://www.dms.dpc.vic.gov.au/Domino/Web_Notes/LDMS/PubLawToday.nsfOpenDatabase" target="_blank">www.dms.dpc.vic.gov.au/Domino/Web_Notes/LDMS/PubLawToday.nsfOpenDatabase</a></li>
	</ul>
</div>
