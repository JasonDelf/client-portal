<script>
	$(function() {
		$(".links li:odd").addClass('alt');

	});
</script>
<div id='main' class='grid_7'>
	<h1>Other Documents</h1>

	<p>We have made available some other useful documents for your information. </p>
	<br />
	<ul class='doclinks'>
		<li><a href="/library/docs/pdf/ContractorsAgreement.pdf">Contractor's Safe Working Agreement</a> (90 kB)</li>
		<li><a href="/library/docs/pdf/ImplementationPlan.pdf">Implementation Plan</a> (61 kB)</li>
		<li><a href="/library/docs/pdf/SafetyUpdates.pdf">Safety Report Update Schedule</a> (53 kB)</li>
		<li><a href="/library/docs/pdf/WorkingAtHeights.pdf">Standard Procedures for Working at Heights</a> (53 kB)</li>
		<li><a href="/library/docs/pdf/JSA.pdf">Trade Specific Job Safety Analysis (JSA)</a> (172 kB)</li>
	</ul>
</div>
