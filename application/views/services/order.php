<div id='main' class='grid_7'>
	<h1>Quote/Order</h1>

	<p>Please note: our <strong>Client Portal</strong> is now open.  You will be receiving your login details to access our online ordering system shortly.</p>
	<p>If you have been provided with your <strong>Client Portal</strong> access details, you can login using the box on the left hand side of the page.  From there, you can request a quote or place an order for any of our reports.</p>
	<p>Until that time, you can still access our electronic/printable order forms below.</p>
	<p>&nbsp;</p>
	<div class='archivecolumn'>
		<h3>NSW</h3>
		<ul class="orderlist">
			<li><a href="/library/docs/forms/NSWQuote2011.pdf" target="_blank">Quote Request</a> <img src="/library/images/pdfsmall.png" /> (119 kB)</li>
			<li><a href="/library/docs/forms/NSWOrder2011.pdf" target="_blank">Order Form</a> <img src="/library/images/pdfsmall.png" /> (61 kB)</li>
		</ul>
	</div>
	<div class='archivecolumn'>
		<h3>QLD</h3>
		<ul class="orderlist">
			<li><a href="/library/docs/forms/QLDQuote2011.pdf" target="_blank">General - Quote Request</a> <img src="/library/images/pdfsmall.png" /> (94 kB)</li>
			<li><a href="/library/docs/forms/QLDOrder2011.pdf" target="_blank">General - Order Form</a> <img src="/library/images/pdfsmall.png" /> (106 kB)</li>
		</ul>
		<p>&nbsp;</p>
		<ul class="orderlist">
			<li><a href="/library/docs/forms/QLDFIREQuote2011.pdf" target="_blank">Fire Services - Quote Request</a> <img src="/library/images/pdfsmall.png" /> (123 kB)</li>
			<li><a href="/library/docs/forms/QLDFIREOrder2011.pdf" target="_blank">Fire Services - Order Form</a> <img src="/library/images/pdfsmall.png" /> (123 kB)</li>
		</ul>
		<p>&nbsp;</p>
		<ul class="orderlist">
			<li><a href="/library/docs/forms/QLDPoolResidential2011.pdf" target="_blank">Pool Safety - Residential - Quote Request</a> <img src="/library/images/pdfsmall.png" /> (139 kB)</li>
			<li><a href="/library/docs/forms/QLDPoolBodyCorp2011.pdf" target="_blank">Pool Safety - Bodies Corporate - Quote Request</a> <img src="/library/images/pdfsmall.png" /> (147 kB)</li>
		</ul>
	</div>
	<div class='archivecolumn'>
		<h3>VIC</h3>
		<ul class="orderlist">
			<li><a href="/library/docs/forms/VICQuote2011.pdf" target="_blank">Quote Request</a> <img src="/library/images/pdfsmall.png" /> (102 kB)</li>
			<li><a href="/library/docs/forms/VICOrder2011.pdf" target="_blank">Order Form</a> <img src="/library/images/pdfsmall.png" /> (90 kB)</li>
		</ul>
	</div>
	<div class='archivecolumn'>
		<h3>ACT</h3>
		<ul class="orderlist">
			<li><a href="/library/docs/forms/ACTQuote2011.pdf" target="_blank">Quote Request</a> <img src="/library/images/pdfsmall.png" /> (119 kB)</li>
			<!-- 
			<li><a href="/library/docs/forms/ACT-EOF.pdf" target="_blank">Order Form</a> <img src="/library/images/pdfsmall.png" /> (94 kB)</li>
			-->
		</ul>
	
	</div>

</div>
