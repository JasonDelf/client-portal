<div id='main' class='grid_7'>
	<h1>Queensland Pool and Spa Compliance</h1>

	<p>Who else would you use with a FREE government pool registration service, competitive pricing, the experience of 10,000 pool safety inspections already and most importantly accurate and timely advice?</p>
	<p>As a professional Real Estate Property Manager/Owner you want to ensure your pool certification process is as easy as possible to organise and implement. With $16,500 fines for each instance of non-compliance, a 90 day compliance period and 600,000 pools to certify it must be on time every time.</p>
	<p>A property with a pool cannot have a new lease until the pool is certified. When a residential property with a pool is sold there is a 90 day period in which to get the pool compliant. Here are 4 key benefits for using <?=COMPANY?> for your compliance regime:-</p>
	<ul class='listing'>
		<li>FREE registration of all your pools on the government website. Must be done properly by 4th May 2011 at the latest or a $2,000 fine per pool applies;</li>
    	<li>Accurate advice - shared pools have 2 years to comply and yet almost all other suppliers are telling you that you must certify them now;</li>
    	<li>Competitive prices from the most highly regarded Real Estate (commercial and residential) and strata compliance report provider. After 25 years and 85,000 compliance reports you can rely on us to back up what we do. There are many fly by nighters showing up in pool compliance;</li>
    	<li>100% money back satisfaction guarantee for your report.</li>
	</ul>
	<h3>Think about cramming this into 90 days:</h3>
	<p>Property lease to be signed/sold <img src="/library/images/blue_right.png" /> order pool certification <img src="/library/images/blue_right.png" /> pool inspected <img src="/library/images/blue_right.png" /> report received <img src="/library/images/blue_right.png" /> get quotes for any work needed <img src="/library/images/blue_right.png" /> get owner&rsquo;s permission to go ahead <img src="/library/images/blue_right.png" /> engage contractor <img src="/library/images/blue_right.png" /> organise re-inspection <img src="/library/images/blue_right.png" /> if compliant then receive a certificate.</p>
	<h3>What are smart Real Estate Property Managers/Owners doing?</h3>
	<p>Ordering all your pool certifications now taking the hassle and pressure associated with getting the certification process completed. Remember a lease cannot be signed until the pool certificate is received. By ordering now you will get a great result including a great price, and delivery in an efficient manner to agreed timelines.</p>
	<h4>Fact Sheets</h4>
	<p><a href="/library/docs/pdf/QLDPool2011.pdf">Qld Pool and Spa Certification</a> <img src="/library/images/pdfsmall.png"> (188 kB)</p>
	<p><a href="/library/docs/pdf/QLDPoolLaws2011.pdf">About the new Qld Pool Laws</a> <img src="/library/images/pdfsmall.png"> (111 kB)</p>

	
</div>
