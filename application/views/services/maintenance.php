<div id='main' class='grid_7'>
	<h1>Maintenance</h1>

	<p>Our maintenance inspections include the checking of many building elements including:</p>
	<ul class='listing'>
		<li>Condition of driveways, car parks and pathways. We check for things like cracking of concrete, movement at control joints and movement of pavers.</li>
    	<li>Assessment of fence conditions including post integrity and signs of rot.</li>
    	<li>Checking of the condition of all stairs, stairwells and associated hand railing and balustrade.</li>
    	<li>Exterior of building inspection - all visible elements</li>
    	<li>Specifically checking condition of guttering & downpipes.</li>
    	<li>Checking all paintwork - visually inspect and test porosity.</li>
    	<li>Internal floor surfaces inspection - check carpets especially leading edge of stair treads. Check tiles for cracks and check for control joints (should be at Building Code of Australia required intervals) and for movement at control joints.</li>
    	<li>Visually inspecting installed items at plant such as hot water systems, airconditioning units, cooling towers, ventilation ducting etc.</li>
    	<li>Checking door furniture including hinges, locks and automatic door closers.</li>
    	<li>Checking any roller body corporate roller doors, motors and associated fittings.</li>
	</ul>
	<br />
	<p><strong>Swimming Pools:</strong></p>
	<ul class='listing'>
		<li>Checking pump motor, chlorinator, filter, pool coping and surrounds, pool lining, sign fixings, fence and any associated plumbing.</li>
    	<li>Checking of all body corporate assets including outdoor furniture, BBQs, umbrellas, etc</li>
	</ul>
	<br />
	<h3><?=COMPANY?> personalised service and how you&rsquo;ll benefit:</h3>
	<ul class='listing'>
		<li>Courteous, easily identifiable and professionally-dressed Inspectors who are fully trained building professionals with decades of combined experience in report generation and maintenance/new project management</li>
    	<li>Reports that are compiled using tailor made, Australian-designed software that we designed for sinking fund forecasts to calculate the lowest possible levy whilst still maintaining sufficient funds to meet expenditure needs</li>
    	<li>Simple, comprehensive easy to read reports which include tips for extending the life of many high maintenance items</li>
    	<li>Protection of the service provided for you with a $2 000 000 of Professional Indemnity Insurance</li>
    	<li>Rates and information used in calculations and reports are obtained from real contract rates and relative industry sources not just estimating schedules This is to ensure they are current and accurate</li>
	</ul>	
	<!-- 
	<h4>Fact Sheets</h4>
	<p><a href="/library/docs/pdf/QLDMaintenance2008.pdf">Qld Building Maintenance</a> <img src="/library/images/pdfsmall.png"> (33 kB)</p>
	-->
</div>