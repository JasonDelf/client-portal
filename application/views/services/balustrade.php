<div id='main' class='grid_7'>
	<h1>Balustrades</h1>

	<p>How do you know if you need your balustrade checked?</p>
	<ol>
		<li>If there are any visible signs of rust on the balustrade and/or fixings (E.g. rusted bolts or rivets) and/or</li>
   		<li>If the balustrades are over 10 years old - there was little or no certification of balustrades until recently so some may constitute a risk.</li>
   		<li>If you have no balustrade maintenance regime in place - after recent accidents councils are insisting testing and maintenance regimes be put in place</li>
	</ol>
	<br />
	<p>Many bodies corporate & owners corporations have achieved peace of mind before events where balconies and balustrades get a lot of use. Examples of where our services have been used:-</p>
	<ol>
		<li>Before parties</li>
   		<li>Before festivals and events - like Sydney Harbour New Year's Eve Fireworks, Brisbane's Skyfire, the Gold Coast Indy or Sydney's Mardi Gras</li>
   		<li>Where balconies have views of fireworks and are used a viewing platforms - like Sydney's New Year's fireworks display</li>
	</ol>
	<br />
	<p>Why a <?=COMPANY?> balustrade test is the only guaranteed test?</p>
	<ol>
		<li>This is not a superficial visual inspection, this service includes a proper structural performance test using our exclusive patented machine and system</li>
   		<li>Your test includes load testing of the structural performance of the posts and rails by our own patented machine. Pre 2002, balustrades can be tested by applying a load of 0.9 kilo Newton (kN). Loads are applied as per Clause 4.7.1 (a), Australian Standard AS1170 Part 1 - SAA Loading Code.</li>
   		<li>We will test railings by applying the prescribed pressures in an outward and downward direction.</li>
   		<li>Also a proper assessment of the geometry of the balustrade in terms of the Building Code of Australia including height and railing width requirements.</li>
	</ol>
	<br />
	<p>Your report also includes: -</p>
	<ul class='listing'>
    	<li>Digital photos identifying any balustrade issues</li>
    	<li>General recommendations on how the railings can be effectively repaired</li>
	</ul>
	<h4>Fact Sheets</h4>
	<p><a href="/library/docs/pdf/QLDBalustrade2011.pdf">Qld Balustrade Testing</a> <img src="/library/images/pdfsmall.png"> (299 kB)</p>
	<!-- 
	<p><a href="/library/docs/pdf/NSWBalsutrade2008.pdf">NSW Balustrade Testing</a> <img src="/library/images/pdfsmall.png"> (33 kB)</p>
	<p><a href="/library/docs/pdf/VICBalustrade2008.pdf">Vic Balustrade Testing</a> <img src="/library/images/pdfsmall.png"> (37 kB)</p>
	-->
</div>
