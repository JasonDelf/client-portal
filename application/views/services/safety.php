<div id='main' class='grid_7'>
	<h1>Safety Report</h1>

	<p>Our Compliance Reports (Safety Reports) can  be a great tool for strata schemes and owners to ensure that the common  property is a safe environment for all workers, tenants, owners and visiting  members of the public. </p>
	<p><br />
      <strong>What  obligations does the Strata Scheme have?</strong><br />
      Under the <em>Work Health and Safety Act </em>if your strata scheme is a &lsquo;Person  Conducting a Business or Undertaking&rsquo; (PCBU) you will have obligations to ensure  health and safety for the common property. While there has been an attempt to exclude strata  schemes from the <em>Work Health and Safety </em>obligations,  the definition is very limited. &nbsp;</p>
      A strata scheme will be a PCBU, and  therefore not exempt, if: </p>
	<ul class='listing'>
	 <li>It employs anyone</li>
	 <li>Any of the lots are used for  non-residential purposes, this includes</li>
	  <ul class='listing'>
	&nbsp;<li>Short term letting</li>
	&nbsp;<li>Commercial use in any of the  lots, such as a home business</li>
	&nbsp;<li>Leases over common property</li>
	&nbsp;<li>The common property is used for  commercial purposes, such as boot camps, aqua aerobics</li>
	 &nbsp;<li>Owners are allowed to perform  work or maintenance, such as gardening, on the common property</li>
      </ul>
  </ul>
  <p>&nbsp;</p>
  <p>As it is almost impossible to ensure  occupiers do not conduct business from their lots (which may include working  from home in any capacity), the reality is that this exemption will have little  application.</p>
  <p><br />
        <strong>What  are the fines for non-compliance?</strong><br />
      The penalties for noncompliance are severe,  in the case of a serious accident resulting in death, the fines can be:<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>$3 million for Strata Scheme<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $600,000 for individuals  such as committee members and owners<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; and/or 5 years jail</strong></p>
	<p><br />
      <strong>What  do you need to do to comply?</strong><br />
  If your strata scheme is a PCBU then a safety  report from a reputable company, such as <em>Solutions  in Engineering</em>, can help meet your obligations.
  <em>Solutions  in Engineering</em> offers strata schemes and businesses  a &lsquo;Compliance Report&rsquo;, which includes:</p>
	<ul class='listing'>
	  <li>Inspection of the common property  and provide a report that clearly identifies hazards;</li>
	  <li>Assessment of the risks that  may result from those hazards;</li>
	  <li>Recommendations to manage the  risk</li>
	  <li>Contractors Safe Working  Agreement; which is required under the Work Health and Safety </li>
	  <li>Compliance Report Update  Schedule</li>
	  <li>Implementation Plan</li>
	  <li>Property Profile (Site  information for workers)</li>
	  <li>Job Safety Analysis Worksheets</li>
  </ul>
	<p>&nbsp;</p>
	<p>For more information, please see our Fact  Sheets. </p>
  <h4>Fact Sheets</h4>
	<p><a href="/library/docs/pdf/WHS_Committee_Flyer _V11.12.pdf">What every owner ought to know about WHS</a> <img src="/library/images/pdfsmall.png"> (426 kB)	</p>
  <p><a href="/library/docs/pdf/QLD-Sect7-StatDec.docx">QLD Committee Statutory Declaration</a><img src="/library/images/pdfsmall.png" alt="" /></p>
	<p><a href="/library/docs/pdf/NSW-Sect7-StatDec.docx">NSW Committee Statutory Declaration</a><img src="/library/images/pdfsmall.png" alt="" /></p>
</div>