<div id='main' class='grid_7'>
	<h1>Part 5</h1>

	<p>Part 5 can be a very effective way of managing your Body Corporate in Queensland. Many owners and committee members are choosing to delegate the management of their building to their Body Corporate Manager so they can spend their spare time relaxing instead of dealing with body corporate head aches.</p>
	<p>We work with the Body Corporate Manager under Part 5. The Body Corporate Manager provides the expert administration and accounting advice while <?=COMPANY?> provides the structural maintenance and equipment expertise to ensure that the Part 5 maintenance and asset management aspects are met. It creates peace of mind for owners to know that their building asset is being inspected by experts in the maintenance field.</p>
	<h3>How we give you cost effective Part 5 Maintenance Reports</h3>	
	<p>The Act asks for a list of proposed maintenance items to be done for the quarter with no mention of a physical quarterly inspection. It is the legal opinion of a leading Body Corporate Solicitor that an inspection can be done annually and still meet the Part 5 requirements and intention of the Act.</p>
	<p>This is good news for Bodies Corporate because one report as opposed to four will reduce the costs of Part 5 considerably. A special report format has been developed by <?=COMPANY?> that includes:-</p>
	<ol>
		<li>All maintenance items into the four quarter of the year</li>
   		<li>Report facilitates easy creation of projected maintenance reports for each quarterly owner&rsquo;s report</li>
   		<li>Any major structural issues detected in our inspection of the building noted in the report</li>
	</ol>
	<br />
	<h3>Why use <?=COMPANY?>?</h3>
	<p>As an owner you want to ensure that you get the right information about maintaining a valuable asset. As a Body Corporate Manager you want to ensure that those providing information get it right because the ultimate Part 5 delegation and its responsibilities are yours. Here is how <?=COMPANY?> delivers these outcomes for you:-</p>
	<p>We are professionally qualified and insured for:-</p>
	<ol>
		<li>Building maintenance experience including management of large rectification work</li>
   		<li>24 years of Body Corporate industry experience</li>
   		<li>Workplace Health and Safety expertise</li>
	</ol>
	<br />
	<p>Flexibility - 	you can get an Annual Safety Audit Report in conjunction with a Part 5 Report. This helps meet obligations under The Workplace Health & Safety Act, mitigates civil liability risk and is very advisable as the Body Corporate Manager has these delegated responsibilities.</p>
	<h3>What <?=COMPANY?> provides to you...</h3>
	<ul class='listing'>
		<li>Courteous, easily identifiable and professionally dressed Inspectors who are willing to spend time with a Body Corporate representative on the day of the inspection</li>
    	<li>Local representation in Brisbane, Sunshine Coast, Gold Coast & Cairns by qualified expert staff who understand local conditions.</li>
    </ul>
</div>
