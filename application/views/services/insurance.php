<div id='main' class='grid_7'>
	<h1>Insurance</h1>

	<p>Your insurance policy&rsquo;s averaging clause could end up costing you money if your building is underinsured, even on your smallest claim. The clause means that if your building is underinsured by 20% then your payout on any claim will be discounted by 20% leaving you out of pocket.</p>
	<p>With the recent hikes in building costs and continued pressure on costs many buildings are currently underinsured and that will cost you money.</p>
	<p>Our experienced valuers and quantity surveyors ensure that your replacement cost estimation is accurate and comprehensive by systematically breaking down your building&rsquo;s replacement costs to the necessary level of detail that ensures you get a professional report you can rely on every time.</p>
	<p>Your valuation will take into account cost factor like differing levels of finish, differing types and qualities of construction material, lifts types, fire safety equipment, different costs for areas like open air balconies, fire escapes, common hallways, cross-flow ventilation car parks. Basically anything that could cost you money.</p>
	<p>All your other building costs that you would expect in a professional report as well as items that are required under the relevant Act are included. Demolition costs, consultant fees, council and other regulatory fees, escalation costs, landscaping and external works.</p>
	<p>You will not get a valuation based on the &lsquo;pro-forma&rsquo; method where an office junior to does a drive-by photo and then a general construction rate is applied to the whole area of the building. This method is inaccurate, unprofessional and yet is used widely and seen unfortunately as acceptable.</p>
	<p>Using <?=COMPANY?> you get a professional report completed at a competitive price due to <?=COMPANY?>&rsquo;s use of cutting edge technology based on systems that ensure accuracy and consistency and a qualified experienced team. Your Insurance Replacement Valuation will be based on 21 years of experience, and over 7,000 Insurance Replacement Valuations.</p> 
	<h4>Fact Sheets</h4>
	<p><a href="/library/docs/pdf/QLDInsurance2011.pdf">Qld Insurance Valuation</a> <img src="/library/images/pdfsmall.png"> (283 kB)</p>
	<p><a href="/library/docs/pdf/NSWInsurance2011.pdf">NSW Insurance Valuation</a> <img src="/library/images/pdfsmall.png"> (283 kB)</p>
	<p><a href="/library/docs/pdf/VICInsurance2011.pdf">Vic Insurance Valuation</a> <img src="/library/images/pdfsmall.png"> (283 kB)</p>

</div>
