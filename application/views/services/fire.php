<div id='main' class='grid_7'>
	<h1>Fire Services</h1>

	<p>The requirements for fire safety in Queensland altered dramatically in July 2008, with the introduction of the Building Fire Safety Regulation 2008 and the adoption, under MP 6.1 of the Queensland Development Code, of AS 1851-2005 (Maintenance of Fire Protection Systems and Equipment) as the standard to which Fire Safety Installations ('FSIs') must be maintained.</p>
	<p>The changes aim to greatly increase the safety of people during a fire emergency, by ensuring that buildings have adequate and properly maintained FSIs and by improving the arrangements and training for evacuation in case of an emergency. The owner, occupier and/or manager of a building in Queensland have a legal obligation to ensure the safety of every person in the building in the event of a fire or other emergency. The Body Corporate has control over and is the occupier of the common areas of your property. As such, it must make sure that:</p>
	<ul class='listing'>
		<li>All FSIs required to be in the common property areas are installed and properly maintained;</li>
    	<li>All evacuation routes are safe and free from hazards;</li>
    	<li>A Fire and Emergency Plan for the building is developed and maintained;</li>
    	<li>The evacuation coordinator, people with responsibilities under the Fire and Emergency Plan and workers receive specific training;</li>
    	<li>If the building is a High Occupancy Building, a Fire Safety Advisor is appointed; and</li>
    	<li>An &lsquo;Occupier&rsquo;s Statement&rsquo; is submitted to the Commissioner of the Queensland Fire and Rescue Service every 12 months, stating that all FSIs have been properly maintained.</li>
    </ul>
	<br />
	<p>There are penalties set out under the Fire and Rescue Services Act 1990 of up to $7,500 for each instance of basic non-compliance, and of up to $150,000 or three years imprisonment if the non-compliance causes multiple deaths.</p>
	<h4>Fact Sheets</h4>
	<p><a href="/library/docs/pdf/QLDFire2p.pdf">Qld Fire Services Summary Sheet (2 pages)</a> <img src="/library/images/pdfsmall.png"> (369 kB)</p>
</div>
