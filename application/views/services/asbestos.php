<div id='main' class='grid_7'>
	<h1>Asbestos</h1>
	
	<p>Asbestos has been a major health and safety issue over the last 20 years. Although asbestos has been removed from most products and, where it is used it is strictly controlled, the issues relating to asbestos are far from over.</p>
	<p>Decades after the hazards of asbestos were first revealed, large quantities from earlier uses remain undiscovered or "forgotten" in Australian workplaces.</p>
	<p>Where a hazard is known to exist, the law is clear about how it is to be treated and safety measures that are to be taken. But until any individual asbestos application is revealed the issue remains largely a matter of conscience for managers, property owners and others. It is, however, an issue that must be faced, and we have set out some guidelines for persons in control of workplaces to manage the asbestos hazard that may exist in the workplace.</p>
	<h3>What obligations do you have?</h3>
	<p>A person who controls a workplace (for example, the owner, body corporate or owners corporation, or employer) has obligations, both at common law and under the various Health and Safety regimes, to identify and control asbestos-related risks at their property.</p>
	<p>An effective approach to asbestos risk management involves four stages:</p>
	<ol>
		<li>Identifying asbestos and asbestos containing materials (ACM)</li>
   		<li>Implementing control measures to control asbestos-related risks</li>
   		<li>Developing an asbestos register for the property, to alert workers to the presence of asbestos</li>
   		<li>Reviewing the condition of the asbestos and ACM, the control measures, and the register at least every 12 months.</li>
	</ol>
	<br />
	<h3>How can <?=COMPANY?> help you manage asbestos-related risks?</h3>
	<p>Our inspector will inspect your property to identify materials that are presumed to contain asbestos fibres. Where we identify such materials, we will assess its condition and the health hazard they potentially pose. Based on this assessment, we may take samples for testing at a National Association of Testing Authorities (NATA) accredited laboratory, for a positive identification that the material contains asbestos fibres. We will also make recommendations about the ongoing management and maintenance of the material, and produce an asbetsos register for the property.</p>
	
	<h4>Fact Sheets</h4>
	<p><a href="/library/docs/pdf/QLDAsbestos2011.pdf">Qld Asbestos Report</a> <img src="/library/images/pdfsmall.png"> (319 kB)</p>
</div>