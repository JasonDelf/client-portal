<div id='main' class='grid_7'>
	<h1>Sinking Fund Planning</h1>

	<p>Preparing a Sinking Fund Forecast or Plan, or a 10 Year Maintenance Plan is as much an art as a science. There is a fine balance between ensuring that all future expenditure is covered, to avoid unexpected special levies, and excessive allowances that impose unnecessary costs on owners.</p>
	<p>A <?=COMPANY?> report ensures that the owners&rsquo; funds are used in the most efficient manner, to keep levies as low as possible while ensuring the proper maintenance of the property. We strive to achieve a balance, to ensure that adequate funds are available to cover major expenses, while avoiding the unnecessary build-up of capital reserves that owners could put to better use.</p>
	<p>We have completed over 17,000 Sinking Fund Forecasts and Plans and 10 Year Maintenance Plans, for all types of buildings. Our experience ranges from duplexes to the largest residential, commercial and industrial schemes in Australia.</p>
	<p>Here is why <?=COMPANY?> is the biggest supplier of Sinking Funds & 10 Year Maintenance Plans in the country:</p>
	<ul class='listing'>
    	<li>We continuously update our maintenance, repair and replacement rates using industry sources and privately-sourced pricing data, to ensure we deliver the most accurate expense forecasts.</li>
    	<li>We maintain pricing data for more than 125 suburbs, towns and regions. This allows us to provide forecasts based on maintenance prices in your local region.</li>
    	<li>Our Reports include suggestions to lengthen the life and/or reduce the maintenance costs of items, which save you money.</li>
    	<li>We ensure that our Reports don&rsquo;t include small value that should not be included and can be allowed for in a contingency allowance, which we tailor based on the age, condition and features of the property.</li>
    	<li>We employ qualified quantity surveyors and building inspectors to produce our reports, as well as technical research staff to ensure a consistent, high-quality service.</li>
    	<li>We have served the Strata and Property Management industries since 1984. We have the experience, and will be around to support our report in years to come.</li>
	</ul>
	<br/>
	<p>For our clients, we ensure that:</p>
	<ul class='listing'>
    	<li>Our reports are simple to understand, and easy to apply;</li>
    	<li>We take the time to develop a complete understanding of your property up-front, to avoid pitfalls down the track;</li>
    	<li>Our inspectors are fully trained industry professionals, backed with the systems and tools to produce high-quality reports;</li>
    	<li>Our technical team and inspectors are available to discuss any questions you may have, and to provide advice at any time.</li>
	</ul>
	<br />
	<p>Our reports are compiled using our proprietary software that has been custom designed to calculate the most efficient possible use of your funds, within the requirements of each Australian jurisdiction.</p>
	<h4>Fact Sheets</h4>
	<p><a href="/library/docs/pdf/QLDSinkingFund2011.pdf">Qld Sinking Fund Forecast</a> <img src="/library/images/pdfsmall.png"> (307 kB)</p>
	<p><a href="/library/docs/pdf/NSWSinkingFund2011.pdf">NSW Sinking Fund Plan</a> <img src="/library/images/pdfsmall.png"> (303 kB)</p>
	<p><a href="/library/docs/pdf/VIC10YearMaintenance2011.pdf">Vic 10 Year Maintenance Plan</a> <img src="/library/images/pdfsmall.png"> (307 kB)</p>
</div>
